<?php
class DATABASE_CONFIG
{
	public $default = array(
		'datasource'	=> 'Database/Mysql',
		'persistent'	=> false,
		'host'			=> 'localhost',
		'login'			=> 'root',
		'password'		=> 'root',
		'database'		=> 'super_cerdo',
		'prefix'		=> 'sc_',
		'encoding' 		=> 'utf8'
	);
}
