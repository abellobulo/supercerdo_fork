<?php

/**************** Enlaces publicos **************/
Router::connect('/', array('controller' => 'pages', 'action' => 'home'));
Router::connect('/home', array('controller' => 'pages', 'action' => 'home'));
// Router::connect('/nuestros_productos', array('controller' => 'productos', 'action' => 'index'));
Router::connect('/productos', array('controller' => 'productos', 'action' => 'linea_producto'));

Router::connect('/nuestros_cortes', array('controller' => 'cortes', 'action' => 'index'));
Router::connect('/recetas', array('controller' => 'recetas', 'action' => 'index'));

//Router::connect('/:slug', array('controller' => 'productos', 'action' => 'linea_producto'), array('pass' => array('slug')));
//Router::connect('/Producto/:id-:slug', array('controller' => 'productos', 'action' => 'view'), array('pass' => array('id', 'slug'), 'id' => '[0-9]+'));

/**************** Enlaces Admin **************/
Router::connect('/admin', array('controller' => 'pages', 'action' => 'home', 'admin' => true));
Router::connect('/admin/login', array('controller' => 'administradores', 'action' => 'login', 'admin' => true));

Router::connect('/seccion/*', array('controller' => 'pages', 'action' => 'display'));

CakePlugin::routes();
require CAKE . 'Config' . DS . 'routes.php';
