<?php
App::uses('AppController', 'Controller');
class BannersController extends AppController
{
	public function admin_index()
	{
		$this->paginate		= array(
			'recursive'			=> 0
		);
		$banners	= $this->paginate();
		$this->set(compact('banners'));
	}

	public function admin_add( $modulo = null, $redirect_controller = null, $redirect_action = null, $redirect_param = null )
	{
		if ( $this->request->is('post') )
		{
			$this->Banner->create();
			if ( $this->Banner->save($this->request->data) )
			{
				$this->Session->setFlash('Registro agregado correctamente.', null, array(), 'success');
				$this->redirect(array('controller' => $redirect_controller,'action' => $redirect_action));
			}
			else
			{
				$this->Session->setFlash('Error al guardar el registro. Por favor intenta nuevamente.', null, array(), 'danger');
				$this->redirect(array('controller' => $redirect_controller,'action' => $redirect_action));
			}
		}
		$this->set(compact('modulo', 'redirect_controller', 'redirect_action', 'redirect_param'));
	}

	public function admin_edit($id = null, $redirect_controller = null, $redirect_action = null, $redirect_param = null)
	{
		if ( ! $this->Banner->exists($id) )
		{
			$this->Session->setFlash('Registro inválido.', null, array(), 'danger');
			$this->redirect(array('controller' => 'pages', 'action' => 'home'));
		}

		if ( $this->request->is('post') || $this->request->is('put') )
		{
			if ( $this->Banner->save($this->request->data) )
			{
				$this->Session->setFlash('Registro editado correctamente', null, array(), 'success');
				$this->redirect(array('controller' => $redirect_controller,'action' => $redirect_action));
			}
			else
			{
				$this->Session->setFlash('Error al guardar el registro. Por favor intenta nuevamente.', null, array(), 'danger');
				$this->redirect(array('controller' => $redirect_controller,'action' => $redirect_action));
			}
		}
		else
		{
			$this->request->data	= $this->Banner->find('first', array(
				'conditions'	=> array('Banner.id' => $id)
			));
		}


		$this->set(compact('redirect_controller', 'redirect_action', 'redirect_param'));
	}

	public function admin_delete($id = null)
	{
		$this->Banner->id = $id;
		if ( ! $this->Banner->exists() )
		{
			$this->Session->setFlash('Registro inválido.', null, array(), 'danger');
			$this->redirect(array('action' => 'index'));
		}

		$this->request->onlyAllow('post', 'delete');
		if ( $this->Banner->delete() )
		{
			$this->Session->setFlash('Registro eliminado correctamente.', null, array(), 'success');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash('Error al eliminar el registro. Por favor intenta nuevamente.', null, array(), 'danger');
		$this->redirect(array('action' => 'index'));
	}

	public function admin_exportar()
	{
		$datos			= $this->Banner->find('all', array(
			'recursive'				=> -1
		));
		$campos			= array_keys($this->Banner->_schema);
		$modelo			= $this->Banner->alias;

		$this->set(compact('datos', 'campos', 'modelo'));
	}

	/**
	 * Funcion que permite activar el registro, desde el listado
	 * @param			Object			$id			Id del registro
	 */
	public function admin_activar($id = null)
	{
		$this->Banner->id = $id;
		if ( ! $this->Banner->exists() )
		{
			$this->Session->setFlash('Banner inválido.', null, array(), 'danger');
			$this->redirect(array('controller' => 'pages', 'action' => 'home'));
		}

		if ( $this->Banner->saveField('activo', true) )
		{
			$this->Session->setFlash('Banner activado correctamente.', null, array(), 'success');
			$this->redirect(array('controller' => 'pages', 'action' => 'home'));
		}
		$this->Session->setFlash('Error al activar el registro. Por favor intentalo nuevamente.', null, array(), 'danger');
		$this->redirect(array('controller' => 'pages', 'action' => 'home'));
	}

	/**
	 * Funcion que permite desactivar el registro, desde el listado
	 * @param			Object			$id			Id del registro
	 */
	public function admin_desactivar($id = null)
	{
		$this->Banner->id = $id;
		if ( ! $this->Banner->exists() )
		{
			$this->Session->setFlash('Banner inválido.', null, array(), 'danger');
			$this->redirect(array('controller' => 'pages', 'action' => 'home'));
		}

		if ( $this->Banner->saveField('activo', false) )
		{
			$this->Session->setFlash('Banner desactivado correctamente.', null, array(), 'success');
			$this->redirect(array('controller' => 'pages', 'action' => 'home'));
		}
		$this->Session->setFlash('Error al desactivar el registro. Por favor intentalo nuevamente.', null, array(), 'danger');
		$this->redirect(array('controller' => 'pages', 'action' => 'home'));
	}

	public function admin_bannerFondo()
	{
		// START BREADCRUMB
		$breadcrumb = array(
			array(
				'label' =>	'Aministrador Súper Cerdo',
				'link'	=>	'',
				'class'	=>	''
			),
			array(
				'label' =>	'Sección Banners Fondo',
				'link'	=>	'',
				'class'	=>	''
			)
		);


		$this->paginate		= array(
			'recursive'			=> 0,
			'conditions'	=> array(
				'Banner.activo'		=>	true,
				'Banner.tipo'		=>	'fondo'
			)
		);

		$banner_fondo	= $this->paginate();

		$redirect_page	=	array(
			'controller'	=> 'banners',
			'action'		=> 'bannerFondo',
			'param'			=> ''
		);

		$this->set(compact('breadcrumb', 'banner_fondo', 'redirect_page'));
	}
}
