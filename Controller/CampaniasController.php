<?php
App::uses('AppController', 'Controller');
class CampaniasController extends AppController
{
	public function admin_index()
	{
		// Instancia con el modulo de SEO
		$this->Seo = ClassRegistry::init('Seo');
		// START BREADCRUMB
		$breadcrumb = array(
			array(
				'label' =>	'Aministrador Súper Cerdo',
				'link'	=>	'',
				'class'	=>	''
			),
			array(
				'label' =>	'Lista Campañas',
				'link'	=>	'',
				'class'	=>	''
			)
		);

		$this->paginate		= array(
			'recursive'			=> 0,
			'limit'				=> 8
		);
		$campanias	= $this->paginate();
		// Variables Home
		$modulo_seo 	= 'Campania';
		$modulo_seo_id 	= '0';
		$redirect_page	=	array(
			'controller'	=> 'Campanias',
			'action'		=> 'index',
			'param'			=> ''
		);
		// DATOS DEL SEO DEL HOME
		$datos_seo	=	$this->Seo->obtenerSEO( $modulo_seo, $modulo_seo_id );

		$this->set(compact('campanias', 'breadcrumb', 'modulo_seo', 'modulo_seo_id', 'redirect_page', 'datos_seo'));
	}

	public function admin_add()
	{
		if ( $this->request->is('post') )
		{
			$this->Campania->create();
			if ( $this->Campania->save($this->request->data) )
			{
				$this->Session->setFlash('Registro agregado correctamente.', null, array(), 'success');
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash('Error al guardar el registro. Por favor intenta nuevamente.', null, array(), 'danger');
			}
		}
		$administradores	= $this->Campania->Administrador->find('list');
		$this->set(compact('administradores'));
	}

	public function admin_edit($id = null)
	{
		if ( ! $this->Campania->exists($id) )
		{
			$this->Session->setFlash('Registro inválido.', null, array(), 'danger');
			$this->redirect(array('action' => 'index'));
		}

		if ( $this->request->is('post') || $this->request->is('put') )
		{
			if ( $this->Campania->save($this->request->data) )
			{
				$this->Session->setFlash('Registro editado correctamente', null, array(), 'success');
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash('Error al guardar el registro. Por favor intenta nuevamente.', null, array(), 'danger');
			}
		}
		else
		{
			$this->request->data	= $this->Campania->find('first', array(
				'conditions'	=> array('Campania.id' => $id)
			));
		}
		$administradores	= $this->Campania->Administrador->find('list');
		$this->set(compact('administradores'));
	}

	public function admin_delete($id = null)
	{
		$this->Campania->id = $id;
		if ( ! $this->Campania->exists() )
		{
			$this->Session->setFlash('Registro inválido.', null, array(), 'danger');
			$this->redirect(array('action' => 'index'));
		}

		$this->request->onlyAllow('post', 'delete');
		if ( $this->Campania->delete() )
		{
			$this->Session->setFlash('Registro eliminado correctamente.', null, array(), 'success');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash('Error al eliminar el registro. Por favor intenta nuevamente.', null, array(), 'danger');
		$this->redirect(array('action' => 'index'));
	}

	public function admin_exportar()
	{
		$datos			= $this->Campania->find('all', array(
			'recursive'				=> -1
		));
		$campos			= array_keys($this->Campania->_schema);
		$modelo			= $this->Campania->alias;

		$this->set(compact('datos', 'campos', 'modelo'));
	}

	public function index()
	{
		$this->paginate		= array(
			//'limit'				=> 8,
			'conditions'		=> array(
				'Campania.activo'	=> true
			)
		);
		$campanias	= $this->paginate();
		$this->set(compact('campanias'));
	}
}
