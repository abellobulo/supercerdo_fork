<?php
App::uses('AppController', 'Controller');
class ContactosController extends AppController
{

	public function admin_contacto()
	{
		// INSTANCIA CON EL MODULO DE RECETA
		$this->Configuracion 		 = ClassRegistry::init('Configuracion');
		// Instancia con el modulo de SEO
		$this->Seo = ClassRegistry::init('Seo');

		// START BREADCRUMB
		$breadcrumb = array(
			array(
				'label' =>	'Aministrador Súper Cerdo',
				'link'	=>	'',
				'class'	=>	''
			),
			array(
				'label' =>	'Sección Contactos',
				'link'	=>	'',
				'class'	=>	''
			)
		);

		// ITEMS DE CONTACTO
		$items_contacto		=	$this->Configuracion->find('all', array(
			'conditions'		=> array(
				'Configuracion.modulo'	=>	'contacto',
				'Configuracion.tipo'	=> 	'pageContacto',
				'Configuracion.activo'	=>	'true'
			)
		));


		// Variables Home
		$modulo_seo 	= 'Contacto';
		$modulo_seo_id 	= '0';
		$redirect_page	=	array(
			'controller'	=> 'Contactos',
			'action'		=> 'contacto',
			'param'			=> ''
		);
		// DATOS DEL SEO DEL HOME
		$datos_seo	=	$this->Seo->obtenerSEO( $modulo_seo, $modulo_seo_id );



		$this->set(compact('breadcrumb', 'items_contacto', 'modulo_seo', 'modulo_seo_id', 'redirect_page', 'datos_seo'));
	}

	public function admin_index()
	{
		$this->paginate		= array(
			'recursive'			=> 0
		);
		$contactos	= $this->paginate();
		$this->set(compact('contactos'));
	}

	public function admin_add()
	{
		if ( $this->request->is('post') )
		{
			$this->Contacto->create();
			if ( $this->Contacto->save($this->request->data) )
			{
				$this->Session->setFlash('Registro agregado correctamente.', null, array(), 'success');
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash('Error al guardar el registro. Por favor intenta nuevamente.', null, array(), 'danger');
			}
		}
	}

	public function admin_edit($id = null)
	{
		if ( ! $this->Contacto->exists($id) )
		{
			$this->Session->setFlash('Registro inválido.', null, array(), 'danger');
			$this->redirect(array('action' => 'index'));
		}

		if ( $this->request->is('post') || $this->request->is('put') )
		{
			if ( $this->Contacto->save($this->request->data) )
			{
				$this->Session->setFlash('Registro editado correctamente', null, array(), 'success');
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash('Error al guardar el registro. Por favor intenta nuevamente.', null, array(), 'danger');
			}
		}
		else
		{
			$this->request->data	= $this->Contacto->find('first', array(
				'conditions'	=> array('Contacto.id' => $id)
			));
		}
	}

	public function admin_delete($id = null)
	{
		$this->Contacto->id = $id;
		if ( ! $this->Contacto->exists() )
		{
			$this->Session->setFlash('Registro inválido.', null, array(), 'danger');
			$this->redirect(array('action' => 'index'));
		}

		$this->request->onlyAllow('post', 'delete');
		if ( $this->Contacto->delete() )
		{
			$this->Session->setFlash('Registro eliminado correctamente.', null, array(), 'success');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash('Error al eliminar el registro. Por favor intenta nuevamente.', null, array(), 'danger');
		$this->redirect(array('action' => 'index'));
	}

	public function admin_exportar()
	{
		$datos			= $this->Contacto->find('all', array(
			'recursive'				=> -1
		));
		$campos			= array_keys($this->Contacto->_schema);
		$modelo			= $this->Contacto->alias;

		$this->set(compact('datos', 'campos', 'modelo'));
	}

	public function index()
	{
		
	}
}
