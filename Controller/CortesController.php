<?php
App::uses('AppController', 'Controller');
class CortesController extends AppController
{
	public function admin_index()
	{
		// Instancia con el modulo de SEO
		$this->Seo = ClassRegistry::init('Seo');

		// START BREADCRUMB
		$breadcrumb = array(
			array(
				'label' =>	'Aministrador Súper Cerdo',
				'link'	=>	'',
				'class'	=>	''
			),
			array(
				'label' =>	'Cortes',
				'link'	=>	'',
				'class'	=>	''
			)
		);


		$this->paginate		= array(
			'limit'				=> 8,
			'order'				=> array('Corte.nombre'	=>	'ASC'),
		);
		$cortes	= $this->paginate();

		// Variables Home
		$modulo_seo 	= 'Corte';
		$modulo_seo_id 	= false;
		$redirect_page	=	array(
			'controller'	=> 'Cortes',
			'action'		=> 'index',
			'param'			=> ''
		);
		// DATOS DEL SEO DEL HOME
		$datos_seo	=	$this->Seo->obtenerSEO( $modulo_seo, $modulo_seo_id );



		$this->set(compact('breadcrumb', 'cortes', 'modulo_seo', 'modulo_seo_id', 'redirect_page', 'datos_seo')); 
	}

	public function admin_add()
	{
		if ( $this->request->is('post') )
		{
			$this->Corte->create();
			if ( $this->Corte->save($this->request->data) )
			{
				$this->Session->setFlash('Registro agregado correctamente.', null, array(), 'success');
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash('Error al guardar el registro. Por favor intenta nuevamente.', null, array(), 'danger');
			}
		}
		$recetas	= $this->Corte->Receta->find('list');
		$this->set(compact('recetas'));
	}

	public function admin_edit($id = null)
	{
		if ( ! $this->Corte->exists($id) )
		{
			$this->Session->setFlash('Registro inválido.', null, array(), 'danger');
			$this->redirect(array('action' => 'index'));
		}

		if ( $this->request->is('post') || $this->request->is('put') )
		{
			if ( $this->Corte->save($this->request->data) )
			{
				$this->Session->setFlash('Registro editado correctamente', null, array(), 'success');
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash('Error al guardar el registro. Por favor intenta nuevamente.', null, array(), 'danger');
			}
		}
		else
		{
			$this->request->data	= $this->Corte->find('first', array(
				'conditions'	=> array('Corte.id' => $id)
			));
		}
		$recetas	= $this->Corte->Receta->find('list');
		$this->set(compact('recetas'));
	}

	public function admin_delete($id = null)
	{
		$this->Corte->id = $id;
		if ( ! $this->Corte->exists() )
		{
			$this->Session->setFlash('Registro inválido.', null, array(), 'danger');
			$this->redirect(array('action' => 'index'));
		}

		$this->request->onlyAllow('post', 'delete');
		if ( $this->Corte->delete() )
		{
			$this->Session->setFlash('Registro eliminado correctamente.', null, array(), 'success');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash('Error al eliminar el registro. Por favor intenta nuevamente.', null, array(), 'danger');
		$this->redirect(array('action' => 'index'));
	}

	public function admin_exportar()
	{
		$datos			= $this->Corte->find('all', array(
			'recursive'				=> -1
		));
		$campos			= array_keys($this->Corte->_schema);
		$modelo			= $this->Corte->alias;

		$this->set(compact('datos', 'campos', 'modelo'));
	}


	public function index()
	{

		// START BREADCRUMB
		$breadcrumb = array(
			array(
				'label' 		=>	'Home',
				'controller'	=>	'pages',
				'action'		=>	'home'
			),
			array(
				'label' 		=>	'Nuestros Cortes',
				'controller'	=>	'',
				'action'		=>	''
			)
		);

		$cortes 	=	$this->Corte->find('all', array(
			'conditions'		=>	array(
				'Corte.activo'	=>	true
			)
		));

		$this->set(compact('cortes', 'breadcrumb'));
	}
}
