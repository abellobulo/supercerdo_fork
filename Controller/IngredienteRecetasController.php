<?php
App::uses('AppController', 'Controller');
class IngredienteRecetasController extends AppController
{
	public function admin_index()
	{
		$this->paginate		= array(
			'recursive'			=> 0
		);
		$ingredienteRecetas	= $this->paginate();
		$this->set(compact('ingredienteRecetas'));
	}

	public function admin_add()
	{
		if ( $this->request->is('post') )
		{
			$this->IngredienteReceta->create();
			if ( $this->IngredienteReceta->save($this->request->data) )
			{
				$this->Session->setFlash('Registro agregado correctamente.', null, array(), 'success');
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash('Error al guardar el registro. Por favor intenta nuevamente.', null, array(), 'danger');
			}
		}
		$recetas	= $this->IngredienteReceta->Receta->find('list');
		$this->set(compact('recetas'));
	}

	public function admin_edit($id = null)
	{
		if ( ! $this->IngredienteReceta->exists($id) )
		{
			$this->Session->setFlash('Registro inválido.', null, array(), 'danger');
			$this->redirect(array('action' => 'index'));
		}

		if ( $this->request->is('post') || $this->request->is('put') )
		{
			if ( $this->IngredienteReceta->save($this->request->data) )
			{
				$this->Session->setFlash('Registro editado correctamente', null, array(), 'success');
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash('Error al guardar el registro. Por favor intenta nuevamente.', null, array(), 'danger');
			}
		}
		else
		{
			$this->request->data	= $this->IngredienteReceta->find('first', array(
				'conditions'	=> array('IngredienteReceta.id' => $id)
			));
		}
		$recetas	= $this->IngredienteReceta->Receta->find('list');
		$this->set(compact('recetas'));
	}

	public function admin_delete($id = null)
	{
		$this->IngredienteReceta->id = $id;
		if ( ! $this->IngredienteReceta->exists() )
		{
			$this->Session->setFlash('Registro inválido.', null, array(), 'danger');
			$this->redirect(array('action' => 'index'));
		}

		$this->request->onlyAllow('post', 'delete');
		if ( $this->IngredienteReceta->delete() )
		{
			$this->Session->setFlash('Registro eliminado correctamente.', null, array(), 'success');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash('Error al eliminar el registro. Por favor intenta nuevamente.', null, array(), 'danger');
		$this->redirect(array('action' => 'index'));
	}

	public function admin_exportar()
	{
		$datos			= $this->IngredienteReceta->find('all', array(
			'recursive'				=> -1
		));
		$campos			= array_keys($this->IngredienteReceta->_schema);
		$modelo			= $this->IngredienteReceta->alias;

		$this->set(compact('datos', 'campos', 'modelo'));
	}
}
