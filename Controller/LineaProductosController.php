<?php
App::uses('AppController', 'Controller');
class LineaProductosController extends AppController
{
	public function admin_index()
	{
		$this->paginate		= array(
			'recursive'			=> 0
		);
		$lineaProductos	= $this->paginate();
		$this->set(compact('lineaProductos'));
	}

	public function admin_add()
	{
		if ( $this->request->is('post') )
		{
			$this->LineaProducto->create();
			if ( $this->LineaProducto->save($this->request->data) )
			{
				$this->Session->setFlash('Registro agregado correctamente.', null, array(), 'success');
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash('Error al guardar el registro. Por favor intenta nuevamente.', null, array(), 'danger');
			}
		}
		$parentLineaProductos	= $this->LineaProducto->ParentLineaProducto->find('list');
		$administradores	= $this->LineaProducto->Administrador->find('list');
		$this->set(compact('parentLineaProductos', 'administradores'));
	}

	public function admin_edit($id = null, $redirect_controller = null, $redirect_action = null, $redirect_param = null)
	{
		if ( ! $this->LineaProducto->exists($id) )
		{
			$this->Session->setFlash('Registro inválido.', null, array(), 'danger');
			$this->redirect(array('action' => 'index'));
		}

		if ( $this->request->is('post') || $this->request->is('put') )
		{
			if ( $this->LineaProducto->save($this->request->data) )
			{
				$this->Session->setFlash('Registro editado correctamente', null, array(), 'success');
				$this->redirect(array('controller' => 'productos', 'action' => 'seccion_productos'));
			}
			else
			{
				$this->Session->setFlash('Error al guardar el registro. Por favor intenta nuevamente.', null, array(), 'danger');
				$this->redirect(array('controller' => 'productos', 'action' => 'seccion_productos'));
			}
		}
		else
		{
			$this->request->data	= $this->LineaProducto->find('first', array(
				'conditions'	=> array('LineaProducto.id' => $id)
			));
		}
		$parentLineaProductos	= $this->LineaProducto->ParentLineaProducto->find('list');
		$administradores	= $this->LineaProducto->Administrador->find('list');
		$this->set(compact('parentLineaProductos', 'administradores', 'redirect_controller', 'redirect_action', 'redirect_param'));
	}

	public function admin_delete($id = null)
	{
		$this->LineaProducto->id = $id;
		if ( ! $this->LineaProducto->exists() )
		{
			$this->Session->setFlash('Registro inválido.', null, array(), 'danger');
			$this->redirect(array('action' => 'index'));
		}

		$this->request->onlyAllow('post', 'delete');
		if ( $this->LineaProducto->delete() )
		{
			$this->Session->setFlash('Registro eliminado correctamente.', null, array(), 'success');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash('Error al eliminar el registro. Por favor intenta nuevamente.', null, array(), 'danger');
		$this->redirect(array('action' => 'index'));
	}

	public function admin_exportar()
	{
		$datos			= $this->LineaProducto->find('all', array(
			'recursive'				=> -1
		));
		$campos			= array_keys($this->LineaProducto->_schema);
		$modelo			= $this->LineaProducto->alias;

		$this->set(compact('datos', 'campos', 'modelo'));
	}
}
