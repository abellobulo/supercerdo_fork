<?php
App::uses('AppController', 'Controller');
class NoticiasController extends AppController
{
	public function admin_index()
	{
		// Instancia con el modulo de SEO
		$this->Seo = ClassRegistry::init('Seo');


		// START BREADCRUMB
		$breadcrumb = array(
			array(
				'label' =>	'Aministrador Súper Cerdo',
				'link'	=>	'',
				'class'	=>	''
			),
			array(
				'label' =>	'Lista Noticias',
				'link'	=>	'',
				'class'	=>	''
			)
		);

		$this->paginate		= array(
			'recursive'			=> 0,
			'limit'				=> 12
		);
		$noticias	= $this->paginate();

		// Variables Home
		$modulo_seo 	= 'Noticia';
		$modulo_seo_id 	= '0';
		$redirect_page	=	array(
			'controller'	=> 'Noticias',
			'action'		=> 'index',
			'param'			=> ''
		);
		// DATOS DEL SEO DEL HOME
		$datos_seo	=	$this->Seo->obtenerSEO( $modulo_seo, $modulo_seo_id );

		$this->set(compact('noticias', 'breadcrumb', 'modulo_seo', 'modulo_seo_id', 'redirect_page', 'datos_seo'));
	}

	public function admin_add()
	{
		// INSTANCIA CON EL MODULO
		$this->Seo 		 	= ClassRegistry::init('Seo');

		if ( $this->request->is('post') )
		{
			$this->Noticia->create();
			if ( $this->Noticia->save($this->request->data) )
			{
				// GUARDA LA INFORMACIÓN DEL SEO
				$this->request->data['Seo']['modulo_id']	= $this->Noticia->id;
				$this->Seo->create();
				$this->Seo->save($this->request->data['Seo']);

				$this->Session->setFlash('Registro agregado correctamente.', null, array(), 'success');
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash('Error al guardar el registro. Por favor intenta nuevamente.', null, array(), 'danger');
			}
		}
		$administradores	= $this->Noticia->Administrador->find('list');
		$this->set(compact('administradores'));
	}

	public function admin_edit($id = null)
	{
		// INSTANCIA CON EL MODULO
		$this->Seo 		 	= ClassRegistry::init('Seo');

		if ( ! $this->Noticia->exists($id) )
		{
			$this->Session->setFlash('Registro inválido.', null, array(), 'danger');
			$this->redirect(array('action' => 'index'));
		}

		if ( $this->request->is('post') || $this->request->is('put') )
		{
			if ( $this->Noticia->save($this->request->data) )
			{
				$this->Seo->save($this->request->data['Seo']);
				$this->Session->setFlash('Registro editado correctamente', null, array(), 'success');
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash('Error al guardar el registro. Por favor intenta nuevamente.', null, array(), 'danger');
			}
		}
		else
		{
			$this->request->data	= $this->Noticia->find('first', array(
				'conditions'	=> array('Noticia.id' => $id)
			));
		}

		$seo_producto 			= $this->Seo->obtenerSEO('Noticia', $id);
		$this->set(compact('seo_producto'));
	}

	public function admin_delete($id = null)
	{
		$this->Noticia->id = $id;
		if ( ! $this->Noticia->exists() )
		{
			$this->Session->setFlash('Registro inválido.', null, array(), 'danger');
			$this->redirect(array('action' => 'index'));
		}

		$this->request->onlyAllow('post', 'delete');
		if ( $this->Noticia->delete() )
		{
			$this->Session->setFlash('Registro eliminado correctamente.', null, array(), 'success');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash('Error al eliminar el registro. Por favor intenta nuevamente.', null, array(), 'danger');
		$this->redirect(array('action' => 'index'));
	}

	public function admin_exportar()
	{
		$datos			= $this->Noticia->find('all', array(
			'recursive'				=> -1
		));
		$campos			= array_keys($this->Noticia->_schema);
		$modelo			= $this->Noticia->alias;

		$this->set(compact('datos', 'campos', 'modelo'));
	}

	public function index()
	{
		// LISTA DE NOTICIAS
		$this->paginate		= array(
			'recursive'			=> 0,
			'limit'				=> 12,
			'conditions'		=> array(
				'Noticia.activo'	=> true
			)
		);
		$noticias	= $this->paginate();

		$this->set(compact('noticias'));
	}

	public function view( $noticia_id = null )
	{
		if (  $noticia_id){

			// Ultimas noticias
			$ultimas_noticias 	=	$this->Noticia->find('all', array(
				'conditions'		=> array(
					'Noticia.activo'	=> true,
					'Noticia.id <>'		=> $noticia_id
				),
				'order'				=> array(
					'Noticia.id'		=> 'DESC'
				),
				'limit'				=> 2
			));

			// INFORMACIÓN DE LA NOTICIA
			$noticia 			=	$this->Noticia->find('first', array(
				'conditions'		=> array(
					'Noticia.id'		=> $noticia_id
				)
			));

			$this->set(compact('noticia', 'ultimas_noticias'));

		}
		else{
			$this->redirect(array('action' => 'index'));
		}

	}
}
