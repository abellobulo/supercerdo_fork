<?php
App::uses('AppController', 'Controller');
class NutricionalProductosController extends AppController
{
	public function admin_index()
	{
		$this->paginate		= array(
			'recursive'			=> 0
		);
		$nutricionalProductos	= $this->paginate();
		$this->set(compact('nutricionalProductos'));
	}

	public function admin_add()
	{
		if ( $this->request->is('post') )
		{
			$this->NutricionalProducto->create();
			if ( $this->NutricionalProducto->save($this->request->data) )
			{
				$this->Session->setFlash('Registro agregado correctamente.', null, array(), 'success');
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash('Error al guardar el registro. Por favor intenta nuevamente.', null, array(), 'danger');
			}
		}
		$productos	= $this->NutricionalProducto->Producto->find('list');
		$this->set(compact('productos'));
	}

	public function admin_edit($id = null)
	{
		if ( ! $this->NutricionalProducto->exists($id) )
		{
			$this->Session->setFlash('Registro inválido.', null, array(), 'danger');
			$this->redirect(array('action' => 'index'));
		}

		if ( $this->request->is('post') || $this->request->is('put') )
		{
			if ( $this->NutricionalProducto->save($this->request->data) )
			{
				$this->Session->setFlash('Registro editado correctamente', null, array(), 'success');
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash('Error al guardar el registro. Por favor intenta nuevamente.', null, array(), 'danger');
			}
		}
		else
		{
			$this->request->data	= $this->NutricionalProducto->find('first', array(
				'conditions'	=> array('NutricionalProducto.id' => $id)
			));
		}
		$productos	= $this->NutricionalProducto->Producto->find('list');
		$this->set(compact('productos'));
	}

	public function admin_delete($id = null)
	{
		$this->NutricionalProducto->id = $id;
		if ( ! $this->NutricionalProducto->exists() )
		{
			$this->Session->setFlash('Registro inválido.', null, array(), 'danger');
			$this->redirect(array('action' => 'index'));
		}

		$this->request->onlyAllow('post', 'delete');
		if ( $this->NutricionalProducto->delete() )
		{
			$this->Session->setFlash('Registro eliminado correctamente.', null, array(), 'success');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash('Error al eliminar el registro. Por favor intenta nuevamente.', null, array(), 'danger');
		$this->redirect(array('action' => 'index'));
	}

	public function admin_exportar()
	{
		$datos			= $this->NutricionalProducto->find('all', array(
			'recursive'				=> -1
		));
		$campos			= array_keys($this->NutricionalProducto->_schema);
		$modelo			= $this->NutricionalProducto->alias;

		$this->set(compact('datos', 'campos', 'modelo'));
	}
}
