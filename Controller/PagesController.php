<?php
App::uses('AppController', 'Controller');
class PagesController extends AppController
{
	public $name = 'Pages';
	public $uses = array();


	public function display()
	{
		$path	= func_get_args();
		$count	= count($path);
		if ( ! $count )
			$this->redirect('/');

		$page	= $subpage = $title_for_layout = null;

		if ( ! empty($path[0]) )
			$page = $path[0];

		if ( ! empty($path[1]) )
			$subpage = $path[1];

		if ( ! empty($path[$count - 1]) )
			$title_for_layout = Inflector::humanize($path[$count - 1]);

		$this->set(compact('page', 'subpage', 'title_for_layout'));
		$this->render(implode('/', $path));
	}


	public function admin_home( $activo_banner = null )
	{
		// Instancia con el modulo de Banner
		$this->Banner = ClassRegistry::init('Banner');
		// Instancia con el modulo de SEO
		$this->Seo = ClassRegistry::init('Seo');

		// START BREADCRUMB
		$breadcrumb = array(
			array(
				'label' =>	'Aministrador Súper Cerdo',
				'link'	=>	'',
				'class'	=>	''
			),
			array(
				'label' =>	'Home',
				'link'	=>	'',
				'class'	=>	''
			)
		);

		// Obtenemos los banners existentes
		$banners 		=	$this->Banner->obtenerBanner('Home', ( $activo_banner == '0' ? 2 : $activo_banner)) ;
		// Obtenemos la cantidad de banner activos
		$banner_todos =		$this->Banner->obtenerCatidadBanner('Home');
		// Obtenemos la cantidad de banner activos
		$banner_activos =	$this->Banner->obtenerCatidadBanner( 'Home', 1);
		// Obtenemos la cantidad de banner Inactivos
		$banner_inactivos =	$this->Banner->obtenerCatidadBanner( 'Home', '0');

		// Variables Home
		$modulo_seo 	= 'Home';
		$modulo_seo_id 	= false;
		$redirect_page	=	array(
			'controller'	=> 'pages',
			'action'		=> 'home',
			'param'			=> ''
		);
		// DATOS DEL SEO DEL HOME
		$datos_seo	=	$this->Seo->obtenerSEO( $modulo_seo, $modulo_seo_id );

		$this->set(compact('breadcrumb', 'banners', 'modulo_seo', 'modulo_seo_id', 'redirect_page', 'datos_seo', 'banner_todos', 'banner_activos', 'banner_inactivos'));

	}


	/**
		HOME
		Funcion que permite obtener todos los datos necesarios para el home
		@Data: Enero 2018
		@Autor: SSO - Ukko.cl
	*/
	public function home()
	{
		// Instancia con el modulo de Banner
		$this->Banner 		 = ClassRegistry::init('Banner');
		// Instancia con el modulo de Lineas de Productos
		$this->LineaProducto = ClassRegistry::init('LineaProducto');
		//Instancia modelo Recetas
		$this->Receta = ClassRegistry::init('Recetas');

		// Variables SEO
		$seo = array(
			'title_seo' => 'Súper Cerdo - Dev',
			'description_seo' => 'Súper Cerdo',
			'keywords_seo' => 'Súper Cerdo'
		);

		// Obtenemos los banners existentes
		$banners 	=	$this->Banner->find('all', array(
			'conditions'	=> array(
				'Banner.modulo'	=>	'home',
				'Banner.tipo'	=>	Null,
				'Banner.activo'	=>	true,
				'Banner.eliminado'	=>	false,
				'Banner.imagen IS NOT NULL'
			)
		));

		// Obtenemos las lineas de productos
		$linea_producto = $this->LineaProducto->find('all', array(
			'conditions'	=> 	array(
				'LineaProducto.activo' => true
			),
			'fields'		=> array(
				'LineaProducto.id',
				'LineaProducto.imagen',
				'LineaProducto.nombre'
			)
		));
		$recetas = $this->Receta->find('all', array('limit' => 8));
		$banner_fondo	=	$this->Banner->obtenerBannerFondo('Home');

		$this->set(compact('seo', 'banners', 'linea_producto', 'banner_fondo', 'recetas'));


	}


}
