<?php

App::uses('AppController', 'Controller');
class ProductosController extends AppController
{

	public function admin_seccion_productos()
	{
		// Instancia con el modulo de Banner
		$this->Banner 		 = ClassRegistry::init('Banner');
		// Instancia con el modulo de SEO
		$this->Seo = ClassRegistry::init('Seo');

		if ( $this->request->is('post') ){
			$banner_seccion_producto['Banner']['modulo'] = 'Producto';
			$banner_seccion_producto['Banner']['id'] = $this->request->data['Producto']['id'];
			$banner_seccion_producto['Banner']['imagen'] = $this->request->data['Producto']['imagen'];
			$banner_seccion_producto['Banner']['bajada'] = $this->request->data['Producto']['bajada'];


			$this->Banner->create();
			if ( $this->Banner->save($banner_seccion_producto) )
			{
				$this->Session->setFlash('Registro agregado correctamente.', null, array(), 'success');
			}
			else
			{
				$this->Session->setFlash('Error al guardar el registro. Por favor intenta nuevamente.', null, array(), 'danger');
			}

		}

		// START BREADCRUMB
		$breadcrumb = array(
			array(
				'label' =>	'Aministrador Súper Cerdo',
				'link'	=>	'',
				'class'	=>	''
			),
			array(
				'label' =>	'Sección Productos',
				'link'	=>	'',
				'class'	=>	''
			)
		);

		// LINEA DE PRODUCTOS
		$linea_producto = $this->Producto->LineaProducto->find('all', array(
			'conditions'	=> 	array(
				'LineaProducto.activo' => true
			)
		));

		// CANTIDAD DE PRODUCTOS

		/** TODOS */
		$cantidad_productos['cantidad_producto']			=	$this->Producto->find('count');
		/** Linea Cocina */
		$cantidad_productos['cantidad_producto_cocina'] 	=	$this->Producto->obtenerCantidadProductoLinea(1);
		/** Linea Sandwich */
		$cantidad_productos['cantidad_producto_sandwich'] 	=	$this->Producto->obtenerCantidadProductoLinea(2);
		/** Linea Parrilla */
		$cantidad_productos['cantidad_producto_parrilla'] 	=	$this->Producto->obtenerCantidadProductoLinea(3);

		// Obtenemos los banner existente de la pag de producto
		$banner_producto 	=	$this->Banner->find('first', array(
			'conditions'	=> array(
				'Banner.modulo'	=>	'Producto',
				'Banner.activo'	=>	true,
				'Banner.eliminado'	=>	false,
				'Banner.imagen IS NOT NULL'
			)
		));

		$redirect_page	=	array(
			'controller'	=> 'Productos',
			'action'		=> 'seccion_productos',
			'param'			=> ''
		);

		// DATOS DEL SEO DEL HOME
		$modulo_seo 	= 	'Producto';
		$modulo_seo_id	=	'0';
		$datos_seo		=	$this->Seo->obtenerSEO( 'Producto', $modulo_seo_id );

		$this->set(compact('breadcrumb', 'linea_producto', 'cantidad_productos', 'banner_producto', 'modulo_seo', 'modulo_seo_id', 'datos_seo', 'redirect_page'));
	}

	public function admin_index( $linea_producto_id = null )
	{

		// ASIGNAR PRODUCTO DESTACADO
		if ( $this->request->is('post') )
		{
			foreach ($this->request->data['ProductoDestacado'] as $key => $destacado) {
				if ( $this->Producto->asignarProductoDestacado(  $destacado['lista_producto'],  $destacado['destacado_linea'] ) ){
				}
			}

		}

		// START BREADCRUMB
		$breadcrumb = array(
			array(
				'label' =>	'Aministrador Súper Cerdo',
				'link'	=>	'',
				'class'	=>	''
			),
			array(
				'label' =>	'Lista Productos',
				'link'	=>	'',
				'class'	=>	''
			)
		);

		// LISTA PRODUCTOS
		$conditions	=	array();
		if ( $linea_producto_id ){
			$conditions['Producto.linea_producto_id'] = $linea_producto_id;
		}
		$this->paginate		= array(
			'limit'				=> 20,
			'conditions'		=> $conditions,
			'order'				=> array('Producto.nombre'	=>	'ASC'),
			'contain'			=> array(
				'LineaProducto'
			)
		);

		$productos	= $this->paginate();


		// INFORMACIÓN DE LAS LINEAS DE PRODUCTOS
		$lienas_productos 	=	$this->Producto->LineaProducto->find('all', array(
			'conditions'		=> array(
				'LineaProducto.activo'	=>	true
			),
			'fields'			=> array(
				'LineaProducto.id', 'LineaProducto.nombre', 'LineaProducto.producto_count'
			)
		));

		/** TODOS */
		$cantidad_productos['cantidad_producto']			=	$this->Producto->find('count');
		/** Linea Cocina */
		$cantidad_productos['cantidad_producto_cocina'] 	=	$this->Producto->obtenerCantidadProductoLinea(1);
		/** Linea Sandwich */
		$cantidad_productos['cantidad_producto_sandwich'] 	=	$this->Producto->obtenerCantidadProductoLinea(2);
		/** Linea Parrilla */
		$cantidad_productos['cantidad_producto_parrilla'] 	=	$this->Producto->obtenerCantidadProductoLinea(3);

		// LISTADO DE PRODCUTOS COCINA
		$lista_producto_cocina		=	$this->Producto->find('list', array('order'=>array('Producto.nombre'=>	'ASC'),'conditions' => array('Producto.linea_producto_id'=>1)));
		// LISTADO DE PRODCUTOS PARRILLA
		$lista_producto_parrilla	=	$this->Producto->find('list', array('order'=>array('Producto.nombre'=>	'ASC'),'conditions' => array('Producto.linea_producto_id'=>3)));
		// LISTADO DE PRODCUTOS SANDWICH
		$lista_producto_sadwich		=	$this->Producto->find('list', array('order'=>array('Producto.nombre'=>	'ASC'),'conditions' => array('Producto.linea_producto_id'=>2)));

		// PRODUCTO DESTACADO
		$destacado_cocina 				=	$this->Producto->productoDestacado(1);
		$destacado_sandwich 			=	$this->Producto->productoDestacado(2);
		$destacado_parrilla 			=	$this->Producto->productoDestacado(3);

		$this->set(compact('productos', 'lista_producto_cocina', 'lista_producto_parrilla', 'lista_producto_sadwich', 'breadcrumb', 'lienas_productos', 'destacado_cocina', 'destacado_sandwich', 'destacado_parrilla', 'cantidad_productos'));

	}

	public function admin_add()
	{
		// INSTANCIA CON EL MODULO DE RECETA
		$this->Receta 		 = ClassRegistry::init('Receta');
		// INSTANCIA CON EL MODULO
		$this->Seo 		 	= ClassRegistry::init('Seo');

		if ( $this->request->is('post') )
		{
			// RECOMENDADO PARA HACER CON EL PRODUCTO
			if ( ! empty($this->request->data['ProductoParaProducto']) ){

				$paraProducto = '';
				foreach ($this->request->data['ProductoParaProducto'] as $key => $opcion) {
					if ( $key > 0 ){ $paraProducto .=','; }
					$paraProducto .= $opcion;
				}
				$this->request->data['Producto']['para_producto'] = $paraProducto;
			}


			// PRODUCTO RECOMENDADO
			if ( ! empty($this->request->data['ProductoProbarProducto']) ){

				$probarProducto = '';
				foreach ($this->request->data['ProductoProbarProducto'] as $key_probar => $probar) {
					if ( $key_probar > 0 ){ $probarProducto .=','; }
					$probarProducto .= $probar;
				}
				$this->request->data['Producto']['probar_otro_producto'] = $probarProducto;
			}

			// RECETA RECOMENDADO
			if ( ! empty($this->request->data['ProductoRecomendarReceta']) ){

				$recetaProducto = '';
				foreach ($this->request->data['ProductoRecomendarReceta'] as $key_receta => $receta) {
					if ( $key_receta > 0 ){ $recetaProducto .=','; }
					$recetaProducto .= $receta;
				}
				$this->request->data['Producto']['recomendar_receta'] = $recetaProducto;
			}



			$this->Producto->create();
			if ( $this->Producto->saveAll($this->request->data) )
			{
				// GUARDA LA INFORMACIÓN DEL SEO
				$this->request->data['Seo']['modulo_id']	= $this->Producto->id;
				$this->Seo->create();
				$this->Seo->save($this->request->data['Seo']);


				$this->Session->setFlash('Registro agregado correctamente.', null, array(), 'success');
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash('Error al guardar el registro. Por favor intenta nuevamente.', null, array(), 'danger');
			}
		}

		$cortes				= $this->Producto->Corte->find('list');
		$lineas				= $this->Producto->LineaProducto->find('list');
		$productos			= $this->Producto->find('list', array('conditions' => array('Producto.activo' => true),'order' => array('Producto.nombre' => 'ASC')));
		$recetas			= $this->Receta->find('list', array('conditions' => array('Receta.activo' => true),'order' => array('Receta.nombre' => 'ASC')));

		$this->set(compact('cortes', 'lineas',  'productos', 'recetas'));
	}

	public function admin_edit($id = null)
	{
		// INSTANCIA CON EL MODULO DE RECETA
		$this->Receta 		 = ClassRegistry::init('Receta');
		// INSTANCIA CON EL MODULO
		$this->Seo 		 	= ClassRegistry::init('Seo');

		if ( ! $this->Producto->exists($id) )
		{
			$this->Session->setFlash('Registro inválido.', null, array(), 'danger');
			$this->redirect(array('action' => 'index'));
		}

		if ( $this->request->is('post') || $this->request->is('put') )
		{
			// ELIMINAR LOS DATOS DE NUTRIENTES
			$this->Producto->NutricionalProducto->deleteAll(array('Producto.id' => $this->request->data['Producto']['id']), false);

			// RECOMENDADO PARA HACER CON EL PRODUCTO
			if ( ! empty($this->request->data['ProductoParaProducto']) ){

				$paraProducto = '';
				foreach ($this->request->data['ProductoParaProducto'] as $key => $opcion) {
					if ( $key > 0 ){ $paraProducto .=','; }
					$paraProducto .= $opcion;
				}
				$this->request->data['Producto']['para_producto'] = $paraProducto;
			}


			// PRODUCTO RECOMENDADO
			if ( ! empty($this->request->data['ProductoProbarProducto']) ){

				$probarProducto = '';
				foreach ($this->request->data['ProductoProbarProducto'] as $key_probar => $probar) {
					if ( $key_probar > 0 ){ $probarProducto .=','; }
					$probarProducto .= $probar;
				}
				$this->request->data['Producto']['probar_otro_producto'] = $probarProducto;
			}

			// RECETA RECOMENDADO
			if ( ! empty($this->request->data['ProductoRecomendarReceta']) ){

				$recetaProducto = '';
				foreach ($this->request->data['ProductoRecomendarReceta'] as $key_receta => $receta) {
					if ( $key_receta > 0 ){ $recetaProducto .=','; }
					$recetaProducto .= $receta;
				}
				$this->request->data['Producto']['recomendar_receta'] = $recetaProducto;
			}

			if ( $this->Producto->saveAll($this->request->data) )
			{
				// GUARDA LA INFORMACIÓN DEL SEO
				$this->Seo->save($this->request->data['Seo']);

				$this->Session->setFlash('Registro editado correctamente', null, array(), 'success');
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash('Error al guardar el registro. Por favor intenta nuevamente.', null, array(), 'danger');
			}
		}
		else
		{
			$this->request->data	= $this->Producto->find('first', array(
				'conditions'	=> array('Producto.id' => $id),
				'contain'		=> array('NutricionalProducto')
			));
		}

		$cortes					= $this->Producto->Corte->find('list');
		$lineas					= $this->Producto->LineaProducto->find('list');
		$productos				= $this->Producto->find('list', array('conditions' => array('Producto.activo' => true),'order' => array('Producto.nombre' => 'ASC')));
		$recetas				= $this->Receta->find('list', array('conditions' => array('Receta.activo' => true),'order' => array('Receta.nombre' => 'ASC')));
		$paraProducto 			= $this->Producto->obtenerRecomendacionesProducto($id, 'para_producto');
		$probar_producto		= $this->Producto->obtenerRecomendacionesProducto($id, 'probar_otro_producto');
		$receta_prodcuto 		= $this->Producto->obtenerRecomendacionesProducto($id, 'recomendar_receta');
		$seo_producto 			= $this->Seo->obtenerSEO('Producto', $id);


		$this->set(compact('cortes', 'lineas',  'productos', 'seo_producto', 'recetas', 'paraProducto', 'probar_producto', 'receta_prodcuto'));
	}

	public function admin_delete($id = null)
	{
		$this->Producto->id = $id;
		if ( ! $this->Producto->exists() )
		{
			$this->Session->setFlash('Registro inválido.', null, array(), 'danger');
			$this->redirect(array('action' => 'index'));
		}

		$this->request->onlyAllow('post', 'delete');
		if ( $this->Producto->delete() )
		{
			$this->Session->setFlash('Registro eliminado correctamente.', null, array(), 'success');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash('Error al eliminar el registro. Por favor intenta nuevamente.', null, array(), 'danger');
		$this->redirect(array('action' => 'index'));
	}

	public function admin_exportar()
	{
		$datos			= $this->Producto->find('all', array(
			'recursive'				=> -1
		));
		$campos			= array_keys($this->Producto->_schema);
		$modelo			= $this->Producto->alias;

		$this->set(compact('datos', 'campos', 'modelo'));
	}


	public function index()
	{
		// Instancia con el modulo de Banner
		$this->Banner 		 = ClassRegistry::init('Banner');


		// START BREADCRUMB
		$breadcrumb = array(
			array(
				'label' 		=>	'Home',
				'controller'	=>	'pages',
				'action'		=>	'home'
			),
			array(
				'label' 		=>	'L&iacute;neas de Productos',
				'controller'	=>	'',
				'action'		=>	''
			)
		);

		// Banner Principal
		$banner 	=	$this->Banner->find('first', array(
			'conditions'	=> array(
				'Banner.modulo'	=>	'Producto',
				'Banner.activo'	=>	true,
				'Banner.eliminado'	=>	false,
				'Banner.imagen IS NOT NULL'
			)
		));

		// Obtenemos las lineas de productos
		$linea_producto = $this->Producto->LineaProducto->find('all', array(
			'conditions'	=> 	array(
				'LineaProducto.activo' => true
			),
			'fields'		=> array(
				'LineaProducto.id',
				'LineaProducto.imagen',
				'LineaProducto.nombre'
			)
		));

		$banner_fondo	=	$this->Banner->obtenerBannerFondo('Nuestros Productos');

		$this->set(compact('banner', 'linea_producto', 'banner_fondo', 'breadcrumb'));

	}


	/**
	 * [linea_producto description]
	 * @param  [type] $linea [description]
	 * @param  [type] $slug  [description]
	 * @return [type]        [description]
	 */
	public function linea_producto( $linea = null )
	{
		if (!$linea) {
			$linea = 'linea_cocina' ;
		}
		if ( $linea ) {
			// Obtenemos las lineas de productos
			$linea_select = array(
				'linea_cocina'			=>	1,
				'linea_sandwich'		=>	2,
				'linea_parrilla'		=>	3
			);

			$linea_label = array(
				'linea_cocina' 			=>	'L&iacute;nea Cocina',
				'linea_sandwich' 		=>	'L&iacute;nea Sandwich',
				'linea_parrilla' 		=>	'L&iacute;nea Parrilla'
			);

			// START BREADCRUMB
			$breadcrumb = array(
				array(
					'label' 		=>	'Home',
					'controller'	=>	'pages',
					'action'		=>	'home'
				),
				array(
					'label' 		=>	'L&iacute;neas de Productos',
					'controller'	=>	'productos',
					'action'		=>	'index'
				),
				array(
					'label' 		=>	$linea_label[$linea],
					'controller'	=>	'',
					'action'		=>	''
				)
			);

			$productos 	=	$this->Producto->find('all', array(
				'conditions'		=>	array(
					'Producto.activo'				=>	true,
					'Producto.linea_producto_id'	=>	$linea_select[$linea]
				)
			));

			// PRODUCTO DESTACADO
			$lineas_productos = $this->Producto->LineaProducto->find('all', array(
				'conditions'	=> 	array(
					'LineaProducto.activo' => true
				),
				'fields'		=> array(
					'LineaProducto.id',
					'LineaProducto.imagen',
					'LineaProducto.nombre'
				)
			));
			$destacado 			=	$this->Producto->productoDestacado($linea_select[$linea]);
			// LINEA
			$linea_producto 	=	$this->Producto->LineaProducto->find('first', array(
				'conditions'		=>	array(
					'LineaProducto.id'	=>	$linea_select[$linea]
				)
			));

			$this->set(compact('productos', 'destacado', 'linea_producto', 'lineas_productos', 'linea', 'breadcrumb'));
		}
		else{
			$this->redirect(array('action' => 'index'));
		}
	}

	/**
	 * [view description]
	 * @param  [type] $id_producto [description]
	 * @return [type]              [description]
	 */
	public function view( $id_producto = null, $slug = null )
	{
		if ( $id_producto ){

			if ( ! $this->Producto->exists($id_producto) )
			{
				$this->redirect(array('action' => 'linea_producto'));
			}

			// INFORMACION DE PRODUCTO
			$producto 		=	$this->Producto->find('first', array(
				'conditions'	=> 	array(
					'Producto.id'	=> $id_producto
				),
				'contain'		=> array(
					'NutricionalProducto'
				)
			));

			$linea_label = array(
				'1' 			=>	'L&iacute;nea Cocina',
				'2' 			=>	'L&iacute;nea Sandwich',
				'3' 			=>	'L&iacute;nea Parrilla'
			);

			$linea_param	=  array(
				'1' 			=>	'linea_cocina',
				'2' 			=>	'linea_sandwich',
				'3' 			=>	'linea_parrilla'
			);

			// START BREADCRUMB
			$breadcrumb = array(
				array(
					'label' 		=>	'Home',
					'controller'	=>	'pages',
					'action'		=>	'home'
				),
				array(
					'label' 		=>	'L&iacute;neas de Productos',
					'controller'	=>	'productos',
					'action'		=>	'index'
				),
				array(
					'label' 		=>	$linea_label[$producto['Producto']['linea_producto_id']],
					'controller'	=>	'productos',
					'action'		=>	'linea_producto',
					'param'			=>	$linea_param[$producto['Producto']['linea_producto_id']]
				),
				array(
					'label' 		=>	$producto['Producto']['nombre'],
					'controller'	=>	'',
					'action'		=>	'',
					'param'			=>	''
				)
			);
			// LINEA DE PRODUCTOS
			$lineas_productos = $this->Producto->LineaProducto->find('all', array(
				'conditions'	=> 	array(
					'LineaProducto.activo' => true
				)
			));
			$this->set(compact('producto', 'lineas_productos' ,'breadcrumb'));

		}else{
			$this->redirect(array('action' => 'linea_producto'));
		}
	}

	/**
	 * [ajax_obtenerProducto description]
	 * @return [type] [description]
	 */
	public function ajax_obtenerProducto()
	{
		$this->layout = 'ajax';
		if ( $this->request->is('post') ){
			$id_producto = $this->request->data['idProducto'];
			$producto 		=	$this->Producto->obtenerProducto($id_producto);

			$this->set(compact('producto'));
		}
	}
}
