<?php
App::uses('AppController', 'Controller');
class RecetasController extends AppController
{
	public function admin_index()
	{

		// Instancia con el modulo de SEO
		$this->Seo = ClassRegistry::init('Seo');

		// ASIGNAR RECETA DESTACADO
		if ( $this->request->is('post') )
		{
			if ( $this->Receta->asignarRecetaDestacada( $this->request->data['Receta']['detacada'] ) ){
				$this->Session->setFlash('Receta destacada.', null, array(), 'success');
			}

		}

		// START BREADCRUMB
		$breadcrumb = array(
			array(
				'label' =>	'Aministrador Súper Cerdo',
				'link'	=>	'',
				'class'	=>	''
			),
			array(
				'label' =>	'SecciÃ³n Recetas',
				'link'	=>	'',
				'class'	=>	''
			)
		);

		$this->paginate		= array(
			'recursive'			=> 0
		);
		$recetas	= $this->paginate();

		// LISTA RECETAS PARA SELECIONAR DESTACADA
		$lista_recetas	= $this->Receta->find('list');
		// RECETA DESTACADO
		$destacado 			=	$this->Receta->recetaDestacada();

		// Variables Home
		$modulo_seo 	= 'Receta';
		$modulo_seo_id 	= '0';
		$redirect_page	=	array(
			'controller'	=> 'Recetas',
			'action'		=> 'index',
			'param'			=> ''
		);
		// DATOS DEL SEO DEL HOME
		$datos_seo	=	$this->Seo->obtenerSEO( $modulo_seo, $modulo_seo_id );


		$this->set(compact('recetas', 'destacado', 'breadcrumb', 'lista_recetas', 'modulo_seo', 'modulo_seo_id', 'redirect_page', 'datos_seo'));
	}

	public function admin_add()
	{
		// INSTANCIA CON EL MODULO
		$this->Seo 		 	= ClassRegistry::init('Seo');

		if ( $this->request->is('post') )
		{
			// RECOMENDAR RECETA
			if ( ! empty($this->request->data['RecomendarReceta']) ){

				$recomendar_receta = '';
				foreach ($this->request->data['RecomendarReceta'] as $key => $opcion) {
					if ( $key > 0 ){ $recomendar_receta .=','; }
					$recomendar_receta .= $opcion;
				}
				$this->request->data['Receta']['recomendar_receta'] = $recomendar_receta;
			}

			$this->Receta->create();
			if ( $this->Receta->saveAll($this->request->data) )
			{
				// GUARDA LA INFORMACIÃ“N DEL SEO
				$this->request->data['Seo']['modulo_id']	= $this->Receta->id;
				$this->Seo->create();
				$this->Seo->save($this->request->data['Seo']);

				$this->Session->setFlash('Registro agregado correctamente.', null, array(), 'success');
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash('Error al guardar el registro. Por favor intenta nuevamente.', null, array(), 'danger');
			}
		}
		$administradores	= $this->Receta->Administrador->find('list');
		$cortes	= $this->Receta->Corte->find('list');
		$recetas				= $this->Receta->find('list', array('conditions' => array('Receta.activo' => true),'order' => array('Receta.nombre' => 'ASC')));
		$this->set(compact('administradores', 'cortes', 'recetas'));
	}

	public function admin_edit($id = null)
	{
		// INSTANCIA CON EL MODULO
		$this->Seo 		 	= ClassRegistry::init('Seo');

		if ( ! $this->Receta->exists($id) )
		{
			$this->Session->setFlash('Registro invÃ¡lido.', null, array(), 'danger');
			$this->redirect(array('action' => 'index'));
		}

		if ( $this->request->is('post') || $this->request->is('put') )
		{
			// ELIMINAR LOS DATOS DE NUTRIENTES
			$this->Receta->IngredienteReceta->deleteAll(array('Receta.id' => $this->request->data['Receta']['id']), false);

			// RECOMENDAR RECETA
			if ( ! empty($this->request->data['RecomendarReceta']) ){

				$recomendar_receta = '';
				foreach ($this->request->data['RecomendarReceta'] as $key => $opcion) {
					if ( $key > 0 ){ $recomendar_receta .=','; }
					$recomendar_receta .= $opcion;
				}
				$this->request->data['Receta']['recomendar_receta'] = $recomendar_receta;
			}

			if ( $this->Receta->saveAll($this->request->data) )
			{
				$this->Seo->save($this->request->data['Seo']);
				$this->Session->setFlash('Registro editado correctamente', null, array(), 'success');
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash('Error al guardar el registro. Por favor intenta nuevamente.', null, array(), 'danger');
			}
		}
		else
		{
			$this->request->data	= $this->Receta->find('first', array(
				'conditions'	=> array('Receta.id' => $id),
				'contain'		=> array('IngredienteReceta')
			));
		}
		$administradores		= $this->Receta->Administrador->find('list');
		$cortes					= $this->Receta->Corte->find('list');
		$recetas				= $this->Receta->find('list', array('conditions' => array('Receta.activo' => true),'order' => array('Receta.nombre' => 'ASC')));
		$seo_producto 			= $this->Seo->obtenerSEO('Receta', $id);
		$recomendar_recetas		= $this->Receta->obtenerReceraRecomendada($id);

		$this->set(compact('administradores', 'cortes', 'recetas', 'seo_producto', 'recomendar_recetas'));
	}

	public function admin_delete($id = null)
	{
		$this->Receta->id = $id;
		if ( ! $this->Receta->exists() )
		{
			$this->Session->setFlash('Registro invÃ¡lido.', null, array(), 'danger');
			$this->redirect(array('action' => 'index'));
		}

		$this->request->onlyAllow('post', 'delete');
		if ( $this->Receta->delete() )
		{
			$this->Session->setFlash('Registro eliminado correctamente.', null, array(), 'success');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash('Error al eliminar el registro. Por favor intenta nuevamente.', null, array(), 'danger');
		$this->redirect(array('action' => 'index'));
	}

	public function admin_exportar()
	{
		$datos			= $this->Receta->find('all', array(
			'recursive'				=> -1
		));
		$campos			= array_keys($this->Receta->_schema);
		$modelo			= $this->Receta->alias;

		$this->set(compact('datos', 'campos', 'modelo'));
	}

	/**
	 * [index description]
	 * @return [type] [description]
	 */
	public function index()
	{
		// RECETA DESTACADA
		$destacado 			=	$this->Receta->recetaDestacada();

		// LISTA DE RECETAS
		$this->paginate		= array(
			'conditions'		=> array(
				'Receta.activo'		=>	true,
				'Receta.destacado'	=>	false
			),
			'limit'				=> 99
		);
		$recetas	= $this->paginate();

		$this->set(compact('destacado', 'recetas'));
	}

	/**
	 * [view description]
	 * @param  [type] $receta_id [description]
	 * @return [type]            [description]
	 */
	public function view( $receta_id = null )
	{
		if ( $receta_id ){

			// INFORMACION DE LA RECETA
			$receta 	=	$this->Receta->find('first', array(
				'conditions'	=> 	array(
					'Receta.id'	=> $receta_id
				),
				'contain'		=> array(
					'IngredienteReceta'
				)
			));

			$this->set(compact('receta'));

		}else{
			$this->redirect(array('action' => 'index'));
		}
	}
}
