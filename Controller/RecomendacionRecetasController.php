<?php
App::uses('AppController', 'Controller');
class RecomendacionRecetasController extends AppController
{
	public function admin_index()
	{
		$this->paginate		= array(
			'recursive'			=> 0
		);
		$recomendacionRecetas	= $this->paginate();
		$this->set(compact('recomendacionRecetas'));
	}

	public function admin_add()
	{
		if ( $this->request->is('post') )
		{
			$this->RecomendacionReceta->create();
			if ( $this->RecomendacionReceta->save($this->request->data) )
			{
				$this->Session->setFlash('Registro agregado correctamente.', null, array(), 'success');
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash('Error al guardar el registro. Por favor intenta nuevamente.', null, array(), 'danger');
			}
		}
		$recetas	= $this->RecomendacionReceta->Receta->find('list');
		$this->set(compact('recetas'));
	}

	public function admin_edit($id = null)
	{
		if ( ! $this->RecomendacionReceta->exists($id) )
		{
			$this->Session->setFlash('Registro inválido.', null, array(), 'danger');
			$this->redirect(array('action' => 'index'));
		}

		if ( $this->request->is('post') || $this->request->is('put') )
		{
			if ( $this->RecomendacionReceta->save($this->request->data) )
			{
				$this->Session->setFlash('Registro editado correctamente', null, array(), 'success');
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash('Error al guardar el registro. Por favor intenta nuevamente.', null, array(), 'danger');
			}
		}
		else
		{
			$this->request->data	= $this->RecomendacionReceta->find('first', array(
				'conditions'	=> array('RecomendacionReceta.id' => $id)
			));
		}
		$recetas	= $this->RecomendacionReceta->Receta->find('list');
		$this->set(compact('recetas'));
	}

	public function admin_delete($id = null)
	{
		$this->RecomendacionReceta->id = $id;
		if ( ! $this->RecomendacionReceta->exists() )
		{
			$this->Session->setFlash('Registro inválido.', null, array(), 'danger');
			$this->redirect(array('action' => 'index'));
		}

		$this->request->onlyAllow('post', 'delete');
		if ( $this->RecomendacionReceta->delete() )
		{
			$this->Session->setFlash('Registro eliminado correctamente.', null, array(), 'success');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash('Error al eliminar el registro. Por favor intenta nuevamente.', null, array(), 'danger');
		$this->redirect(array('action' => 'index'));
	}

	public function admin_exportar()
	{
		$datos			= $this->RecomendacionReceta->find('all', array(
			'recursive'				=> -1
		));
		$campos			= array_keys($this->RecomendacionReceta->_schema);
		$modelo			= $this->RecomendacionReceta->alias;

		$this->set(compact('datos', 'campos', 'modelo'));
	}
}
