<?php
App::uses('AppController', 'Controller');
class RecomendacionesController extends AppController
{
	public function admin_index()
	{
		$this->paginate		= array(
			'recursive'			=> 0
		);
		$recomendaciones	= $this->paginate();
		$this->set(compact('recomendaciones'));
	}

	public function admin_add()
	{
		if ( $this->request->is('post') )
		{
			$this->Recomendacion->create();
			if ( $this->Recomendacion->save($this->request->data) )
			{
				$this->Session->setFlash('Registro agregado correctamente.', null, array(), 'success');
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash('Error al guardar el registro. Por favor intenta nuevamente.', null, array(), 'danger');
			}
		}
		$cortes	= $this->Recomendacion->Corte->find('list');
		$productos	= $this->Recomendacion->Producto->find('list');
		$tipoRecomendaciones	= $this->Recomendacion->TipoRecomendacion->find('list');
		$this->set(compact('cortes', 'productos', 'tipoRecomendaciones'));
	}

	public function admin_edit($id = null)
	{
		if ( ! $this->Recomendacion->exists($id) )
		{
			$this->Session->setFlash('Registro inválido.', null, array(), 'danger');
			$this->redirect(array('action' => 'index'));
		}

		if ( $this->request->is('post') || $this->request->is('put') )
		{
			if ( $this->Recomendacion->save($this->request->data) )
			{
				$this->Session->setFlash('Registro editado correctamente', null, array(), 'success');
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash('Error al guardar el registro. Por favor intenta nuevamente.', null, array(), 'danger');
			}
		}
		else
		{
			$this->request->data	= $this->Recomendacion->find('first', array(
				'conditions'	=> array('Recomendacion.id' => $id)
			));
		}
		$cortes	= $this->Recomendacion->Corte->find('list');
		$productos	= $this->Recomendacion->Producto->find('list');
		$tipoRecomendaciones	= $this->Recomendacion->TipoRecomendacion->find('list');
		$this->set(compact('cortes', 'productos', 'tipoRecomendaciones'));
	}

	public function admin_delete($id = null)
	{
		$this->Recomendacion->id = $id;
		if ( ! $this->Recomendacion->exists() )
		{
			$this->Session->setFlash('Registro inválido.', null, array(), 'danger');
			$this->redirect(array('action' => 'index'));
		}

		$this->request->onlyAllow('post', 'delete');
		if ( $this->Recomendacion->delete() )
		{
			$this->Session->setFlash('Registro eliminado correctamente.', null, array(), 'success');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash('Error al eliminar el registro. Por favor intenta nuevamente.', null, array(), 'danger');
		$this->redirect(array('action' => 'index'));
	}

	public function admin_exportar()
	{
		$datos			= $this->Recomendacion->find('all', array(
			'recursive'				=> -1
		));
		$campos			= array_keys($this->Recomendacion->_schema);
		$modelo			= $this->Recomendacion->alias;

		$this->set(compact('datos', 'campos', 'modelo'));
	}
}
