<?php
App::uses('AppController', 'Controller');
class SeosController extends AppController
{
	public function admin_index()
	{
		$this->paginate		= array(
			'recursive'			=> 0
		);
		$seos	= $this->paginate();
		$this->set(compact('seos'));
	}

	public function admin_add($modulo_seo = null, $modulo_id = null, $redirect_controller = null, $redirect_action = null, $redirect_param = null)
	{
		if ( $this->request->is('post') )
		{
			$this->Seo->create();
			if ( $this->Seo->save($this->request->data) )
			{
				$this->Session->setFlash('Datos del SEO agregado correctamente.', null, array(), 'success');
				$this->redirect(array('controller' => $redirect_controller,'action' => $redirect_action));
			}
			else
			{
				$this->Session->setFlash('Error al guardar el registro. Por favor intenta nuevamente.', null, array(), 'danger');
				$this->redirect(array('controller' => $redirect_controller,'action' => $redirect_action));
			}
		}

		$this->set(compact('modulo_seo', 'modulo_id', 'redirect_controller', 'redirect_action', 'redirect_param'));
	}

	public function admin_edit($id = null, $redirect_controller = null, $redirect_action = null, $redirect_param = null)
	{
		if ( ! $this->Seo->exists($id) )
		{
			$this->Session->setFlash('Registro inválido.', null, array(), 'danger');
			$this->redirect(array('action' => 'index'));
		}

		if ( $this->request->is('post') || $this->request->is('put') )
		{
			if ( $this->Seo->save($this->request->data) )
			{
				$this->Session->setFlash('Datos del SEO editado correctamente', null, array(), 'success');
				$this->redirect(array('controller' => $redirect_controller,'action' => $redirect_action));
			}
			else
			{
				$this->Session->setFlash('Error al guardar el registro. Por favor intenta nuevamente.', null, array(), 'danger');
				$this->redirect(array('controller' => $redirect_controller,'action' => $redirect_action));
			}
		}
		else
		{
			$this->request->data	= $this->Seo->find('first', array(
				'conditions'	=> array('Seo.id' => $id)
			));
		}

		$this->set(compact('redirect_controller', 'redirect_action', 'redirect_param'));
	}

	public function admin_delete($id = null)
	{
		$this->Seo->id = $id;
		if ( ! $this->Seo->exists() )
		{
			$this->Session->setFlash('Registro inválido.', null, array(), 'danger');
			$this->redirect(array('action' => 'index'));
		}

		$this->request->onlyAllow('post', 'delete');
		if ( $this->Seo->delete() )
		{
			$this->Session->setFlash('Registro eliminado correctamente.', null, array(), 'success');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash('Error al eliminar el registro. Por favor intenta nuevamente.', null, array(), 'danger');
		$this->redirect(array('action' => 'index'));
	}

	public function admin_exportar()
	{
		$datos			= $this->Seo->find('all', array(
			'recursive'				=> -1
		));
		$campos			= array_keys($this->Seo->_schema);
		$modelo			= $this->Seo->alias;

		$this->set(compact('datos', 'campos', 'modelo'));
	}
}
