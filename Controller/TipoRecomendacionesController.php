<?php
App::uses('AppController', 'Controller');
class TipoRecomendacionesController extends AppController
{
	public function admin_index()
	{
		$this->paginate		= array(
			'recursive'			=> 0
		);
		$tipoRecomendaciones	= $this->paginate();
		$this->set(compact('tipoRecomendaciones'));
	}

	public function admin_add()
	{
		if ( $this->request->is('post') )
		{
			$this->TipoRecomendacion->create();
			if ( $this->TipoRecomendacion->save($this->request->data) )
			{
				$this->Session->setFlash('Registro agregado correctamente.', null, array(), 'success');
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash('Error al guardar el registro. Por favor intenta nuevamente.', null, array(), 'danger');
			}
		}
		$administradores	= $this->TipoRecomendacion->Administrador->find('list');
		$this->set(compact('administradores'));
	}

	public function admin_edit($id = null)
	{
		if ( ! $this->TipoRecomendacion->exists($id) )
		{
			$this->Session->setFlash('Registro inválido.', null, array(), 'danger');
			$this->redirect(array('action' => 'index'));
		}

		if ( $this->request->is('post') || $this->request->is('put') )
		{
			if ( $this->TipoRecomendacion->save($this->request->data) )
			{
				$this->Session->setFlash('Registro editado correctamente', null, array(), 'success');
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash('Error al guardar el registro. Por favor intenta nuevamente.', null, array(), 'danger');
			}
		}
		else
		{
			$this->request->data	= $this->TipoRecomendacion->find('first', array(
				'conditions'	=> array('TipoRecomendacion.id' => $id)
			));
		}
		$administradores	= $this->TipoRecomendacion->Administrador->find('list');
		$this->set(compact('administradores'));
	}

	public function admin_delete($id = null)
	{
		$this->TipoRecomendacion->id = $id;
		if ( ! $this->TipoRecomendacion->exists() )
		{
			$this->Session->setFlash('Registro inválido.', null, array(), 'danger');
			$this->redirect(array('action' => 'index'));
		}

		$this->request->onlyAllow('post', 'delete');
		if ( $this->TipoRecomendacion->delete() )
		{
			$this->Session->setFlash('Registro eliminado correctamente.', null, array(), 'success');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash('Error al eliminar el registro. Por favor intenta nuevamente.', null, array(), 'danger');
		$this->redirect(array('action' => 'index'));
	}

	public function admin_exportar()
	{
		$datos			= $this->TipoRecomendacion->find('all', array(
			'recursive'				=> -1
		));
		$campos			= array_keys($this->TipoRecomendacion->_schema);
		$modelo			= $this->TipoRecomendacion->alias;

		$this->set(compact('datos', 'campos', 'modelo'));
	}
}
