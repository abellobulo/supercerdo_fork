<?php
App::uses('AppController', 'Controller');
class ValoracionesController extends AppController
{
	public function admin_index()
	{
		$this->paginate		= array(
			'recursive'			=> 0
		);
		$valoraciones	= $this->paginate();
		$this->set(compact('valoraciones'));
	}

	public function admin_add()
	{
		if ( $this->request->is('post') )
		{
			$this->Valoracion->create();
			if ( $this->Valoracion->save($this->request->data) )
			{
				$this->Session->setFlash('Registro agregado correctamente.', null, array(), 'success');
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash('Error al guardar el registro. Por favor intenta nuevamente.', null, array(), 'danger');
			}
		}
		$productos	= $this->Valoracion->Producto->find('list');
		$administradores	= $this->Valoracion->Administrador->find('list');
		$recetas	= $this->Valoracion->Receta->find('list');
		$this->set(compact('productos', 'administradores', 'recetas'));
	}

	public function admin_edit($id = null)
	{
		if ( ! $this->Valoracion->exists($id) )
		{
			$this->Session->setFlash('Registro inválido.', null, array(), 'danger');
			$this->redirect(array('action' => 'index'));
		}

		if ( $this->request->is('post') || $this->request->is('put') )
		{
			if ( $this->Valoracion->save($this->request->data) )
			{
				$this->Session->setFlash('Registro editado correctamente', null, array(), 'success');
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash('Error al guardar el registro. Por favor intenta nuevamente.', null, array(), 'danger');
			}
		}
		else
		{
			$this->request->data	= $this->Valoracion->find('first', array(
				'conditions'	=> array('Valoracion.id' => $id)
			));
		}
		$productos	= $this->Valoracion->Producto->find('list');
		$administradores	= $this->Valoracion->Administrador->find('list');
		$recetas	= $this->Valoracion->Receta->find('list');
		$this->set(compact('productos', 'administradores', 'recetas'));
	}

	public function admin_delete($id = null)
	{
		$this->Valoracion->id = $id;
		if ( ! $this->Valoracion->exists() )
		{
			$this->Session->setFlash('Registro inválido.', null, array(), 'danger');
			$this->redirect(array('action' => 'index'));
		}

		$this->request->onlyAllow('post', 'delete');
		if ( $this->Valoracion->delete() )
		{
			$this->Session->setFlash('Registro eliminado correctamente.', null, array(), 'success');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash('Error al eliminar el registro. Por favor intenta nuevamente.', null, array(), 'danger');
		$this->redirect(array('action' => 'index'));
	}

	public function admin_exportar()
	{
		$datos			= $this->Valoracion->find('all', array(
			'recursive'				=> -1
		));
		$campos			= array_keys($this->Valoracion->_schema);
		$modelo			= $this->Valoracion->alias;

		$this->set(compact('datos', 'campos', 'modelo'));
	}
}
