<?php
App::uses('AppModel', 'Model');
class Banner extends AppModel
{
	/**
	 * CONFIGURACION DB
	 */

	/**
	 * BEHAVIORS
	 */
	var $actsAs			= array(
		/**
		 * IMAGE UPLOAD
		 */
		
		'Image'		=> array(
			'fields'	=> array(
				'imagen'	=> array(
					'versions'	=> array(
						array(
							'prefix'	=> 'mini',
							'width'		=> 100,
							'height'	=> 100,
							'crop'		=> true
						),
						array(
							'prefix'	=> 'admin',
							'width'		=> 250,
							'height'	=> 150,
							'crop'		=> true
						)
					)
				)
			)
		)
		
	);

	/**
	 * VALIDACIONES
	 */

	/**
	 * ASOCIACIONES
	 */
	public $belongsTo = array(
		'Administrador' => array(
			'className'				=> 'Administrador',
			'foreignKey'			=> 'administrador_id',
			'conditions'			=> '',
			'fields'				=> '',
			'order'					=> '',
			'counterCache'			=> true,
			//'counterScope'			=> array('Asociado.modelo' => 'Administrador')
		)
	);

	public function beforeSave($options = array())
	{
		$this->data[$this->alias]['administrador_id']        = AuthComponent::user('id');
		return true;
	}

	/**
	 * [obtenerBanner description]
	 * @param  [type] $modulo [description]
	 * @return [type]         [description]
	 */
	public function obtenerBanner( $modulo = null, $activo = 'Null' )
	{
		if ( $modulo ){
			$conditions['Banner.modulo'] = $modulo;
			$conditions['Banner.tipo'] = 'Null';
			if ( $activo >= 1 ){
				$conditions['Banner.activo'] = ( $activo == 2 ? 0 : $activo);
			}
			$banners 	=	$this->find('all', array(
				'conditions'	=> $conditions,
				'order'			=> array(
					'Banner.id'		=> 'DESC'
				)
			));

			return $banners;

		}

		return false;
	}

	/**
	 * [obtenerCatidadBanner description]
	 * @param  [type]  $modulo [description]
	 * @param  integer $estado [description]
	 * @return [type]          [description]
	 */
	public function obtenerCatidadBanner( $modulo = null, $activo = 'Null')
	{
		if ( $modulo ){
			$conditions['Banner.modulo'] 	= $modulo;
			$conditions['Banner.tipo'] 		= 'Null';
			if ( $activo != 'Null' ){
				$conditions['Banner.activo'] = $activo;
			}

			$catidad_banner 	= $this->find('count', array(
				'conditions'		=> $conditions
			));

			return $catidad_banner;

		}

		return false;
	}

	/**
	 * [obtenerBannerFondo description]
	 * @param  [type] $modulo [description]
	 * @return [type]         [description]
	 */
	public function obtenerBannerFondo( $modulo = null )
	{
		if ( $modulo ){

			$banner_fondo	=	$this->find('first', array(
				'conditions'		=> array(
					'Banner.activo'		=> 	true,
					'Banner.tipo'		=>	'fondo',
					'Banner.modulo'		=>	$modulo
				)
			));

			return $banner_fondo;
		}

		return false;
	}
}
