<?php
App::uses('AppModel', 'Model');
class Campania extends AppModel
{
	/**
	 * CONFIGURACION DB
	 */
	public $useTable		= 'campania';
	public $displayField	= 'nombre';

	/**
	 * BEHAVIORS
	 */
	var $actsAs			= array(
		/**
		 * IMAGE UPLOAD
		 */
		
		'Image'		=> array(
			'fields'	=> array(
				'imagen'	=> array(
					'versions'	=> array(
						array(
							'prefix'	=> 'mini',
							'width'		=> 100,
							'height'	=> 100,
							'crop'		=> true
						),
						array(
							'prefix'	=> 'catalogo',
							'width'		=> 760,
							'height'	=> 380,
							'crop'		=> true
						),
						array(
							'prefix'	=> 'admin',
							'width'		=> 250,
							'height'	=> 150,
							'crop'		=> true
						)
					)
				)
			)
		)
		
	);

	/**
	 * VALIDACIONES
	 */

	/**
	 * ASOCIACIONES
	 */
	public $belongsTo = array(
		'Administrador' => array(
			'className'				=> 'Administrador',
			'foreignKey'			=> 'administrador_id',
			'conditions'			=> '',
			'fields'				=> '',
			'order'					=> '',
			'counterCache'			=> true,
			//'counterScope'			=> array('Asociado.modelo' => 'Administrador')
		)
	);

	public function beforeSave($options = array())
    {
        parent::beforeSave($options);
        /**
         * Actualiza el usuario que crea o modifica 
         */
        if ( ! isset($this->data[$this->alias]['administrador_id']) )
        {
            $this->data[$this->alias]['administrador_id']        = AuthComponent::user('id');
        }

        return true;
    }
}
