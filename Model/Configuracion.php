<?php
App::uses('AppModel', 'Model');
class Configuracion extends AppModel
{
	/**
	 * CONFIGURACION DB
	 */

	/**
	 * BEHAVIORS
	 */
	var $actsAs			= array(
		/**
		 * IMAGE UPLOAD
		 */
		
		'Image'		=> array(
			'fields'	=> array(
				'imagen'	=> array(
					'versions'	=> array(
						array(
							'prefix'	=> 'mini',
							'width'		=> 100,
							'height'	=> 100,
							'crop'		=> true
						),
						array(
							'prefix'	=> 'admin',
							'width'		=> 250,
							'height'	=> 150,
							'crop'		=> true
						),
						array(
							'prefix'	=> 'catalogo',
							'width'		=> 250,
							'height'	=> 170,
							'crop'		=> true
						),
						array(
							'prefix'	=> 'destacado',
							'width'		=> 500,
							'height'	=> 450,
							'crop'		=> true
						)
					)
				)
			)
		)
		
	);

	/**
	 * VALIDACIONES
	 */

	/**
	 * METODOS
	 */

	public function beforeSave($options = array())
    {
        parent::beforeSave($options);
        /**
         * Actualiza el usuario que crea o modifica el producto
         */
        if ( ! isset($this->data[$this->alias]['administrador_id']) )
        {
            $this->data[$this->alias]['administrador_id']        = AuthComponent::user('id');
        }

        return true;
    }
}
