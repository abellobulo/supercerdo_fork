<?php
App::uses('AppModel', 'Model');
class Producto extends AppModel
{
	/**
	 * CONFIGURACION DB
	 */
	public $displayField	= 'nombre';

	/**
	 * BEHAVIORS
	 */
	var $actsAs			= array(
		/**
		 * IMAGE UPLOAD
		 */
		
		'Image'		=> array(
			'fields'	=> array(
				'imagen'	=> array(
					'versions'	=> array(
						array(
							'prefix'	=> 'mini',
							'width'		=> 100,
							'height'	=> 100,
							'crop'		=> false
						),
						array(
							'prefix'	=> 'admin',
							'width'		=> 250,
							'height'	=> 150,
							'crop'		=> false
						),
						array(
							'prefix'	=> 'catalogo',
							'width'		=> 250,
							'height'	=> 170,
							'crop'		=> false
						),
						array(
							'prefix'	=> 'destacado',
							'width'		=> 500,
							'height'	=> 450,
							'crop'		=> false
						)
					)
				),
				'imagen_banner'	=> array(
					'versions'	=> array(
						array(
							'prefix'	=> 'mini',
							'width'		=> 100,
							'height'	=> 100,
							'crop'		=> false
						),
						array(
							'prefix'	=> 'admin',
							'width'		=> 250,
							'height'	=> 150,
							'crop'		=> false
						),
						array(
							'prefix'	=> 'catalogo',
							'width'		=> 250,
							'height'	=> 170,
							'crop'		=> false
						)
					)
				)
			)
		)
		
	);

	/**
	 * VALIDACIONES
	 */

	/**
	 * ASOCIACIONES
	 */
	public $belongsTo = array(
		'Corte' => array(
			'className'				=> 'Corte',
			'foreignKey'			=> 'corte_id',
			'conditions'			=> '',
			'fields'				=> '',
			'order'					=> '',
			'counterCache'			=> true,
			//'counterScope'			=> array('Asociado.modelo' => 'Corte')
		),
		'Administrador' => array(
			'className'				=> 'Administrador',
			'foreignKey'			=> 'administrador_id',
			'conditions'			=> '',
			'fields'				=> '',
			'order'					=> '',
			'counterCache'			=> true,
			//'counterScope'			=> array('Asociado.modelo' => 'Administrador')
		),
		'LineaProducto' => array(
			'className'				=> 'LineaProducto',
			'foreignKey'			=> 'linea_producto_id',
			'conditions'			=> '',
			'fields'				=> '',
			'order'					=> '',
			'counterCache'			=> true,
			//'counterScope'			=> array('Asociado.modelo' => 'LineaProducto')
		)
	);
	public $hasMany = array(
		'Recomendacion' => array(
			'className'				=> 'Recomendacion',
			'foreignKey'			=> 'producto_id',
			'dependent'				=> false,
			'conditions'			=> '',
			'fields'				=> '',
			'order'					=> '',
			'limit'					=> '',
			'offset'				=> '',
			'exclusive'				=> '',
			'finderQuery'			=> '',
			'counterQuery'			=> ''
		),
		'Valoracion' => array(
			'className'				=> 'Valoracion',
			'foreignKey'			=> 'producto_id',
			'dependent'				=> false,
			'conditions'			=> '',
			'fields'				=> '',
			'order'					=> '',
			'limit'					=> '',
			'offset'				=> '',
			'exclusive'				=> '',
			'finderQuery'			=> '',
			'counterQuery'			=> ''
		),
		'NutricionalProducto' => array(
			'className'				=> 'NutricionalProducto',
			'foreignKey'			=> 'producto_id',
			'dependent'				=> false,
			'conditions'			=> '',
			'fields'				=> '',
			'order'					=> '',
			'limit'					=> '',
			'offset'				=> '',
			'exclusive'				=> '',
			'finderQuery'			=> '',
			'counterQuery'			=> ''
		)
	);

	/**
	 * [beforeSave description]
	 * @param  array  $options [description]
	 * @return [type]          [description]
	 */
	public function beforeSave($options = array())
    {
        parent::beforeSave($options);
        /**
         * Actualiza el usuario que crea o modifica el producto
         */
        if ( ! isset($this->data[$this->alias]['administrador_id']) )
        {
            $this->data[$this->alias]['administrador_id']        = AuthComponent::user('id');
        }

        if (  isset($this->data[$this->alias]['lineas']) ){
        	$this->data[$this->alias]['linea_producto_id']        = $this->data[$this->alias]['lineas'];
        }


        return true;
    }

    public function obtenerProducto( $productoID = null )
    {
    	if ( $productoID ){
			$producto 		=	$this->find('first', array(
	    		'conditions'	=> array(
	    			'Producto.id'	=>	$productoID
	    		)
	    	));
	    	return $producto;
    	}
    	return false;
    }

	/**
	 * [productoDestacado description]
	 * @return [type] [description]
	 */
    public function productoDestacado( $linea_producto_id = null )
    {
    	if ( $linea_producto_id ){
    		$producto 		=	$this->find('first', array(
	    		'conditions'	=> array(
	    			'Producto.destacado'			=>	true,
	    			'Producto.linea_producto_id'	=>	$linea_producto_id
	    		)
	    	));

	    	return $producto;
    	}
    	
    	return false;
    	
    }

    /**
     * [asignarProductoDestacado description]
     * @param  [type] $producto_id [description]
     * @return [type]              [description]
     */
    public function asignarProductoDestacado( $producto_id = null, $linea_id = null )
    {
    	if ( $producto_id ){

    		$this->updateAll(
			    array('Producto.destacado' 			=> false),
			    array('Producto.linea_producto_id' 	=> $linea_id)
			);

			$data = array(
				'id'		=>	$producto_id,
				'destacado'	=>	true
			);
			// Se reasigna el producto destacado
			if ( $this->save($data) ){
				return true;
			}
    	}

    	return false;
    }

    /**
     * [obtenerRecomendadoHacer description]
     * @param  [type] $producto_id [description]
     * @return [type]              [description]
     */
    public function obtenerRecomendacionesProducto( $producto_id = null, $tipo = null )
    {
    	if ( $producto_id ){

    		$productoParaHacer = array();
    		$producto 	= $this->find('first', array(
    			'conditions'	=>	array(
    				'Producto.id'	=>	$producto_id
    			),
    			'fields'		=>	array(
    				'Producto.id', 'Producto.'.$tipo
    			)
    		));
			$valor_array = explode(',',$producto['Producto'][$tipo]); 
			foreach($valor_array as $llave => $valores) { 
			    array_push($productoParaHacer, $valores) ;
			}

			return $productoParaHacer;

    	}
    	return false;
    }

    /**
     * [obtenerCantidadProductoLinea description]
     * @param  [type] $linea [description]
     * @return [type]        [description]
     */
    public function obtenerCantidadProductoLinea( $linea = null )
    {
    	if ( $linea ){

    		$cantidad_producto =	$this->find('count', array(
    			'conditions'		=> array(
    				'Producto.linea_producto_id'	=> $linea
    			)
    		));

    		return $cantidad_producto;
    	}

    	return false;
    }

}
