<?php
App::uses('AppModel', 'Model');
class Receta extends AppModel
{
	/**
	 * CONFIGURACION DB
	 */
	public $displayField	= 'nombre';

	/**
	 * BEHAVIORS
	 */
	var $actsAs			= array(
		/**
		 * IMAGE UPLOAD
		 */
		
		'Image'		=> array(
			'fields'	=> array(
				'imagen'	=> array(
					'versions'	=> array(
						array(
							'prefix'	=> 'mini',
							'width'		=> 100,
							'height'	=> 100,
							'crop'		=> true
						),
						array(
							'prefix'	=> 'admin',
							'width'		=> 250,
							'height'	=> 150,
							'crop'		=> true
						),
						array(
							'prefix'	=> 'catalogo',
							'width'		=> 268,
							'height'	=> 278,
							'crop'		=> true
						),
						array(
							'prefix'	=> 'destacado',
							'width'		=> 500,
							'height'	=> 450,
							'crop'		=> true
						)
					)
				),
				'imagen_banner'	=> array(
					'versions'	=> array(
						array(
							'prefix'	=> 'mini',
							'width'		=> 100,
							'height'	=> 100,
							'crop'		=> true
						),
						array(
							'prefix'	=> 'admin',
							'width'		=> 250,
							'height'	=> 150,
							'crop'		=> true
						),
						array(
							'prefix'	=> 'catalogo',
							'width'		=> 250,
							'height'	=> 170,
							'crop'		=> true
						)
					)
				)
			)
		)
		
	);

	/**
	 * VALIDACIONES
	 */

	/**
	 * ASOCIACIONES
	 */
	public $belongsTo = array(
		'Administrador' => array(
			'className'				=> 'Administrador',
			'foreignKey'			=> 'administrador_id',
			'conditions'			=> '',
			'fields'				=> '',
			'order'					=> '',
			'counterCache'			=> true,
			//'counterScope'			=> array('Asociado.modelo' => 'Administrador')
		)
	);
	public $hasMany = array(
		'Valoracion' => array(
			'className'				=> 'Valoracion',
			'foreignKey'			=> 'receta_id',
			'dependent'				=> false,
			'conditions'			=> '',
			'fields'				=> '',
			'order'					=> '',
			'limit'					=> '',
			'offset'				=> '',
			'exclusive'				=> '',
			'finderQuery'			=> '',
			'counterQuery'			=> ''
		),
		'IngredienteReceta' => array(
			'className'				=> 'IngredienteReceta',
			'foreignKey'			=> 'receta_id',
			'dependent'				=> false,
			'conditions'			=> '',
			'fields'				=> '',
			'order'					=> '',
			'limit'					=> '',
			'offset'				=> '',
			'exclusive'				=> '',
			'finderQuery'			=> '',
			'counterQuery'			=> ''
		)
	);
	public $hasAndBelongsToMany = array(
		'Corte' => array(
			'className'				=> 'Corte',
			'joinTable'				=> 'cortes_recetas',
			'foreignKey'			=> 'receta_id',
			'associationForeignKey'	=> 'corte_id',
			'unique'				=> true,
			'conditions'			=> '',
			'fields'				=> '',
			'order'					=> '',
			'limit'					=> '',
			'offset'				=> '',
			'finderQuery'			=> '',
			'deleteQuery'			=> '',
			'insertQuery'			=> ''
		)
	);

	public function beforeSave($options = array())
    {
        parent::beforeSave($options);
        /**
         * Actualiza el usuario que crea o modifica 
         */
        if ( ! isset($this->data[$this->alias]['administrador_id']) )
        {
            $this->data[$this->alias]['administrador_id']        = AuthComponent::user('id');
        }

        return true;
    }

    /**
     * [recetaDestacada description]
     * @return [type] [description]
     */
    public function recetaDestacada()
    {
    	$receta 		=	$this->find('first', array(
    		'conditions'	=> array(
    			'Receta.destacado'	=> true
    		)
    	));

    	return $receta;
    }

    /**
     * [asignarRecetaDestacada description]
     * @param  [type] $receta_id [description]
     * @return [type]            [description]
     */
    public function asignarRecetaDestacada( $receta_id = null )
    {
    	if ( $receta_id ){

    		$this->updateAll(
			    array('Receta.destacado' => false)
			);

			$data = array(
				'id'		=>	$receta_id,
				'destacado'	=>	true
			);
			// Se reasigna la receta destacado
			if ( $this->save($data) ){
				return true;
			}
    	}

    	return false;
    }

    /**
     * [obtenerReceraRecomendada description]
     * @param  [type] $receta_id [description]
     * @return [type]            [description]
     */
    public function obtenerReceraRecomendada( $receta_id = null )
    {
    	if ( $receta_id ){

    		$recomendar_receta = array();
    		$receta 	= $this->find('first', array(
    			'conditions'	=>	array(
    				'Receta.id'	=>	$receta_id
    			),
    			'fields'		=>	array(
    				'Receta.id', 'Receta.recomendar_receta'
    			)
    		));
			$valor_array = explode(',',$receta['Receta']['recomendar_receta']); 
			foreach($valor_array as $llave => $valores) { 
			    array_push($recomendar_receta, $valores) ;
			}

			return $recomendar_receta;

    	}
    	return false;
    }
}
