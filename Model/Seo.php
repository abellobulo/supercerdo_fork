<?php
App::uses('AppModel', 'Model');
class Seo extends AppModel
{
	/**
	 * CONFIGURACION DB
	 */

	/**
	 * BEHAVIORS
	 */
	var $actsAs			= array(
		/**
		 * IMAGE UPLOAD
		 */
		/*
		'Image'		=> array(
			'fields'	=> array(
				'imagen'	=> array(
					'versions'	=> array(
						array(
							'prefix'	=> 'mini',
							'width'		=> 100,
							'height'	=> 100,
							'crop'		=> true
						)
					)
				)
			)
		)
		*/
	);

	/**
	 * VALIDACIONES
	 */
	

	/**
	 * [obtenerSEO description]
	 * @param  [type] $modulo    [description]
	 * @param  [type] $modulo_id [description]
	 * @return [type]            [description]
	 */
	public function obtenerSEO( $modulo = null, $modulo_id  = null)
	{
		if ( $modulo ){

			$conditions['Seo.modulo'] = $modulo;
			if ($modulo_id > 0 ){
				$conditions['Seo.modulo_id'] = $modulo_id;
			}

			$data_seo	=	$this->find('first', array(
				'conditions'	=> $conditions
			));

			return $data_seo;
		}

		return false;
	}
}
