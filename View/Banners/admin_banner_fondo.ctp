<!-- 
    * Admin Banner Fondo
    * Ene 2018
    * Controller: banners
    * Action: admin_banner_fondo
    * Admin: true
-->  

<?= $this->element('admin_breadcrumb'); ?>

<div class="page-title">
    <h2><span class="fa fa-list"></span> Banners</h2>
</div>


<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap admin-seccion-producto">
     <div class="row">
         <div class="col-md-12">

             <!-- START CONTENT FRAME -->
                <div class="content-frame">   
                    
                    <!-- START CONTENT FRAME TOP -->
                    <div class="content-frame-top">                        
                        <div class="page-title">                    
                            <h2><span class="fa fa-image"></span> Lista de Banners</h2>
                        </div>                                      
                    </div>

                
                    <!-- START CONTENT FRAME BODY -->
                     <div class="panel-body">

                        <div class="gallery" id="links">

                            <? foreach ($banner_fondo as $key => $banner) { 
                                $hr = ( $banner['Banner']['activo'] ? 'activo' : 'no-activo');
                                ?>

                                <div class="col-md-3 col-xs-12">
                                    <div class="row">
                                        <a class="gallery-item" href="<?= sprintf('%simg/%s',$this->webroot,$banner['Banner']['imagen']['path']) ?>" title="<?= $banner['Banner']['modulo'] ?>" data-gallery>
                                            <div class="image">
                                                <? if ( ! empty($banner['Banner']['imagen']) ) { ?> 
                                                     <?= $this->Html->image($banner['Banner']['imagen']['admin']); ?>
                                                <? } else { ?>
                                                    <?= $this->Html->image('template/SUPER_CERDO_250.jpg'); ?>
                                                <? } ?>
                                            </div>
                                        </a>

                                    </div>
                                    <div class="row">
                                        <div class="">
                                            <div class="meta">
                                                <div class="col-md-12 col-xs-12" >
                                                    <div class="titulo_catalogo">
                                                        <strong><?= $banner['Banner']['modulo'] ?></strong>
                                                    </div>
                                                    <span>Modificado: <?= formatoFecha($banner['Banner']['modified'],'dmy'); ?></span>
                                                    <hr class="<?= $hr ?>">
                                                </div>
                                                <div class="col-md-12 col-xs-12" style="text-align: center;">

                                                    <!-- EDITAR -->
                                                    <?= $this->Html->link('<i class="fa fa-pencil"></i>', array('controller' => 'Banners', 'action' => 'edit', $banner['Banner']['id'], $redirect_page['controller'], $redirect_page['action']), array('class' => 'btn btn-default btn-xs', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title' => 'Editar', 'escape' => false)); ?>

                                                </div>
                                            </div> 
                                        </div>
                                    </div>
                                </div>

                            <? } ?>

                        </div>

                        <div class="pull-right">
                            <ul class="pagination">
                                <?= $this->Paginator->prev('« Anterior', array('tag' => 'li'), null, array('tag' => 'li', 'disabledTag' => 'a', 'class' => 'first disabled hidden')); ?>
                                <?= $this->Paginator->numbers(array('tag' => 'li', 'currentTag' => 'a', 'modulus' => 2, 'currentClass' => 'active', 'separator' => '')); ?>
                                <?= $this->Paginator->next('Siguiente »', array('tag' => 'li'), null, array('tag' => 'li', 'disabledTag' => 'a', 'class' => 'last disabled hidden')); ?>
                            </ul>
                        </div>

                        
                    </div>
                </div>

                <!-- BLUEIMP GALLERY -->
                 <?= $this->element('admin_blueimp_gallery'); ?>
                <!-- END BLUEIMP GALLERY -->

          </div>
     </div>
</div>    




