<!-- 
	Model: Banner
	Description: Archivo que permite modificar los datos de un banner determinado por su ID
	Author: Ukko - sso
	Date: Ene 2018
	Version: 1.0
-->

<div class="page-title">
	<h2><span class="fa fa-list"></span> Banners</h2>
</div>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">Editar Banner</h3>
	</div>
	<div class="panel-body form-group-separated">
			<?= $this->Form->create('Banner', array('class' => 'form-horizontal', 'type' => 'file', 'inputDefaults' => array('label' => false, 'div' => false, 'class' => 'form-control'))); ?>

				<?= $this->Form->input('id'); ?>

					<div class="form-group">
		                <label class="col-md-3 col-xs-12 control-label">Banner Actual</label>
		                <div class="col-md-6 col-xs-12">                                            
		                    <div class="input-group">
		                        <?= $this->Html->image($this->request->data['Banner']['imagen']['admin'] ) ?>
		                    </div>                                            
		                </div>
		            </div>


				 	<div class="form-group">
		                <label class="col-md-3 col-xs-12 control-label">Banner (1920 x 705)</label>
		                <div class="col-md-6 col-xs-12">                                            
		                    <div class="input-group">
		                        <?= $this->Form->input('imagen', array('type' => 'file', 'class'=>'file', 'data-preview-file-type' => 'any', 'multiple')); ?>
		                    </div>                                            
		                    <span class="help-block">Para el Banner se recomienda utilizar extensión .jpg y no mayor a 500KB</span>
		                </div>
		            </div>

		            <? if ($redirect_action != 'bannerFondo') {  ?>

			            <div class="form-group">
			                <label class="col-md-3 col-xs-12 control-label">Titulo</label>
			                <div class="col-md-6 col-xs-12">                                            
			                    <?= $this->Form->input('bajada', array('class' => 'summernote')); ?>
			                </div>
			            </div>

			             <div class="form-group">
			                <label class="col-md-3 col-xs-12 control-label">Link</label>
			                <div class="col-md-6 col-xs-12">                                            
			                    <div class="input-group">
			                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
			                        <?= $this->Form->input('link'); ?>
			                    </div>                                            
			                </div>
			            </div>

			       <? }  ?>


				<div class="pull-right">
					<input type="submit" class="btn btn-primary esperar-carga" autocomplete="off" data-loading-text="Espera un momento..." value="Guardar cambios">
					<?= $this->Html->link('Cancelar', array('controller' => $redirect_controller, 'action' => $redirect_action), array('class' => 'btn btn-danger')); ?>
				</div>
			<?= $this->Form->end(); ?>
	</div>
</div>
