<div class="page-title">
	<h2><span class="fa fa-list"></span> Campañas</h2>
</div>

<div class="page-content-wrap">
    <div class="row">
        <div class="col-md-12">
        	<?= $this->Form->create('Campania', array('class' => 'form-horizontal', 'type' => 'file', 'inputDefaults' => array('label' => false, 'div' => false, 'class' => 'form-control'))); ?>

        		<div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><span class="fa fa-mail-forward"></span><strong> Nueva</strong> Campaña </h3>
                    </div>
                    <div class="panel-body">
                        <p>Para la imagen se recomienda utilizar extensiones .png y jpg no mayor a <code>500 KB</code>.</p>
                    </div>
                    <div class="panel-body form-group-separated">   

                    	 <div class="row">
                            
                            <div class="col-md-12">

                               <div class="form-group">
                                    <label class="col-md-3 col-xs-12 control-label">Nombre</label>
                                    <div class="col-md-6 col-xs-12">                                
                                    	<?= $this->Form->input('nombre', array('class' => 'form-control')); ?>
                                    </div>
                                </div>

                                 <div class="form-group">
                                    <label class="col-md-3 col-xs-12 control-label">Bajada</label>
                                    <div class="col-md-6 col-xs-12">                                
                                    	<?= $this->Form->input('bajada', array('class' => 'form-control')); ?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 col-xs-12 control-label">ID Youtube</label>
                                    <div class="col-md-6 col-xs-12">                                
                                        <?= $this->Form->input('link', array('class' => 'form-control')); ?>
                                        <span class="help-block">EJEMPLO: www.youtube.com/watch?v=-aM7F6Au8AE =>  El ID es <code>-aM7F6Au8AE</code></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 col-xs-12 control-label">Imagen (760 x 380)</label>
                                    <div class="col-md-6 col-xs-12">                                
                                    	<?= $this->Form->input('imagen', array('type' => 'file', 'class'=>'file', 'data-preview-file-type' => 'any', 'multiple')); ?>
                                    	 <span class="help-block">Esta imagen se utilizará como referencia del video</span>
                                    </div>
                                    
                                </div>

                            </div>
                            
                        </div>

                    </div>
                    <div class="panel-footer">
                        <div class="pull-right">
							<input type="submit" class="btn btn-primary esperar-carga" autocomplete="off" data-loading-text="Espera un momento..." value="Guardar cambios">
							<?= $this->Html->link('Cancelar', array('action' => 'index'), array('class' => 'btn btn-danger')); ?>
						</div>
                    </div>
                </div>

        	<?= $this->Form->end(); ?>
        </div>
    </div>
</div>


