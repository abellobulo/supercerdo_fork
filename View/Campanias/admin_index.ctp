<!-- 
	* Admin Index Campanias
	* Ene 2018
	* Controller: noticias
	* Action: admin_index
	* Admin: true
-->  

<?= $this->element('admin_breadcrumb'); ?>

<div class="page-title">
	<h2><span class="fa fa-list"></span> Campañas</h2>
</div>


<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap admin-seccion-producto">
	 <div class="row">
         <div class="col-md-12">

         	 <!-- START CONTENT FRAME -->
                <div class="content-frame">   
                    
                    <!-- START CONTENT FRAME TOP -->
                    <div class="content-frame-top">                        
                        <div class="page-title">                    
                            <h2><span class="fa fa-image"></span> Lista de Campañas</h2>
                        </div>                                      
                    </div>

                     <!-- START CONTENT FRAME RIGHT -->
                    <div class="content-frame-right">                        
                        <div class="block push-up-10">
                            <div action="upload.php" class="dropzone dropzone-mini">
                            	<?= $this->Html->link(
                            		'<div class="dz-default dz-message"><h4>Subir Nueva Campaña</h4></div>',
                            		array('controller' => 'campanias', 'action' => 'add'),
                            		array(
                            			'escape'	=> false, 
                            			'class'		=>	'nuevo_banner'
                            		)
                            	); ?>
                            </div>
                        </div>     

                         <?= $this->element('admin_data_seo'); ?>                   	       
                        
                    </div>
                    <!-- END CONTENT FRAME RIGHT -->
                
                    <!-- START CONTENT FRAME BODY -->
                    <div class="content-frame-body content-frame-body-left">

                    	<div class="gallery" id="links">

                        	<? foreach ($campanias as $key => $campania) { 
                                $hr = ( $campania['Campania']['activo'] ? 'activo' : 'no-activo');
                                ?>

                                <div class="col-md-3 col-xs-12">
                                    <div class="row">
                                        <a class="gallery-item" href="<?= sprintf('%simg/%s',$this->webroot,$campania['Campania']['imagen']['path']) ?>" title="<?= $campania['Campania']['nombre'] ?>" data-gallery>
                                        	<div class="image">
                                                 <? if ( ! empty($campania['Campania']['imagen']) ) { ?> 
                                                    <?= $this->Html->image($campania['Campania']['imagen']['admin']); ?>
                                                <? } else { ?>
                                                    <?= $this->Html->image('template/SUPER_CERDO_250.jpg'); ?>
                                                <? } ?>
                                            </div>
                                        </a>

                                    </div>
                                    <div class="row">
                                        <div class="">
                                            <div class="meta">
                                                <div class="col-md-12 col-xs-12">
                                                    <div class="titulo_catalogo">
                                                         <strong><?= $campania['Campania']['nombre'] ?></strong>
                                                    </div>
                                                    <span><?= formatoFecha($campania['Campania']['created'],'dmy'); ?></span>
                                                    <hr class="<?= $hr; ?>">
                                                </div>
                                                <div class="col-md-12 col-xs-12" style="text-align: center;">

                                                    <!-- EDITAR -->
                                                    <?= $this->Html->link('<i class="fa fa-pencil"></i>', array('controller' => 'Campanias', 'action' => 'edit', $campania['Campania']['id']), array('class' => 'btn btn-default btn-xs', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title' => 'Editar', 'escape' => false)); ?>

                                                     <? if ( $campania['Campania']['activo'] ) : ?>

                                                        <!-- DESACTIVAR -->
                                                         <?= $this->Html->link('<i class="fa fa-ban"></i>', array('controller' => 'Campanias','action' => 'desactivar', $campania['Campania']['id'], $campania['Campania']['id']), array('class' => 'btn btn-danger btn-xs', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title' => 'Desactivar', 'escape' => false)); ?>

                                                     <? else : ?>

                                                        <!-- ACTIVAR -->
                                                         <?= $this->Html->link('<i class="fa fa-check-square-o"></i>', array('controller' => 'Campanias', 'action' => 'activar', $campania['Campania']['id'], $campania['Campania']['id']), array('class' => 'btn btn-success btn-xs', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title' => 'Activar', 'escape' => false)); ?>

                                                     <? endif; ?>

                                                     <?//= $this->Form->postLink('<i class="fa fa-remove"></i>', array('action' => 'delete', $campania['Campania']['id']), array('class' => 'btn btn-xs btn-warning confirmar-eliminacion', 'data-toggle'=>'tooltip','data-toggle'=>'tooltip', 'data-placement' => 'top', 'title' => 'Eliminar', 'escape' => false)); ?>

                                                </div>
                                            </div> 
                                        </div>
                                    </div>
                                </div>

                        	<? } ?>

                        </div>

                        <div class="pull-right">
							<ul class="pagination">
								<?= $this->Paginator->prev('« Anterior', array('tag' => 'li'), null, array('tag' => 'li', 'disabledTag' => 'a', 'class' => 'first disabled hidden')); ?>
								<?= $this->Paginator->numbers(array('tag' => 'li', 'currentTag' => 'a', 'modulus' => 2, 'currentClass' => 'active', 'separator' => '')); ?>
								<?= $this->Paginator->next('Siguiente »', array('tag' => 'li'), null, array('tag' => 'li', 'disabledTag' => 'a', 'class' => 'last disabled hidden')); ?>
							</ul>
						</div>

                        
                    </div>
                </div>

                <!-- BLUEIMP GALLERY -->
		         <?= $this->element('admin_blueimp_gallery'); ?>
		        <!-- END BLUEIMP GALLERY -->

          </div>
     </div>
</div>     


