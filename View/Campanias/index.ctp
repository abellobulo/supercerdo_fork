
<div id="bootstrap-touch-slider" class="carousel bs-slider slide control-round indicators-line banner-campanas" data-ride="carousel" data-pause="hover" data-interval="8000">

	<div class="carousel-inner" role="listbox">

		<div class="item active" style="background: url(img/otros-calbornoz/banner_campanas.jpg);">
			<div class="bs-slider-overlay"></div>
			<div class="container">
				<div class="slide-text slide_style_center col-sm-12">
					<h1 data-animation="animated fadeInDown">Campañas</h1>
				</div>
			</div>
		</div>

	</div>

</div>

<section class="breadcrums">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<a class="link animated" href="">Home</a> / Campañas
			</div>
		</div>
	</div>
</section>

<section class="interior-campanas" style="background: url(<?= $this->webroot; ?>img/template/fondo-supercerdo-4.jpeg);
    background-position: top center !important;
    background-size: cover !important;
    background-attachment: fixed !important;">
	
	<div class="capa-fondo">

			<div class="container">

				<div class="row">

					<? 
					$iteral = 1;
					foreach ($campanias as $key => $campania) { ?>
					
						<? if ( $iteral == 1 ) { ?>

							<div class="col-xs-12 col-sm-8 col-lg-7 col-lg-offset-1 contenedor-campanas">
								<a class="link animated campana wow fadeInUp modal_compania" href="#" data-toggle="modal" data-target=".modal-compania" data-video-id="<?= existe($campania['Campania']['link']) ?>">
									<?= $this->html->image($campania['Campania']['imagen']['path'], array('class' => 'base animated img-campana', 'alt' => existe($campania['Campania']['nombre']))); ?>
									<div class="capa animated">
										<div class="boton animated">
											<div class="text-center">
												<img src="img/campanas/btn-play.png" />
											</div>
										</div>
										<div class="texto animated">
											<h2><?= existe($campania['Campania']['nombre']) ?></h2>
											<h3><?= existe($campania['Campania']['bajada']) ?></h3>
										</div>
									</div>
								</a>
							</div>

						<? } else if ( $iteral == 2 || $iteral == 3) { ?>

								<? if ( $iteral == 2 ) { ?>

									<div class="col-xs-12 col-sm-4 col-lg-3 contenedor-campanas">

										<a class="link animated campana wow fadeInUp modal_compania" href="#" data-toggle="modal" data-target=".modal-compania" data-video-id="<?= existe($campania['Campania']['link']) ?>">
											<?= $this->html->image($campania['Campania']['imagen']['path'], array('class' => 'base animated img-campana', 'alt' => existe($campania['Campania']['nombre']))); ?>
											<div class="capa animated">
												<div class="boton animated">
													<div class="text-center">
														<img src="img/campanas/btn-play.png" />
													</div>
												</div>
												<div class="texto animated">
													<h3><?= existe($campania['Campania']['nombre']) ?></h3>
												</div>
											</div>
										</a>

								<? } else if ( $iteral == 3 ) {  ?>

										<a class="link animated campana wow fadeInUp modal_compania" href="#" data-toggle="modal" data-target=".modal-compania" data-video-id="<?= existe($campania['Campania']['link']) ?>">
											<?= $this->html->image($campania['Campania']['imagen']['path'], array('class' => 'base animated img-campana', 'alt' => existe($campania['Campania']['nombre']))); ?>
											<div class="capa animated">
												<div class="boton animated">
													<div class="text-center">
														<img src="img/campanas/btn-play.png" />
													</div>
												</div>
												<div class="texto animated">
													<h3><?= existe($campania['Campania']['nombre']) ?></h3>
												</div>
											</div>
										</a>
								<? } ?>	

										
								<? if ( $iteral == 3 ) { ?>	
									
									</div>

								<? } ?>

						<? } else if (  $iteral == 4) {  ?>

							<div class="col-xs-12 col-sm-6 col-lg-5 col-lg-offset-1 contenedor-campanas">

								<a class="link animated campana wow fadeInUp modal_compania" href="#" data-toggle="modal" data-target=".modal-compania" data-video-id="<?= existe($campania['Campania']['link']) ?>">
									<?= $this->html->image($campania['Campania']['imagen']['path'], array('class' => 'base animated img-campana', 'alt' => existe($campania['Campania']['nombre']))); ?>
									<div class="capa animated">
										<div class="boton animated">
											<div class="text-center">
												<img src="img/campanas/btn-play.png" />
											</div>
										</div>
										<div class="texto animated">
											<h2><?= existe($campania['Campania']['nombre']) ?></h2>
											<h3><?= existe($campania['Campania']['bajada']) ?></h3>
										</div>
									</div>
								</a>

							</div>

						<? } else if (  $iteral == 5 ) {  ?>

							<div class="col-xs-12 col-sm-6 col-lg-5 contenedor-campanas">
								<a class="link animated campana wow fadeInUp modal_compania" href="#" data-toggle="modal" data-target=".modal-compania" data-video-id="<?= existe($campania['Campania']['link']) ?>">
									<?= $this->html->image($campania['Campania']['imagen']['path'], array('class' => 'base animated img-campana', 'alt' => existe($campania['Campania']['nombre']))); ?>
									<div class="capa animated">
										<div class="boton animated">
											<div class="text-center">
												<img src="img/campanas/btn-play.png" />
											</div>
										</div>
										<div class="texto animated">
											<h2><?= existe($campania['Campania']['nombre']) ?></h2>
											<h3><?= existe($campania['Campania']['bajada']) ?></h3>
										</div>
									</div>
								</a>
							</div>
						<? } ?>

						<? 
							$iteral ++;
							$iteral = ( $iteral == 5 ? 1 : $iteral ); 
						?>

					<? } ?>

				</div>

			</div>
		
	</div>

</section>

<?= $this->element('seccion_marcas'); ?>

<!-- Modal Videos-->
<div class="modal modal-compania fade" id="video-modal-compania" tabindex="-1" role="dialog" aria-labelledby="Reforestar">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar"></button>
      </div>
      
      <iframe width="100%" height="100%" src="" frameborder="0" allowfullscreen></iframe>
    </div>
  </div>
</div>

