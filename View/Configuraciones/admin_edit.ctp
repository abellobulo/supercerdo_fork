<div class="page-title">
    <h2><span class="fa fa-list"></span> Configuraciones</h2>
</div>

<div class="page-content-wrap">
    <div class="row">
        <div class="col-md-12">
            <?= $this->Form->create('Configuracion', array('class' => 'form-horizontal', 'type' => 'file', 'inputDefaults' => array('label' => false, 'div' => false, 'class' => 'form-control'))); ?>

                <?= $this->Form->input('id'); ?>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><span class="fa fa-mail-forward"></span><strong> Actualizar</strong> Configuración </h3>
                    </div>
                    <div class="panel-body">
                        <p>Hay campos que son necesarios para unos modulos y para otros no, por tal razón si no está seguro del campo, no es necesario ingresarlo</p>
                        <p>Para la imagen se recomienda utilizar extensiones .png y jpg no mayor a <code>500 KB</code>.</p>
                    </div>
                    <div class="panel-body form-group-separated">   

                         <div class="row">
                            
                            <div class="col-md-12">

                               <div class="form-group">
                                    <label class="col-md-3 col-xs-12 control-label">Modulo</label>
                                    <div class="col-md-6 col-xs-12">                                
                                        <?= $this->Form->input('modulo', array('class' => 'form-control')); ?>
                                        <span class="help-block">Contacto, Nosotros, Productos, entro otros</span>
                                    </div>
                                </div>

                                 <div class="form-group">
                                    <label class="col-md-3 col-xs-12 control-label">Nombre</label>
                                    <div class="col-md-6 col-xs-12">                                
                                        <?= $this->Form->input('nombre', array('class' => 'form-control')); ?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 col-xs-12 control-label">Bajada</label>
                                    <div class="col-md-6 col-xs-12">                                
                                        <?= $this->Form->input('bajada', array('class' => 'form-control')); ?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 col-xs-12 control-label">Descripción</label>
                                    <div class="col-md-6 col-xs-12">                                
                                        <?= $this->Form->input('descripcion', array('class' => 'form-control summernote')); ?>
                                    </div>
                                </div>

                                <? if ( ! empty($this->request->data['Configuración']['imagen'])) { ?>
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Imagen (Actual)</label>
                                        <div class="col-md-6 col-xs-12" style="text-align: center;">                                
                                            <?= $this->Html->image($this->request->data['Configuración']['imagen']['admin']); ?>
                                        </div>
                                        
                                    </div>
                                <? } ?>

                                <div class="form-group">
                                    <label class="col-md-3 col-xs-12 control-label">Imagen/Banner</label>
                                    <div class="col-md-6 col-xs-12">                                
                                        <?= $this->Form->input('imagen', array('type' => 'file', 'class'=>'file', 'data-preview-file-type' => 'any', 'multiple')); ?>
                                         <span class="help-block">Esta imagen se utilizará como referencia de la configuración</span>
                                    </div>
                                    
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 col-xs-12 control-label">Tipo</label>
                                    <div class="col-md-6 col-xs-12">                                
                                        <?= $this->Form->input('tipo', array('class' => 'form-control')); ?>
                                    </div>
                                </div>

                            </div>
                            
                        </div>

                    </div>
                    <div class="panel-footer">
                        <div class="pull-right">
                            <input type="submit" class="btn btn-primary esperar-carga" autocomplete="off" data-loading-text="Espera un momento..." value="Guardar cambios">
                            <?= $this->Html->link('Cancelar', array('action' => 'index'), array('class' => 'btn btn-danger')); ?>
                        </div>
                    </div>
                </div>

            <?= $this->Form->end(); ?>
        </div>
    </div>
</div>
