<!-- 
	* Admin Contacto 
	* Ene 2018
	* Controller: contacto
	* Action: admin_contacto
	* Admin: true
-->  
 
<?= $this->element('admin_breadcrumb'); ?>

<div class="page-title">
	<h2><span class="fa fa-list"></span> Contactos</h2>
</div>


<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap admin-seccion-producto">
	 <div class="row">
         <div class="col-md-12">

         	 <!-- START CONTENT FRAME -->
                <div class="content-frame">   
                    
                    <!-- START CONTENT FRAME TOP -->
                    <div class="content-frame-top">                        
                        <div class="page-title">                    
                            <h2><span class="fa fa-image"></span> Items de contactos</h2>
                        </div>                                      
                    </div>

                     <!-- START CONTENT FRAME RIGHT -->
                    <div class="content-frame-right">                        
                                       	       
                        <?= $this->element('admin_data_seo'); ?>                                           
                        
                    </div>
                    <!-- END CONTENT FRAME RIGHT -->
                
                    <!-- START CONTENT FRAME BODY -->
                    <div class="content-frame-body content-frame-body-left">

                    	<div class="gallery" id="links">

                        	<? foreach ($items_contacto as $key => $items) { ?>

                                <div class="col-md-3 col-xs-12">
                                    <div class="row">
                                        <a class="gallery-item" href="<?= sprintf('%simg/%s',$this->webroot,$items['Configuracion']['imagen']['path']) ?>" title="<?= $items['Configuracion']['nombre'] ?>" data-gallery>
                                            <div class="image">                              
                                                <?= $this->Html->image($items['Configuracion']['imagen']['admin']); ?>
                                            </div>

                                        </a>

                                    </div>
                                    <div class="row">
                                        <div class="">
                                            <div class="meta">
                                                <div class="col-md-7 col-xs-12">
                                                    <strong><?= $items['Configuracion']['nombre'] ?></strong><br>
                                                </div>
                                                <div class="col-md-5 col-xs-12">

                                                    <!-- EDITAR -->
                                                    <?= $this->Html->link('<i class="fa fa-pencil"></i>', array('controller' => 'Configuraciones', 'action' => 'edit', $items['Configuracion']['id']), array('class' => 'btn btn-default btn-xs', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title' => 'Editar', 'escape' => false)); ?>

                                                </div>
                                            </div> 
                                        </div>
                                    </div>
                                </div>

                        	<? } ?>

                        </div>

                        
                    </div>
                </div>

          </div>
     </div>
</div>     

