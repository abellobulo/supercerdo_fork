<div class="page-title">
	<h2><span class="fa fa-list"></span> Contactos</h2>
</div>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">Editar Contacto</h3>
	</div>
	<div class="panel-body">
		<div class="table-responsive">
			<?= $this->Form->create('Contacto', array('class' => 'form-horizontal', 'type' => 'file', 'inputDefaults' => array('label' => false, 'div' => false, 'class' => 'form-control'))); ?>
				<table class="table">
					<?= $this->Form->input('id'); ?>
					<tr>
						<th><?= $this->Form->label('nombre', 'Nombre'); ?></th>
						<td><?= $this->Form->input('nombre'); ?></td>
					</tr>
					<tr>
						<th><?= $this->Form->label('email', 'Email'); ?></th>
						<td><?= $this->Form->input('email'); ?></td>
					</tr>
					<tr>
						<th><?= $this->Form->label('ciudad', 'Ciudad'); ?></th>
						<td><?= $this->Form->input('ciudad'); ?></td>
					</tr>
					<tr>
						<th><?= $this->Form->label('f_nacimiento', 'F nacimiento'); ?></th>
						<td><?= $this->Form->input('f_nacimiento'); ?></td>
					</tr>
					<tr>
						<th><?= $this->Form->label('telefono', 'Telefono'); ?></th>
						<td><?= $this->Form->input('telefono'); ?></td>
					</tr>
					<tr>
						<th><?= $this->Form->label('asunto', 'Asunto'); ?></th>
						<td><?= $this->Form->input('asunto'); ?></td>
					</tr>
					<tr>
						<th><?= $this->Form->label('consulta', 'Consulta'); ?></th>
						<td><?= $this->Form->input('consulta'); ?></td>
					</tr>
					<tr>
						<th><?= $this->Form->label('tipo', 'Tipo'); ?></th>
						<td><?= $this->Form->input('tipo'); ?></td>
					</tr>
					<tr>
						<th><?= $this->Form->label('activo', 'Activo'); ?></th>
						<td><?= $this->Form->input('activo', array('class' => 'icheckbox')); ?></td>
					</tr>
					<tr>
						<th><?= $this->Form->label('send', 'Send'); ?></th>
						<td><?= $this->Form->input('send'); ?></td>
					</tr>
				</table>

				<div class="pull-right">
					<input type="submit" class="btn btn-primary esperar-carga" autocomplete="off" data-loading-text="Espera un momento..." value="Guardar cambios">
					<?= $this->Html->link('Cancelar', array('action' => 'index'), array('class' => 'btn btn-danger')); ?>
				</div>
			<?= $this->Form->end(); ?>
		</div>
	</div>
</div>
