<!-- 
	CONTACTO - index.ctp
	Controller: contactos
	Action: index
	Admin: false
-->



<div id="bootstrap-touch-slider" class="carousel bs-slider slide control-round indicators-line banner-contacto" data-ride="carousel" data-pause="hover" data-interval="8000">

	<div class="carousel-inner" role="listbox">

		<div class="item active" style="background: url(img/otros-calbornoz/contacto_r.jpg);">
			<div class="bs-slider-overlay"></div>
			<div class="container">
				<div class="slide-text slide_style_center col-sm-12">
					<h1 data-animation="animated fadeInDown">Contacto</h1>
				</div>
			</div>
		</div> 

	</div>

</div>

<section class="breadcrums">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<a class="link animated" href="">Home</a> / Contacto
			</div>
		</div>
	</div>
</section>

<section class="interior-contacto" style="background: url(<?= $this->webroot; ?>img/template/fondo-supercerdo-4.jpeg);
    background-position: top center !important;
    background-size: cover !important;
    background-attachment: fixed !important;">
	
	<div class="capa-fondo">
		
		<div class="container">

			<div class="row">

				<div class="col-xs-12">

					<div class="tabs contenedor-tabs">

					    <ul class="nav nav-tabs">
					        <li class="active"><a href="#tab-contacto" data-toggle="tab" class="link animated"><div class="icon-contac icon-contacto"></div>Contacto</a></li>
					        <li><a href="#tab-auspicio" data-toggle="tab" class="link animated"><div class="icon-auspicio icon-contacto"></div>Auspicio</a></li>
					        <li><a href="#tab-reclamo" data-toggle="tab" class="link animated"><div class="icon-reclamo icon-contacto"></div>Reclamo</a></li>
					        <li><a href="#tab-felicitaciones" data-toggle="tab" class="link animated"><div class="icon-felicitaciones icon-contacto"></div>Felicitaciones</a></li>
					        <li><a href="#tab-trabaja" data-toggle="tab" class="link animated"><div class="icon-contacto icon-trabaja"></div>Trabaja con Nosotros</a></li>
					    </ul>

			    		<div class="tab-content tab-contacto">

			    			<div class="tab-pane active" id="tab-contacto">

			    				<div class="col-sm-12 col-md-6 contacto">

			    					<form class="form-horizontal" method="post" action="contacto/">

			    						<div class="form-group">

											<div class="col-xs-6 col-sm-6">
												<input type="text" class="form-control input-md" name="nombre" placeholder="Nombre y Apellido" />
											</div>

											<div class="col-xs-6 col-sm-6">
												<input type="text" class="form-control input-md" name="email" placeholder="Email" />
											</div>

										</div>

										<div class="form-group">

											<div class="col-xs-6 col-sm-6">
												<input type="text" class="form-control input-md" name="ciudad" placeholder="Ciudad" />
											</div>

											<div class="col-xs-6 col-sm-6">
												<input type="text" class="form-control input-md" name="fecha" placeholder="Fecha de Nacimiento" />
											</div>

										</div>

			    						<div class="form-group">

											<div class="col-xs-6 col-sm-6">
												<input type="text" class="form-control input-md" name="telefono" placeholder="Teléfono" />
											</div>

											<div class="col-xs-6 col-sm-6">
												<select class="form-control input-md" name="asunto">
													<option>Asunto</option>
												</select>
											</div>

										</div>

										<div class="form-group">

											<div class="col-xs-12 col-sm-12">
												<textarea class="form-control input-md" name="mensaje" placeholder="Escribe tu Consulta" rows="7"></textarea>
											</div>

										</div>

										<div class="form-group">
											<div class="col-xs-12 col-sm-12 text-right">
												<button type="submit" class="link animated boton">Enviar</button>
											</div>
										</div>

									</form>

			    				</div>

			    				<div class="col-sm-12 col-md-6">
			    					<h3>CALL CENTER</h3>
			    					<h4>800 20 6000</h4>
			    					<p>
			    						Línea de atención al consumidor
			    						<br />
			    						Si necesitas información sobre nuestros productos o quieres consultar sobre la elaboración, envasado, información nutricional y calidad de ellos, comunícate con nosotros al siguiente número:
			    					</p>
			    					<p>
				    					Horario de atención
				    					<br />
				    					Lunes a Viernes 8:30 a 18:00 hrs.
				    					<br />
				    					Sábado 9:00 a 14:00 hrs
			    					</p>
			    					<h4>600 600 7777</h4>
			    					<p>
			    						Línea de atención al cliente
			    						<br />
			    						Si eres dueño de un local comercial, negocio, almacén, o quieres comprar nuestros productos, comunícate con nosotros al siguiente número:
			    					</p>
			    					<p>
				    					Horario de atención
				    					<br />
				    					Lunes a Viernes 8:30 a 18:00 hrs.
				    					<br />
				    					Sábado 9:00 a 14:00 hrs
			    					</p>
			    				</div>

			    			</div>

			    			<div class="tab-pane" id="tab-auspicio">
			    				<h2>Auspicio</h2>
			    			</div>

			    			<div class="tab-pane" id="tab-reclamo">
			    				<h2>Reclamo</h2>
			    			</div>

			    			<div class="tab-pane" id="tab-reclamo">
			    				<h2>Reclamo</h2>
			    			</div>

			    			<div class="tab-pane" id="tab-felicitaciones">
			    				<h2>Felicitaciones</h2>
			    			</div>

			    			<div class="tab-pane" id="tab-trabaja">
			    				<h2>Trabaja con Nosotros</h2>
			    			</div>

			    		</div>

			    	</div>

				</div>

			</div>

		</div>
	</div>

</section>

<?= $this->element('seccion_marcas'); ?>