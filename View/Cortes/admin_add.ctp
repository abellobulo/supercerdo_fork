<div class="page-title">
	<h2><span class="fa fa-list"></span> Cortes</h2>
</div>



<div class="page-content-wrap">
    <div class="row">
        <div class="col-md-12">
        	<?= $this->Form->create('Corte', array('class' => 'form-horizontal', 'type' => 'file', 'inputDefaults' => array('label' => false, 'div' => false, 'class' => 'form-control'))); ?>

        		<div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><span class="fa fa-mail-forward"></span><strong> Nuevo</strong> Corte </h3>
                                </div>
                                <div class="panel-body">
                                    <p>Para la imagen se recomienda utilizar extensiones .png y no mayor a 500 KB.</p>
                                </div>
                                <div class="panel-body form-group-separated">                                                                        
                                    
                                    <div class="row">
                                        
                                        <div class="col-md-12">

                                           <div class="form-group">
                                                <label class="col-md-3 col-xs-12 control-label">Nombre</label>
                                                <div class="col-md-6 col-xs-12">                                
                                                	<?= $this->Form->input('nombre', array('class' => 'form-control')); ?>
                                                </div>
                                            </div>

                                             <div class="form-group">
                                                <label class="col-md-3 col-xs-12 control-label">Bajada</label>
                                                <div class="col-md-6 col-xs-12">                                
                                                	<?= $this->Form->input('bajada', array('class' => 'form-control')); ?>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 col-xs-12 control-label">Descripción</label>
                                                <div class="col-md-6 col-xs-12">                                
                                                    <?= $this->Form->input('descripcion', array('class' => 'form-control')); ?>
                                                </div>
                                            </div>

                                             <div class="form-group">
                                                <label class="col-md-3 col-xs-12 control-label">Número del Corte</label>
                                                <div class="col-md-6 col-xs-12">                                
                                                    <?= $this->Form->input('numero', array('class' => 'form-control')); ?>
                                                </div>
                                            </div>
                                            

                                            <div class="form-group">
                                                <label class="col-md-3 col-xs-12 control-label">Imagen (570 x 280)</label>
                                                <div class="col-md-6 col-xs-12">                                
                                                	<?= $this->Form->input('imagen', array('type' => 'file', 'class'=>'file', 'data-preview-file-type' => 'any', 'multiple')); ?>
                                                	 <span class="help-block">Esta imagen se utilizará como referencia del corte</span>
                                                </div>
                                                
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 col-xs-12 control-label">Recomendado para hacer</label>
                                                <div class="col-md-6 col-xs-12">                                
                                                    <?= $this->Form->input('para_producto', array('class' => 'form-control select', 'name' => 'ProductoParaProducto[]', 'multiple', 'options' => array('Horno', 'Parrilla'))); ?>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 col-xs-12 control-label">Recomentar Recetas</label>
                                                <div class="col-md-6 col-xs-12">                                
                                                    <?= $this->Form->input('recomendar_receta', array('class' => 'form-control select', 'name' => 'ProductoRecomendarReceta[]', 'multiple', 'options' => $recetas)); ?>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 col-xs-12 control-label">Recetas del Corte</label>
                                                <div class="col-md-6 col-xs-12">                                
                                                    <?= $this->Form->input('recomendar_receta', array('class' => 'form-control select', 'name' => 'ProductoRecomendarReceta[]', 'multiple', 'options' => $recetas)); ?>
                                                </div>
                                            </div>
                                            
                                        </div>
                                        
                                        
                                    </div>

                                </div>
                                <div class="panel-footer">
                                    <div class="pull-right">
										<input type="submit" class="btn btn-primary esperar-carga" autocomplete="off" data-loading-text="Espera un momento..." value="Guardar cambios">
										<?= $this->Html->link('Cancelar', array('action' => 'index'), array('class' => 'btn btn-danger')); ?>
									</div>
                                </div>
                            </div>

        	<?= $this->Form->end(); ?>
        </div>
    </div>
</div>

