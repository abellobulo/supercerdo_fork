<!-- 
	CORTES - index.ctp
	Controller: cortes
	Action: index
	Admin: false
-->
<div id="bootstrap-touch-slider" class="carousel bs-slider slide control-round indicators-line banner-producto" data-ride="carousel" data-pause="hover" data-interval="8000">

	<div class="carousel-inner" role="listbox">

		<div class="item active" style="background: url(img/otros-calbornoz/nuestroscortes_r.jpg);">
			<div class="bs-slider-overlay"></div>
			<div class="container">
				<div class="slide-text slide_style_center col-sm-12">
					<img src="img/cortes/icon-cortes.png" class="icon-cortes" />
				</div>
			</div>
		</div>

	</div>

</div>

<!-- START BREADCRUMB -->
<?= $this->element('breadcrumb'); ?>
<!-- END BREADCRUMB -->

<section class="interior-producto-top">

	<div class="fondo-right wow fadeInUp hidden-xs hidden-sm"></div>
	
	<div class="container">

		<div class="row">

			<div class="col-sm-12 col-md-6 info-producto-left wow fadeInUp">


				<h2 class="text-center">Secciona tu corte</h2>



				<div class="col-md-9">
						
					<div class="text-center">
						<img src="img/cortes-cerdo/cerdo.png" alt="Cortes" class="img-producto animated img_corte image_corte" />
						<? foreach ($cortes as $key => $corte_img) { ?>
							<div class="<?= sprintf('corte_cerdo_%s',$corte_img['Corte']['id']) ?> img_corte animated no-visible">
								<?= $this->Html->image(
								$corte_img['Corte']['imagen']['path'], 
								array('class' => 'img-producto img-corte image_corte', 'id' => sprintf('img_%s',$corte_img['Corte']['id']))); ?>
							</div>
						<? } ?>
					</div>

				</div>
				<div class="col-md-3">
					<? foreach ($cortes as $key => $corte_botones) { ?>
						<div class=" ver-corte link animated select-corte" id-corte="<?= $corte_botones['Corte']['id']; ?>"><?= sprintf('(%s) %s',$corte_botones['Corte']['numero'],Ucfirst($corte_botones['Corte']['nombre'])) ?></div>
					<? } ?>

				</div>



				
			</div>

			<div class="col-sm-12 col-md-6 info-producto-right corte-div-info wow fadeInUp">

				<h2>Corte Chuleta</h2>

				<p>Curabitur lobortis id lorem id bibendum. Ut id consectetur magna. Quisque volutpat augue enim, pulvinar lobortis nibh lacinia at. Vestibulum nec erat ut mi sollicitudin porttitor id sit amet risus. Nam tempus vel odio vitae aliquam. In imperdiet eros id lacus vestibulum vestibulum. Suspendisse fermentum sem sagittis ante venenatis egestas quis vel justo. Maecenas semper</p>

				<h4>SE RECOMIENDA:</h4>

				<div class="recomendaciones">

					<div class="recomendacion">
						<div class="imagen">
							<img src="img/producto/icon-cocina.png" />
						</div>
						<h5>HORNO</h5>
					</div>

					<div class="recomendacion">
						<div class="imagen">
							<img src="img/producto/icon-parrilla.png" />
						</div>
						<h5>PARRILLA</h5>
					</div>

				</div>

				<div class="ver-receta">
					<a class="link animated" href="recetas/">VER RECETAS</a>
				</div>

			</div>

			<? foreach ($cortes as $key_info => $corteInfo) { ?>

				<div class="col-sm-12 col-md-6 info-producto-right  corte-div-info no-visible corte-info-<?= $corteInfo['Corte']['id']?>">

					<h2><?= $corteInfo['Corte']['nombre']?></h2>

					<p><?= $corteInfo['Corte']['bajada']?></p>

					<h4>SE RECOMIENDA:</h4>

					<div class="recomendaciones">

						<div class="recomendacion">
							<div class="imagen">
								<img src="img/producto/icon-cocina.png" />
							</div>
							<h5>HORNO</h5>
						</div>

						<div class="recomendacion">
							<div class="imagen">
								<img src="img/producto/icon-parrilla.png" />
							</div>
							<h5>PARRILLA</h5>
						</div>

					</div>

					<div class="ver-receta">
						<a class="link animated" href="recetas/">VER RECETAS</a>
					</div>

				</div>
			<? } ?>



		</div>

	</div>

</section>

<?= $this->element('receta_recomendadas'); ?>

<?= $this->element('seccion_marcas'); ?>