<!-- START BREADCRUMB ADMIN -->
<ul class="breadcrumb">
	<? foreach ($breadcrumb as $key => $value) { 
		$link = ( empty($value['link']) ? '#' : $value['link']);
		$class = ( empty($value['class']) ? '#' : $value['class']);
	?>
		<li><a href="<?= $link ?>" class="<?= $class ?>"><?= $value['label'] ?></a></li>
	<?	} ?>

</ul>
<!-- END BREADCRUMB ADMIN-->