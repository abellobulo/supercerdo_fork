
<?  $btn_action = (empty($datos_seo) ? true : false) ; ?>

<div class="block push-up-10">

    <h3 style="text-align: center;">Configuración SEO</h3>

    <hr>

    <div class="row">
        <div class="campos-seo">
            <div class="col-md-4 col-xs-12"> 
                <h6>Título</h6>
            </div>
            <div class="col-md-8 col-xs-12 resultado-seo">
                <span><?= existe($datos_seo['Seo']['titulo_seo']); ?></span>
            </div>
        </div>
    </div>

    <hr class="hr-seo">

    <div class="row">
        <div class="campos-seo">
            <div class="col-md-4 col-xs-12"> 
                <h6>Descripción</h6>
            </div>
            <div class="col-md-8 col-xs-12 resultado-seo">
                <span><?= existe($datos_seo['Seo']['descripcion']); ?></span>
            </div>
        </div>
    </div>

    <hr class="hr-seo">

    <div class="row">
        <div class="campos-seo">
            <div class="col-md-4 col-xs-12"> 
                <h6>Palabras Claves</h6>
            </div>
            <div class="col-md-8 col-xs-12 resultado-seo">
                <span><?= existe($datos_seo['Seo']['palabras']); ?></span>
            </div>
        </div>
    </div>

    <hr class="hr-seo">

    <div class="row">
        <div class="campos-seo">
            <div class="col-md-4 col-xs-12"> 
                <h6>Slug</h6>
            </div>
            <div class="col-md-8 col-xs-12 resultado-seo">
                 <span><?= existe($datos_seo['Seo']['slug']); ?></span>
            </div>
        </div>
    </div>

    <hr>

    <div class="row">
        <div class="campos-seo" style="text-align: center;">
            <div class="col-md-12 col-xs-12"> 
               <? if ( $btn_action ) { ?> 
                <?= $this->Html->link('<i class="fa fa-pencil"></i> Actualizar SEO', array('controller'  => 'seos', 'action' => 'add', $modulo_seo, ( empty($modulo_seo_id) ? 0 : $modulo_seo_id ) , $redirect_page['controller'], $redirect_page['action'], $redirect_page['param']), array('class' => 'btn btn-danger', 'escape' => false)); ?>
                <? } else { ?>
                    <?= $this->Html->link('<i class="fa fa-pencil"></i> Actualizar SEO', array('controller'  => 'seos', 'action' => 'edit', $datos_seo['Seo']['id'],$redirect_page['controller'], $redirect_page['action'], $redirect_page['param']), array('class' => 'btn btn-danger', 'escape' => false)); ?>
                <? } ?>
            </div>
            
        </div>
    </div>

    
</div>  