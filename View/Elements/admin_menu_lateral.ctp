<div class="page-sidebar">
	<ul class="x-navigation x-navigation-custom">
		<li class="xn-logo">
			<?= $this->Html->link(
				'<span class="fa fa-dashboard"></span> <span class="x-navigation-control">Backend</span>',
				'/admin',
				array('escape' => false)
			); ?>
		</li>

		<!-- HOME -->
		<li class="<?= ($this->Html->menuActivo(array('controller' => 'pages', 'action' => 'display', 'home', 'admin' => false)) ? 'active' : ''); ?>">
			<?= $this->Html->link(
				'<span class="fa fa-image"></span> <span class="xn-text">Home</span>',
				array('controller' => 'pages', 'action' => 'home'),
				array('escape' => false)
			); ?>
		</li>


		<li class="xn-title">Nuestros Productos</li>

		<!-- PRODUCTOS -->
		<li class="xn-openable <?= (
			(
				$this->Html->menuActivo(array('controller' => 'Productos', 'action' => 'page_productos')) ||
				$this->Html->menuActivo(array('controller' => 'Productos', 'action' => 'index'))
			)
			? 'active' : ''
		); ?>">
			<a href="#"><span class="fa fa-cog"></span> <span class="xn-text">Productos</span></a>
			<ul>
				<li class="<?= ($this->Html->menuActivo(array('controller' => 'Productos', 'action' => 'admin_seccion_productos')) ? 'active' : ''); ?>">
					<?= $this->Html->link(
						'<span class="fa fa-user"></span> <span class="xn-text">Sección Productos</span>',
						array('controller' => 'productos', 'action' => 'admin_seccion_productos'),
						array('escape' => false)
					); ?>
				</li>
				<li class="<?= ($this->Html->menuActivo(array('controller' => 'Productos', 'action' => 'index')) ? 'active' : ''); ?>">
					<?= $this->Html->link(
						'<span class="fa fa-database"></span> <span class="xn-text">Lista Productos</span>',
						array('controller' => 'Productos', 'action' => 'index'),
						array('escape' => false)
					); ?>
				</li>
			</ul>
		</li>

		<!-- Cortes -->
		<li class="<?= ($this->Html->menuActivo(array('controller' => 'cortes', 'action' => 'index')) ? 'active' : ''); ?>">
			<?= $this->Html->link(
				'<span class="fa fa-image"></span> <span class="xn-text">Cortes</span>',
				array('controller' => 'cortes', 'action' => 'index'),
				array('escape' => false)
			); ?>
		</li>

		<!-- RECETAS -->
		<li class="<?= ($this->Html->menuActivo(array('controller' => 'recetas', 'action' => 'index')) ? 'active' : ''); ?>">
			<?= $this->Html->link(
				'<span class="fa fa-image"></span> <span class="xn-text">Recetas</span>',
				array('controller' => 'recetas', 'action' => 'index'),
				array('escape' => false)
			); ?>
		</li>


		<li class="xn-title">Información</li>

		<li class="<?= ($this->Html->menuActivo(array('controller' => 'noticias', 'action' => 'index')) ? 'active' : ''); ?>">
			<?= $this->Html->link(
				'<span class="fa fa-image"></span> <span class="xn-text">Noticias</span>',
				array('controller' => 'noticias', 'action' => 'index'),
				array('escape' => false)
			); ?>
		</li>

		<li class="<?= ($this->Html->menuActivo(array('controller' => 'campanias', 'action' => 'index')) ? 'active' : ''); ?>">
			<?= $this->Html->link(
				'<span class="fa fa-image"></span> <span class="xn-text">Campañas</span>',
				array('controller' => 'campanias', 'action' => 'index'),
				array('escape' => false)
			); ?>
		</li>

		<li class="<?= ($this->Html->menuActivo(array('controller' => 'contactos', 'action' => 'contacto')) ? 'active' : ''); ?>">
			<?= $this->Html->link(
				'<span class="fa fa-image"></span> <span class="xn-text">Contacto</span>',
				array('controller' => 'contactos', 'action' => 'contacto'),
				array('escape' => false)
			); ?>
		</li>

		<li class="xn-title">Configuración</li>

		<li class="<?= ($this->Html->menuActivo(array('controller' => 'administradores', 'action' => 'index')) ? 'active' : ''); ?>">
			<?= $this->Html->link(
				'<span class="fa fa-list-ol"></span> <span class="xn-text">Administradores</span>',
				array('controller' => 'administradores', 'action' => 'index'),
				array('escape' => false)
			); ?>
		</li>

		<li class="<?= ($this->Html->menuActivo(array('controller' => 'banners', 'action' => 'bannerFondo')) ? 'active' : ''); ?>">
			<?= $this->Html->link(
				'<span class="fa fa-list-ol"></span> <span class="xn-text">Banners Fondo</span>',
				array('controller' => 'banners', 'action' => 'bannerFondo'),
				array('escape' => false)
			); ?>
		</li>
		


	</ul>
</div>
