<section class="breadcrums wow fadeInUp">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">

				<? foreach ($breadcrumb as $key => $value) { ?>

					<? if ( ! empty($value['controller']) ) { ?>

							<?= $this->Html->link(
								$value['label'].' /',
								array('controller' => $value['controller'], 'action' => $value['action'], (!empty($value['param']) ? $value['param'] : '')),
								array(
									'escape'	=>	false,
									'class'		=>	'link animated'
								)
							); ?>

					<? } else { ?>
							<?=	 $value['label'];  ?>
					<?	} ?>
				<?	} ?>

			</div>
		</div>
	</div>
</section>