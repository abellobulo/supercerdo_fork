<!--
	Footer
	Date: Enero 2018
	Autor: sso - Ukko.cl
-->

</div>

<footer>
	<div class="container">
		<div class="row">
			<div class="col-sm-5 col-md-5 txt-footer wow fadeInUp">
				Copyright © <?= date("Y"); ?> Super Cerdo. Todos los derechos reservados.
			</div>
			<div class="col-sm-2 col-md-2 logo-footer wow fadeInUp">
				<?= $this->Html->image('marcas/Logotipo-Agrosuper.png', array('alt' => "Logotipo-Agrosuper")); ?>

			</div>
			<div class="col-sm-5 col-md-5 txt-footer col-lg-4  wow fadeInUp">
				<?= $this->Html->link(
					'Políticas de Privacidad /',
					array('controller' => 'Politicas', 'action' => 'index'),
					array('espace' => 'false',
							'class'	=>	'link animated'
						)
					); ?>
				<?= $this->Html->link(
					'Contacto /',
					array('controller' => 'Contactos', 'action' => 'index'),
					array('espace' => 'false',
							'class'	=>	'link animated'
						)
					); ?>
				<?= $this->Html->link(
					'Denuncias Anónimas',
					array('controller' => 'Contactos', 'action' => 'index'),
					array('espace' => 'false',
							'class'	=>	'link animated'
						)
					); ?>
			</div>
		</div>
	</div>
</footer>
