<header class="animated">

		<div class="container-fluid">

			<div class="row">

				<div class="col-sm-12 text-center logo">

					<div class="btn-menu">
						<a class="link animated" onmouseover="DesplegarMenu();">
							<span class="animated"></span>
							<span class="animated"></span>
							<span class="animated"></span>
						</a>
					</div>
					<?= $this->Html->link(
							$this->Html->image('template/logo-header-super-cerdo.png', array('class' => 'logo-header animated', 'alt' => 'Súper Cerdo')),
							'/',
							array('class' => 'link animated logo', 'escape' => false)
					); ?>
					<div id="form-header-busqueda">
						<form class="form-horizontal" method="post" action="#">
							<input type="text" class="form-control input-md" placeholder="Buscar" id="auto-complete" data-url="<?= $this->webroot; ?>"/>
							<div id="btn-header-busqueda">
								<button><i class="fa fa-search"></i></button>
							</div>
						</form>
					</div>

	                <div class="enlaces hidden-xs hidden-sm">
	                	<a class="link animated" href="https://www.facebook.com/" target="_blank"><i class="fa fa-facebook"></i></a>
	                	<a class="link animated" href="https://www.youtube.com/" target="_blank"><i class="fa fa-youtube"></i></a>
	                	<a class="link animated" href="https://www.instagram.com/" target="_blank"><i class="fa fa-instagram"></i></a>
	                	<a class="link animated" href="https://www.twitter.com/" target="_blank"><i class="fa fa-twitter"></i></a>
	                </div>

				</div>

			</div>

		</div>

		<nav class="animated" id="menu-site">

			<ul>
				<li>
					<?= $this->Html->link(
							'Home',
							array('controller' => 'pages', 'action' => 'home'),
							array('class' => 'link animated','id' => 'lmenu-opc-1')
						);?>
				</li>
				<!-- Link a nuestros productos -->
				<li>
					<?= $this->Html->link(
							'Nuestros Productos',
							array('controller' => 'productos', 'action' => 'index'),
							array('class' => 'link animated','id' => 'lmenu-opc-2')
						);?>
				</li>
				<li>
					<?= $this->Html->link(
							'Nuestros Cortes',
							array('controller' => 'cortes', 'action' => 'index'),
							array('class' => 'link animated','id' => 'lmenu-opc-3')
						);?>
				</li>
				<li>
					<?= $this->Html->link(
							'Recetas',
							array('controller' => 'recetas', 'action' => 'index'),
							array('class' => 'link animated','id' => 'lmenu-opc-4')
						);?>
				</li>
				<li>
					<?= $this->Html->link(
							'Noticias',
							array('controller' => 'noticias', 'action' => 'index'),
							array('class' => 'link animated','id' => 'lmenu-opc-5')
						);?>
				</li>
				<li>
					<?= $this->Html->link(
								'Campañas',
								array('controller' => 'campanias', 'action' => 'index'),
								array('class' => 'link animated','id' => 'lmenu-opc-6')
						);?>
				</li>
				<li>
					<?= $this->Html->link(
								'Nosotros',
								array('controller' => 'nosotros', 'action' => 'index'),
								array('class' => 'link animated','id' => 'lmenu-opc-7')
						);?>
				</li>
				<li>
					<?= $this->Html->link(
								'Contacto',
								array('controller' => 'contactos', 'action' => 'index'),
								array('class' => 'link animated','id' => 'lmenu-opc-8')
						);?>
				</li>
				<li>
					<a class="link animated" id="menu-opc-8" onclick="DesplegarSubmenu(this);">Revisa también <i class="fa fa-angle-down"></i></a>
					<div class="submenu animated">
						<a class="link animated" id="menu-opc-9" href="https://www.agrosuperventas.com/" target="_blank">Ecommerce</a>
						<a class="link animated" id="menu-opc-10" href="http://www.agrosuperfoodservice.cl/" target="_blank">Food Service </a>
						<a class="link animated" id="menu-opc-11" href="http://www.clubagrosuper.cl/" target="_blank">Club Agrosuper</a>
					</div>
				</li>

			</ul>

			<div class="enlaces text-center visible-xs visible-sm">
            	<a class="link animated" href="https://www.facebook.com/" target="_blank"><i class="fa fa-facebook"></i></a>
            	<a class="link animated" href="https://www.youtube.com/" target="_blank"><i class="fa fa-youtube"></i></a>
            	<a class="link animated" href="https://www.instagram.com/" target="_blank"><i class="fa fa-instagram"></i></a>
            	<a class="link animated" href="https://www.twitter.com/" target="_blank"><i class="fa fa-twitter"></i></a>
            </div>

		</nav>

	</header>

	<div class="contenedor-principal animated">
