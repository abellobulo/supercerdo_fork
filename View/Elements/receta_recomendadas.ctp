<section class="seccion-recetas">

	<div class="container-fluid">

		<div class="row">
			<div class="col-sm-12 text-center wow fadeInUp">
				<h2>TE RECOMENDAMOS ESTAS RECETAS</h2>
			</div>
		</div>

		<div class="row">

			<?php for ($i = 1; $i <= 4; $i++) { ?>

				<div class="col-xs-6 col-sm-6 col-md-3 receta wow fadeInUp">

					<a class="link animated" href="<?= $this->webroot; ?>recetas/view/">

						<div class="contenedor">

							<?= $this->Html->image('recetas/receta-'.$i.'.jpg', array('class' => 'base animated')); ?>

							<div class="capa animated">

								<div class="contenido animated">

									<h3 class="sin-margin-bottom">ASIENTO CON CHIMICHURRI Y TOMATES ASADOS</h3>

									<!-- <div class="detalles">

										<div class="detalle">
											<p class="titulo">Tiempo</p>
											<p class="valor">25 min</p>
										</div>

										<div class="detalle">
											<p class="titulo">Porciones</p>
											<p class="valor">4</p>
										</div>

										<div class="detalle">
											<p class="titulo">Dificultad</p>
											<p class="valor">
												<?= $this->Html->image('recetas/icon-dificultad.png', array('class' => 'con-dificultad')); ?>
												<?= $this->Html->image('recetas/icon-dificultad.png', array('class' => 'con-dificultad')); ?>
												<?= $this->Html->image('recetas/icon-dificultad.png', array('class' => 'con-dificultad desactivado')); ?>
											</p>
										</div>

									</div> -->

									<span>VER MÁS</span>

								</div>

							</div>

						</div>

						<div class="ranking animated">
							<!--<?php for ($j = 0; $j < 3; $j++) { ?>
								<i class="fa fa-star"></i>
							<?php } ?>
							<?php for ($j = 0; $j < 2; $j++) { ?>
								<i class="fa fa-star-o"></i>
							<?php } ?>-->
						</div>

					</a>

				</div>

			<?php } ?>

		</div>

	</div>

</section>
