<section class="seccion-lineas-productos">

	<div class="container-fluid">

		<!-- <div class="row">
			<div class="col-sm-12 text-center wow fadeInUp">
				<h2>NUESTROS PRODUCTOS</h2>
			</div>
		</div> -->

		<div class="row">

			<div class="col-xs-6 col-sm-6 col-md-3 linea wow fadeInUp">
				<a class="link animated" href="<?= $this->webroot; ?>nuestros_cortes">
					<!-- <img src="img/lineas-de-productos/lineas-cortes-portada.jpg" alt="Nuestros Cortes" class="base animated" /> -->
					<div class="capa animated">
						<div class="contenido">
              <h3>NUESTROS CORTES</h3>
							<img src="img/lineas-de-productos/lineas-icon-cortes.png" alt="Nuestros Cortes" />
              <span class="animated">Ver Más</span>
						</div>
					</div>
				</a>
			</div>

			<? foreach ($linea_producto as $key => $linea) {
				$img_linea = array(
					'1' =>	'lineas-icon-cocina.png',
					'2'	=>	'lineas-icon-sandwich.png',
					'3'	=>	'lineas-icon-parrilla.png'
 				);

 				$url_linea = array(
					'1' =>	'linea_cocina',
					'2'	=>	'linea_sandwich',
					'3'	=>	'linea_parrilla'
 				);

			?>

				<div class="col-xs-6 col-sm-6 col-md-3 linea wow fadeInUp">
					<a class="link animated" href="<?= $this->webroot; ?>productos/linea_producto/<?= $url_linea[$linea['LineaProducto']['id']] ?>">
						<?= $this->Html->image($linea['LineaProducto']['imagen']['path'], array('class' => 'base animated', 'alt' => $linea['LineaProducto']['nombre'])); ?>
						<div class="capa animated">
							<div class="contenido">
								<h3><?=  existe($linea['LineaProducto']['nombre']); ?></h3>
                <?= $this->Html->image(sprintf('lineas-de-productos/%s',$img_linea[$linea['LineaProducto']['id']])); ?>
								<span class="animated">Ver Más</span>
							</div>
						</div>
					</a>
				</div>

			<? } ?>


		</div>

	</div>

</section>
