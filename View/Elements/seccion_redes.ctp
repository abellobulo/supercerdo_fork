

<section class="seccion-redes">

	<div class="capa-fondo capa-fondo-1">

		<div class="container-fluid">

      <div class="row">
  			<div class="col-sm-12 text-center wow fadeInUp">
  				<h2>Recetas</h2>
  			</div>
  		</div>

			<div class="row">

				<div class="col-sm-12 p-0" id="contenedor-slider-recetashome">

					<div id="slider-recetashome" class="owl-carousel owl-theme" data-ride="carousel" data-pause="hover" data-interval="4000">

						<? foreach ($recetas as $key => $receta) { ?>

							<div class="item wow fadeInUp">
								<a class="link animated img-rrss" href="<?= sprintf('%srecetas/view/%s',$this->webroot,$receta['Recetas']['id']) ?>" target="_self">
									<img src="/img/Receta/2/catalogo_receta_2.jpg" class="base animated img-ints" />
									<div class="capa">
										<h3 class="sin-margin-bottom"><?php echo $receta['Recetas']['nombre']; ?></h3>
                    <span class="animated">Ver Más</span>
									</div>
								</a>
							</div>

		        <?php } ?>

					</div>

				</div>

			</div>

		</div>

	</div>

  <div class="capa-fondo">

		<div class="container-fluid">

      <div class="row">
  			<div class="col-sm-12 text-center wow fadeInUp">
  				<h2>REVISA LAS ULTIMAS PUBLICACIONES</h2>
  			</div>
  		</div>

			<div class="row">

				<div class="col-sm-12 p-0" id="contenedor-slider-instagram">

					<div id="slider-instagram" class="owl-carousel owl-theme" data-ride="carousel" data-pause="hover" data-interval="4000">

						<?php for ($i = 1; $i <= 6; $i++) { ?>

							<div class="item wow fadeInUp">
								<a class="link animated img-rrss" href="https://www.instagram.com/" target="_blank">
									<img src="img/redes/foto-<?= $i; ?>.jpg" class="base animated img-ints" />
									<div class="capa">
										<i class="fa fa-instagram"></i>
                    <span class="animated">Ver Más</span>
									</div>
								</a>
							</div>

						<?php } ?>

					</div>

				</div>

			</div>

			<!-- <div class="row">
				<div class="col-sm-6 text-center feed img-rrss">
					<h2><a class="link animated" href="https://www.youtube.com/" target="_blank">SÍGUENOS EN YOUTUBE</a></h2>
					<img src="img/redes/video-youtube.jpg" />
				</div>
				<div class="col-sm-6 text-center feed img-rrss">
					<h2><a class="link animated" href="https://www.facebook.com/" target="_blank">SÍGUENOS EN FACEBOOK</a></h2>
					<img src="img/redes/feed-facebook.jpg" />
				</div>
			</div> -->

		</div>

	</div>

</section>
