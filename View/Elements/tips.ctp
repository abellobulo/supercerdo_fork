<section class="seccion-tips">
	
	<div class="container">

		<div class="row">
			<div class="col-sm-12 text-center wow fadeInUp">
				<h2>NUESTROS TIPS</h2>
			</div>
		</div>

		<div class="row">

			<?php for ($i = 1; $i <= 4; $i++) { ?>

				<div class="col-xs-6 col-sm-6 col-md-3 tip wow fadeInUp">

					<a class="link animated" href="<?= $this->webroot; ?>tips/index/">

						<div class="contenedor">

							<img src="http://via.placeholder.com/268x278" alt="Tip <?= $i; ?>" class="base animated" />

							<div class="capa animated">

								<div class="contenido animated">

									<h3 class="sin-margin-bottom">Nombre del Tip</h3>

									<p>Curabitur lobortis id lorem id bibendum. Ut id consectetur magna. Quisque volutpat augue enim, pulvinar lobortis nibh lacinia at. Vestibulum</p>

									<span>VER TIP</span>

								</div>

							</div>

						</div>

						<div class="ranking animated">
							<?php for ($j = 0; $j < 4; $j++) { ?>
								<i class="fa fa-star"></i>
							<?php } ?>
							<i class="fa fa-star-o"></i>
						</div>

					</a>

				</div>

			<?php } ?>

		</div>

	</div>

</section>