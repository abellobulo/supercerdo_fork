<? header('Content-type: text/html; charset=UTF-8');  ?>
<!doctype html>
<html>
	<head>
		<?
			$title = ( ! empty($seo['title_seo']) ? $seo['title_seo'] : 'Súper Cerdo');
		?>
		<meta charset="utf-8">
		<title><?= $title; ?></title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
		<?= $this->Html->meta('favicon.ico', '/favicon.ico', array('type' => 'icon')); ?>
		<!-- Call CSS -->
		<?= $this->Html->css(array(
			'bootstrap.min', 'font-awesome.min', 'bs-slider', 'animate', 'owl.carousel', 'owl.theme', 'main'
		)); ?>
		<?= $this->Html->scriptBlock(sprintf("var webroot = '%s';", $this->webroot)); ?>
		<?= $this->Html->scriptBlock(sprintf("var fullwebroot = '%s';", $this->Html->url('/', true))); ?>
		<?= $this->fetch('meta'); ?>
		<?= $this->fetch('css'); ?>
	</head>
	<body>
		<?= $this->element('menu'); ?>
		<?= $this->fetch('content'); ?>
		<?= $this->element('footer'); ?>
	</body>
		<!-- Call JS -->
		<?= $this->Html->script(array(
			'jquery-3.1.1.min', 'bootstrap.min', 'bs-slider', 'wow.min', 'SmoothScroll', 'owl.carousel', 'jquery-ui', 'main'
		)); ?>
		<?= $this->fetch('script'); ?>

    <script src="../../js/jquery.columnizer.js" type="text/javascript"></script>

		<?php if (isset($CFG_MenuActivo)) { ?>
			<script type="text/javascript">
				$(document).ready(function() {
					$("#<?= $CFG_MenuActivo; ?>").addClass("active");
				});
			</script>
		<?php } ?>

		<script type="text/javascript">

			function pagination_history(number,per_page) {
				$(".history_content").css('display', 'none');
				$(".num").removeClass('active');

				var number_show = number*per_page;
				for (var i = 0; i < per_page; i++) {
					number_show --;
					$(".history_content:eq("+number_show+")").css('display', 'block');
					$(".history_content:eq("+number_show+")").css('visibility', 'visible');
					$("#pag_" + number).addClass('active')
				}
			}

			var per_page = 4;
			var numbers = $( ".history_content" ).length / per_page;
			numbers = parseInt(numbers.toFixed(0));
			var elements = "";
			$('#pagination_history').empty();
			for (var i = 1; i < numbers + 1 ; i++) {
				var ac
				elements += "<a class='link animated num angle' id='pag_"+ i +"' onclick='pagination_history("+ i +","+ per_page +");'>"+ i +"</a> ";
			}
			$('#pagination_history').append(elements);
			pagination_history(1,per_page);


			////////////////////////////////





			var generateUrl = function () {
			    return function (request, response) {
			        parent.completer = $(this.element);

			        var url = parent.completer.attr('data-url') + 'term:%s',
			            param_field = parent.completer.attr('data-param');

			        url = url.replace('%s', encodeURIComponent(request.term));

			        if (param_field !== undefined) {
			            url += '/' + param_field + ':' + $('#' + param_field).val();
			        }

			        console.log(url);

			        return $.get(url, response, 'json');
			    };
			};

			 var completer = null;
			    $("#auto-complete").autocomplete({
			        source: generateUrl(),
			        minLength: 2, //This is the min amount of chars before auto complete kicks in
			        autoFocus: true,
			        type: 'json',
			        select: function (event, ui) {
			            $('#' + parent.completer.attr('data-id-map')).val(ui.item.id)
			        }
			    });


		</script>

</html>
