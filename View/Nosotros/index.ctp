<!-- 
	NOSOTROS - index.ctp
	Controller: nosotros
	Action: index
	Admin: false
-->


<div id="bootstrap-touch-slider" class="carousel bs-slider slide control-round indicators-line banner-nosotros" data-ride="carousel" data-pause="hover" data-interval="8000">

	<div class="carousel-inner" role="listbox">

		<div class="item active" style="background: url(img/template/nosotros.jpeg);">
			<div class="bs-slider-overlay"></div>
			<div class="container">
				<div class="slide-text slide_style_center col-sm-12">
					<h1 data-animation="animated fadeInDown">Nosotros</h1>
				</div>
			</div>
		</div>

	</div>

</div>

<section class="breadcrums">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<a class="link animated" href="">Home</a> / Nosotros
			</div>
		</div>
	</div>
</section>

<section class="interior-nosotros" style="background: url(<?= $this->webroot; ?>img/template/fondo-supercerd0-4.jpeg);
    background-position: top center !important;
    background-size: cover !important;
    background-attachment: fixed !important;">
	
	<div class="capa-fondo">

		<div class="contenedor-interior-nosotros">
		
			<div class="container">

				<div class="row">

					<div class="col-xs-12">

						<div class="tabs contenedor-tabs">

						    <ul class="nav nav-tabs">
						        <li class="active"><a href="#tab-historia" data-toggle="tab" class="link animated"><div class="icon-historia icon-nosotros"></div>Historia</a>
						        </li>
						        <li><a href="#tab-planta" data-toggle="tab" class="link animated"><div class="icon-planta icon-nosotros"></div>Planta</a></li>
						        <li><a href="#tab-distribucion" data-toggle="tab" class="link animated"><div class="icon-distribucion icon-nosotros"></div>Distribución</a></li>
						        <li><a href="#tab-calidad" data-toggle="tab" class="link animated"><div class="icon-calidad icon-nosotros"></div>Calidad</a></li>
						        <li><a href="#tab-manejo" data-toggle="tab" class="link animated"><div class="icon-manejo icon-nosotros"></div>Manejo Seguro</a></li>
						    </ul>

				    		<div class="tab-content tab-nosotros">

				    			<div class="tab-pane active" id="tab-historia">
				    				<div class="col-sm-12 col-md-6">
				    					<h2>Historia</h2>
				    					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum commodo elementum ornare. Suspendisse elementum erat ut sagittis iaculis. Vestibulum non leo vel sapien laoreet maximus. Suspendisse auctor nisl at lectus dictum suscipit non id massa. Donec mattis tempor quam, in pharetra leo auctor vitae. Aenean hendrerit nisi vitae purus tincidunt, sed eleifend ante efficitur. Suspendisse porta urna sit amet convallis maximus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Fusce congue dolor est, et egestas dolor suscipit ut. Nullam auctor pharetra est in pharetra.</p>
				    					<p>Ut id velit purus. Donec faucibus risus nibh, in posuere sem convallis posuere. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed gravida egestas quam sed finibus. Etiam fringilla risus velit, ac imperdiet tellus fermentum ut. Mauris nec tellus blandit nibh sagittis sagittis eu quis massa. Sed aliquam luctus ante, molestie euismod dolor facilisis non. Nam vitae interdum ante. Donec fringilla ex pellentesque lacus gravida blandit. Vivamus feugiat eu tellus et mattis. Integer consequat auctor ante, sed rhoncus sapien faucibus non. Vestibulum ac lorem bibendum, facilisis nibh eget, tincidunt</p>
				    				</div>
				    				<div class="col-sm-12 col-md-6 text-center">
				    					<img src="http://via.placeholder.com/630x380" />
				    				</div>
				    			</div>

				    			<div class="tab-pane" id="tab-planta">
				    				<div class="col-sm-12 col-md-6">
				    					<h2>Planta</h2>
				    					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum commodo elementum ornare. Suspendisse elementum erat ut sagittis iaculis. Vestibulum non leo vel sapien laoreet maximus. Suspendisse auctor nisl at lectus dictum suscipit non id massa. Donec mattis tempor quam, in pharetra leo auctor vitae. Aenean hendrerit nisi vitae purus tincidunt, sed eleifend ante efficitur. Suspendisse porta urna sit amet convallis maximus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Fusce congue dolor est, et egestas dolor suscipit ut. Nullam auctor pharetra est in pharetra.</p>
				    					<p>Ut id velit purus. Donec faucibus risus nibh, in posuere sem convallis posuere. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed gravida egestas quam sed finibus. Etiam fringilla risus velit, ac imperdiet tellus fermentum ut. Mauris nec tellus blandit nibh sagittis sagittis eu quis massa. Sed aliquam luctus ante, molestie euismod dolor facilisis non. Nam vitae interdum ante. Donec fringilla ex pellentesque lacus gravida blandit. Vivamus feugiat eu tellus et mattis. Integer consequat auctor ante, sed rhoncus sapien faucibus non. Vestibulum ac lorem bibendum, facilisis nibh eget, tincidunt</p>
				    				</div>
				    				<div class="col-sm-12 col-md-6 text-center">
				    					<img src="http://via.placeholder.com/630x380" />
				    				</div>
				    			</div>

				    			<div class="tab-pane" id="tab-distribucion">
				    				<div class="col-sm-12 col-md-6">
				    					<h2>Distribución</h2>
				    					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum commodo elementum ornare. Suspendisse elementum erat ut sagittis iaculis. Vestibulum non leo vel sapien laoreet maximus. Suspendisse auctor nisl at lectus dictum suscipit non id massa. Donec mattis tempor quam, in pharetra leo auctor vitae. Aenean hendrerit nisi vitae purus tincidunt, sed eleifend ante efficitur. Suspendisse porta urna sit amet convallis maximus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Fusce congue dolor est, et egestas dolor suscipit ut. Nullam auctor pharetra est in pharetra.</p>
				    					<p>Ut id velit purus. Donec faucibus risus nibh, in posuere sem convallis posuere. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed gravida egestas quam sed finibus. Etiam fringilla risus velit, ac imperdiet tellus fermentum ut. Mauris nec tellus blandit nibh sagittis sagittis eu quis massa. Sed aliquam luctus ante, molestie euismod dolor facilisis non. Nam vitae interdum ante. Donec fringilla ex pellentesque lacus gravida blandit. Vivamus feugiat eu tellus et mattis. Integer consequat auctor ante, sed rhoncus sapien faucibus non. Vestibulum ac lorem bibendum, facilisis nibh eget, tincidunt</p>
				    				</div>
				    				<div class="col-sm-12 col-md-6 text-center">
				    					<img src="http://via.placeholder.com/630x380" />
				    				</div>
				    			</div>

				    			<div class="tab-pane" id="tab-calidad">
				    				<div class="col-sm-12 col-md-6">
				    					<h2>Calidad</h2>
				    					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum commodo elementum ornare. Suspendisse elementum erat ut sagittis iaculis. Vestibulum non leo vel sapien laoreet maximus. Suspendisse auctor nisl at lectus dictum suscipit non id massa. Donec mattis tempor quam, in pharetra leo auctor vitae. Aenean hendrerit nisi vitae purus tincidunt, sed eleifend ante efficitur. Suspendisse porta urna sit amet convallis maximus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Fusce congue dolor est, et egestas dolor suscipit ut. Nullam auctor pharetra est in pharetra.</p>
				    					<p>Ut id velit purus. Donec faucibus risus nibh, in posuere sem convallis posuere. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed gravida egestas quam sed finibus. Etiam fringilla risus velit, ac imperdiet tellus fermentum ut. Mauris nec tellus blandit nibh sagittis sagittis eu quis massa. Sed aliquam luctus ante, molestie euismod dolor facilisis non. Nam vitae interdum ante. Donec fringilla ex pellentesque lacus gravida blandit. Vivamus feugiat eu tellus et mattis. Integer consequat auctor ante, sed rhoncus sapien faucibus non. Vestibulum ac lorem bibendum, facilisis nibh eget, tincidunt</p>
				    				</div>
				    				<div class="col-sm-12 col-md-6 text-center">
				    					<img src="http://via.placeholder.com/630x380" />
				    				</div>
				    			</div>

				    			<div class="tab-pane" id="tab-manejo">
				    				<div class="col-sm-12 col-md-6">
				    					<h2>Manejo Seguro</h2>
				    					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum commodo elementum ornare. Suspendisse elementum erat ut sagittis iaculis. Vestibulum non leo vel sapien laoreet maximus. Suspendisse auctor nisl at lectus dictum suscipit non id massa. Donec mattis tempor quam, in pharetra leo auctor vitae. Aenean hendrerit nisi vitae purus tincidunt, sed eleifend ante efficitur. Suspendisse porta urna sit amet convallis maximus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Fusce congue dolor est, et egestas dolor suscipit ut. Nullam auctor pharetra est in pharetra.</p>
				    					<p>Ut id velit purus. Donec faucibus risus nibh, in posuere sem convallis posuere. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed gravida egestas quam sed finibus. Etiam fringilla risus velit, ac imperdiet tellus fermentum ut. Mauris nec tellus blandit nibh sagittis sagittis eu quis massa. Sed aliquam luctus ante, molestie euismod dolor facilisis non. Nam vitae interdum ante. Donec fringilla ex pellentesque lacus gravida blandit. Vivamus feugiat eu tellus et mattis. Integer consequat auctor ante, sed rhoncus sapien faucibus non. Vestibulum ac lorem bibendum, facilisis nibh eget, tincidunt</p>
				    				</div>
				    				<div class="col-sm-12 col-md-6 text-center">
				    					<img src="http://via.placeholder.com/630x380" />
				    				</div>
				    			</div>

				    		</div>

				    	</div>

					</div>

				</div>

			</div>
		</div>
	</div>

</section>

<?= $this->element('seccion_marcas'); ?>

