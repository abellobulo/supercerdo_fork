

<div class="page-title">
	<h2><span class="fa fa-list"></span> Noticias</h2>
</div>

<div class="page-content-wrap">
    <div class="row">
        <div class="col-md-12">
        	<?= $this->Form->create('Noticia', array('class' => 'form-horizontal', 'type' => 'file', 'inputDefaults' => array('label' => false, 'div' => false, 'class' => 'form-control'))); ?>

        		<div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><span class="fa fa-mail-forward"></span><strong> Nueva</strong> Noticia </h3>
                    </div>
                    <div class="panel-body">
                        <p>Para la imagen se recomienda utilizar extensiones .png y jpg no mayor a <code>500 KB</code>.</p>
                    </div>
                    <div class="panel-body form-group-separated">   

                    	 <div class="row">
                            
                            <div class="col-md-12">

                               <div class="form-group">
                                    <label class="col-md-3 col-xs-12 control-label">Título</label>
                                    <div class="col-md-6 col-xs-12">                                
                                    	<?= $this->Form->input('titulo', array('class' => 'form-control')); ?>
                                    </div>
                                </div>

                                 <div class="form-group">
                                    <label class="col-md-3 col-xs-12 control-label">Bajada</label>
                                    <div class="col-md-6 col-xs-12">                                
                                    	<?= $this->Form->input('bajada', array('class' => 'form-control')); ?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 col-xs-12 control-label">Descripción</label>
                                    <div class="col-md-6 col-xs-12">                                
                                        <?= $this->Form->input('descripcion', array('class' => 'form-control summernote')); ?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 col-xs-12 control-label">Imagen (1920 x 565)</label>
                                    <div class="col-md-6 col-xs-12">                                
                                    	<?= $this->Form->input('imagen', array('type' => 'file', 'class'=>'file', 'data-preview-file-type' => 'any', 'multiple')); ?>
                                    	 <span class="help-block">Esta imagen se utilizará como referencia del corte</span>
                                    </div>
                                    
                                </div>

                                <h3 style="text-align: center;">Configuración SEO</h3>

                                     <?= $this->Form->input('Seo.modulo', array('type' => 'hidden', 'value' => 'Noticia')); ?>
                                    
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Título</label>
                                        <div class="col-md-6 col-xs-12">                                
                                            <?= $this->Form->input('Seo.titulo_seo', array('class' => 'form-control')); ?>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Descripción</label>
                                        <div class="col-md-6 col-xs-12">                                
                                            <?= $this->Form->input('Seo.descripcion', array('class' => 'form-control', 'type' => 'textarea')); ?>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Palabras Claves</label>
                                        <div class="col-md-6 col-xs-12">                                
                                            <?= $this->Form->input('Seo.palabras', array('class' => 'form-control')); ?>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Slug</label>
                                        <div class="col-md-6 col-xs-12">                                
                                            <?= $this->Form->input('Seo.slug', array('class' => 'form-control')); ?>
                                        </div>
                                    </div>

                            </div>
                            
                        </div>

                    </div>
                    <div class="panel-footer">
                        <div class="pull-right">
							<input type="submit" class="btn btn-primary esperar-carga" autocomplete="off" data-loading-text="Espera un momento..." value="Guardar cambios">
							<?= $this->Html->link('Cancelar', array('action' => 'index'), array('class' => 'btn btn-danger')); ?>
						</div>
                    </div>
                </div>

        	<?= $this->Form->end(); ?>
        </div>
    </div>
</div>


