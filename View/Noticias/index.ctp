
<div id="bootstrap-touch-slider" class="carousel bs-slider slide control-round indicators-line banner-noticias" data-ride="carousel" data-pause="hover" data-interval="8000">

	<div class="carousel-inner" role="listbox">

		<div class="item active">
			<div class="container">
				<div class="slide-text slide_style_center col-sm-12">
					<h1 data-animation="animated fadeInDown">Noticias</h1>
				</div>
			</div>
		</div>

	</div>

</div>

<div class="section-noticia-destacada">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-6 p-0">
        <div class="container-img-noticia" style="background: url(http://via.placeholder.com/550x550)">
          <a href="#">Ver más</a>
        </div>
      </div>
      <div class="col-lg-6 p-0">
        <div class="container-info-noticia">
          <h4>CHILE SIGUE GANANDO NO EXENTO DE DIFICULTADES</h4>
          <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no seaest Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua, ugo consetetur Lorem Ipsum.</p>
        </div>
      </div>
    </div>
  </div>
</div>

<section class="seccion-noticias">

	<div class="capa-fondo">

		<div class="container-fluid">

			<div class="row">

				<? foreach ($noticias as $key => $noticia) { ?>

					<div class="col-xs-12 col-sm-6 col-md-4 p-0 noticia wow fadeInUp">

						<a class="link animated" href="<?= sprintf('%snoticias/view/%s',$this->webroot,$noticia['Noticia']['id']) ?>">

							<div class="contenedor">

								<?= $this->Html->image($noticia['Noticia']['imagen']['catalogo'], array('class' => 'base animated', 'alt' => existe($noticia['Noticia']['titulo']))); ?>

								<div class="capa animated">

									<div class="contenido animated">

										<h3 class="sin-margin-bottom"><?= existe($noticia['Noticia']['titulo']); ?></h3>

										<!-- <p><?= truncate(existe($noticia['Noticia']['bajada']),230); ?></p> -->

										<span>Ver Más</span>

										<div class="break"></div>

									</div>

								</div>

							</div>

						</a>

					</div>

				<?php } ?>

			</div>




		</div>

	</div>

</section>



<?= $this->element('seccion_marcas'); ?>
