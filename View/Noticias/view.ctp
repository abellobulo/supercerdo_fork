<!--
	NOTOCIAS - view.ctp
	Controller: noticias
	Action: view
	Admin: false
-->

<div id="bootstrap-touch-slider" class="carousel bs-slider slide control-round indicators-line banner-noticias" data-ride="carousel" data-pause="hover" data-interval="8000">

	<div class="carousel-inner" role="listbox">

		<div class="item active">
			<div class="container">
				<div class="slide-text slide_style_center col-sm-12">
					<h1 data-animation="animated fadeInDown">Noticias</h1>
				</div>
			</div>
		</div>

	</div>

</div>

<div class="section-noticia-interior">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-6 p-0">
        <div class="container-img-noticia"  style="background: url(<?= sprintf('%simg/%s',$this->webroot,$noticia['Noticia']['imagen']['path']) ?>);">
        </div>
      </div>
      <div class="col-lg-6 p-0">
        <div class="container-info-noticia">
          <h1 data-animation="animated fadeInDown"><?= existe($noticia['Noticia']['titulo']); ?></h1>

          <p><?= existe($noticia['Noticia']['bajada']); ?></p>
        </div>
      </div>
    </div>
  </div>
</div>


<section class="interior-noticia" >

	<div class="">

		<div class="container-fluid">

			<div class="row">

				<div class="col-sm-12 col-md-9 contenido-noticia">

					<p class="wow fadeInUp"><?= existe($noticia['Noticia']['descripcion']); ?></p>

          <div class="row">
      			<div class="col-sm-12 col-md-12 col-lg-12 interior-receta-compartir">
      				COMPARTIR
      				<a class="link animated" href="https://www.facebook.com/" target="_blank"><i class="fa fa-facebook"></i></a>
      				<a class="link animated" href="https://www.youtube.com/" target="_blank"><i class="fa fa-youtube"></i></a>
      				<a class="link animated" href="https://www.instagram.com/" target="_blank"><i class="fa fa-instagram"></i></a>
      				<a class="link animated" href="" target="_blank"><i class="fa fa-whatsapp"></i></a>
      			</div>
      		</div>

				</div>

				<div class="col-sm-12 col-md-3 seccion-noticias">

					<? foreach ($ultimas_noticias as $key => $noticia) { ?>

						<div class="col-xs-12 col-sm-6 col-md-12 noticia wow fadeInUp">

							<a class="link animated" href="<?= sprintf('%snoticias/view/%s',$this->webroot,$noticia['Noticia']['id']) ?>">

								<div class="contenedor">

									<?= $this->Html->image($noticia['Noticia']['imagen']['catalogo'], array('class' => 'base animated', 'alt' => existe($noticia['Noticia']['titulo']))); ?>

									<div class="capa animated">

										<div class="contenido contenido-recetas-dest-int animated ">

											<h3 class="sin-margin-bottom"><?= existe($noticia['Noticia']['titulo']); ?></h3>

											<p><?= truncate(existe($noticia['Noticia']['bajada']),230); ?></p>

											<span>Ver Más</span>

											<div class="break"></div>

										</div>

									</div>

								</div>

							</a>

						</div>

					<?php } ?>



				</div>

			</div>

		</div>

	</div>

</section>


<?= $this->element('seccion_marcas'); ?>
