<div class="page-title">
	<h2><span class="fa fa-list"></span> Nutricional Productos</h2>
</div>

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">Editar Nutricional Producto</h3>
	</div>
	<div class="panel-body">
		<div class="table-responsive">
			<?= $this->Form->create('NutricionalProducto', array('class' => 'form-horizontal', 'type' => 'file', 'inputDefaults' => array('label' => false, 'div' => false, 'class' => 'form-control'))); ?>
				<table class="table">
					<?= $this->Form->input('id'); ?>
					<tr>
						<th><?= $this->Form->label('nombre', 'Nombre'); ?></th>
						<td><?= $this->Form->input('nombre'); ?></td>
					</tr>
					<tr>
						<th><?= $this->Form->label('medida', 'Medida'); ?></th>
						<td><?= $this->Form->input('medida'); ?></td>
					</tr>
					<tr>
						<th><?= $this->Form->label('100g', '100g'); ?></th>
						<td><?= $this->Form->input('100g'); ?></td>
					</tr>
					<tr>
						<th><?= $this->Form->label('procion', 'Procion'); ?></th>
						<td><?= $this->Form->input('procion'); ?></td>
					</tr>
					<tr>
						<th><?= $this->Form->label('cda', 'Cda'); ?></th>
						<td><?= $this->Form->input('cda'); ?></td>
					</tr>
					<tr>
						<th><?= $this->Form->label('activo', 'Activo'); ?></th>
						<td><?= $this->Form->input('activo', array('class' => 'icheckbox')); ?></td>
					</tr>
					<tr>
						<th><?= $this->Form->label('producto_id', 'Producto'); ?></th>
						<td><?= $this->Form->input('producto_id', array('class' => 'form-control select')); ?></td>
					</tr>
				</table>

				<div class="pull-right">
					<input type="submit" class="btn btn-primary esperar-carga" autocomplete="off" data-loading-text="Espera un momento..." value="Guardar cambios">
					<?= $this->Html->link('Cancelar', array('action' => 'index'), array('class' => 'btn btn-danger')); ?>
				</div>
			<?= $this->Form->end(); ?>
		</div>
	</div>
</div>
