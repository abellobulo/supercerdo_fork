<!-- 
	* Admin Home
	* Ene 2018
	* Controller: pages
	* Action: home
	* Admin: true
-->

<?= $this->element('admin_breadcrumb'); ?>

<section class="page-title">
	<h2><span class="fa fa-list"></span> Home</h2>
</section>

<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap admin-home">
	 <div class="row">
         <div class="col-md-12">

			 <!-- START CONTENT FRAME -->
                <div class="content-frame">   
                    
                    <!-- START CONTENT FRAME TOP -->
                    <div class="content-frame-top">                        
                        <div class="page-title">                    
                            <h2><span class="fa fa-image"></span> Banners / Carrusel</h2>
                        </div>                                      
                    </div>
                    
                    <!-- START CONTENT FRAME RIGHT -->
                    <div class="content-frame-right">                        
                        <div class="block push-up-10">
                            <div action="upload.php" class="dropzone dropzone-mini">
                            	<?= $this->Html->link(
                            		'<div class="dz-default dz-message"><span>Subir Nuevo Banner</span></div>',
                            		array('controller' => 'banners', 'action' => 'add', 'home', $redirect_page['controller'], $redirect_page['action'], $redirect_page['param']),
                            		array(
                            			'escape'	=> false, 
                            			'class'		=>	'nuevo_banner'
                            		)
                            	); ?>
                            </div>
                        </div>                        
                        <div class="block push-up-10">
                            <div class="list-group border-bottom push-down-20">
                                <a href="<?= $this->webroot; ?>admin/pages/home" class="list-group-item active">Todos <span class="badge badge-primary"><?= existe($banner_todos) ?></span></a>
                                <a href="<?= $this->webroot; ?>admin/pages/home/1" class="list-group-item">Activos <span class="badge badge-primary"><?= existe($banner_activos); ?></span></a>
                                <a href="<?= $this->webroot; ?>admin/pages/home/0" class="list-group-item">Inactivos <span class="badge badge-primary"><?= existe($banner_inactivos); ?></span></a>
                            </div> 
                        </div>  

                        <?= $this->element('admin_data_seo'); ?>                                           
                        
                    </div>
                    <!-- END CONTENT FRAME RIGHT -->
                
                    <!-- START CONTENT FRAME BODY -->
                    <div class="content-frame-body content-frame-body-left">
                         <div class="gallery" id="links">

                        	<? foreach ($banners as $key => $banner) { 
                                $hr = ( $banner['Banner']['activo'] ? 'activo' : 'no-activo');
                            ?>

                                <div class="col-md-3 col-xs-6">
                                    <div class="row">
                                        <a class="gallery-item" href="<?= sprintf('%simg/%s',$this->webroot,$banner['Banner']['imagen']['path']) ?>" title="Nature Image 1" data-gallery>
                                            <div class="image">  
                                                <? if ( ! empty($banner['Banner']['imagen']) ) { ?> 
                                                    <?= $this->Html->image($banner['Banner']['imagen']['admin']); ?>
                                                <? } else { ?>
                                                    <?= $this->Html->image('template/SUPER_CERDO_250.jpg'); ?>
                                                <? } ?>
                                            </div>

                                        </a>

                                    </div>
                                    <div class="row">
                                        <div class="">
                                            <div class="meta">
                                                <div class="col-md-12 col-xs-12">
                                                    <strong><?= strip_tags(existe($banner['Banner']['bajada'])); ?></strong><br>
                                                    <span><?= formatoFecha($banner['Banner']['created']) ?></span>
                                                    <hr class="<?= $hr; ?>">
                                                </div>
                                                <div class="col-md-12 col-xs-12 options">

                                                    <!-- EDITAR -->
                                                    <?= $this->Html->link('<i class="fa fa-pencil"></i>', array('controller' => 'Banners', 'action' => 'edit', $banner['Banner']['id'],$redirect_page['controller'], $redirect_page['action'], $redirect_page['param']), array('class' => 'btn btn-default btn-xs', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title' => 'Editar', 'escape' => false)); ?>

                                                     <? if ( $banner['Banner']['activo'] ) : ?>

                                                        <!-- DESACTIVAR -->
                                                         <?= $this->Html->link('<i class="fa fa-ban"></i>', array('controller' => 'Banners','action' => 'desactivar', $banner['Banner']['id'], $banner['Banner']['id']), array('class' => 'btn btn-danger btn-xs', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title' => 'Desactivar', 'escape' => false)); ?>

                                                     <? else : ?>

                                                        <!-- ACTIVAR -->
                                                         <?= $this->Html->link('<i class="fa fa-check-square-o"></i>', array('controller' => 'Banners', 'action' => 'activar', $banner['Banner']['id'], $banner['Banner']['id']), array('class' => 'btn btn-success btn-xs', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title' => 'Activar', 'escape' => false)); ?>

                                                     <? endif; ?>
                                                     <!--<button type="button" class="btn btn-warning btn-xs" data-toggle="tooltip" data-placement="top" title="Eliminar"><span class="fa fa-times"></span></button>-->
                                                </div>
                                            </div> 
                                        </div>
                                    </div>
                                </div>

                        	<? } ?>

                        </div>     
                    </div>       
                    <!-- END CONTENT FRAME BODY -->
                </div>               
                <!-- END CONTENT FRAME -->

                <!-- BLUEIMP GALLERY -->
		        <div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls">
		            <div class="slides"></div>
		            <h3 class="title"></h3>
		            <a class="prev">‹</a>
		            <a class="next">›</a>
		            <a class="close">×</a>
		            <a class="play-pause"></a>
		            <ol class="indicator"></ol>
		        </div>      
		        <!-- END BLUEIMP GALLERY -->

		</div>
	</div>

</div>
<!-- PAGE CONTENT WRAPPER -->  