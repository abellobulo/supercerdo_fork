

<div id="bootstrap-touch-slider" class="carousel bs-slider slide control-round indicators-line banner-home" data-ride="carousel" data-pause="hover" data-interval="8000">

	<ol class="carousel-indicators">
		<? foreach ($banners as $key_ind => $banner_ind) { 
			$ind_active = ( $key_ind == 0 ? 'active' : '' ); ?>
			<li data-target="#bootstrap-touch-slider" data-slide-to="<?= $key_ind; ?>" class="<?= $ind_active; ?>"></li>
		<? } ?>
	</ol>
	
	<div class="carousel-inner" role="listbox">

		<? foreach ($banners as $key => $banner) { 
				$banne_active = ( $key == 0 ? 'active' : '' );
			?>
			
			<div class="item <?= $banne_active?>" style="background: url(<?= sprintf('img/%s', $banner['Banner']['imagen']['path']); ?>);">
				<div class="bs-slider-overlay"></div>
				<div class="container">
					<div class="slide-text slide_style_center col-sm-12">
						<h1 data-animation="animated fadeInDown">
							<?= $banner['Banner']['bajada']; ?>
						</h1>
						<a href="http://supercerdo.cl/" class="btn btn-default outline" data-animation="animated fadeInUp" target="_blank">VER MÁS</span></a>
						<div class="slider-scroll text-center animated fadeInUp">
							<a class="link animated wow pulse" data-wow-iteration="infinite" data-wow-duration="4s" onclick="SliderScroll ();"><span></span></a>
						</div>
					</div>
				</div>
			</div>

		<? } ?>

	</div>

	<a class="left carousel-control animated" href="#bootstrap-touch-slider" role="button" data-slide="prev">
		<span class="fa fa-angle-left" aria-hidden="true"></span>
		<span class="sr-only">Previous</span>
	</a>
	
	<a class="right carousel-control animated" href="#bootstrap-touch-slider" role="button" data-slide="next">
		<span class="fa fa-angle-right" aria-hidden="true"></span>
		<span class="sr-only">Next</span>
	</a>

</div>

<?= $this->element('seccion_lineas'); ?>

<?= $this->element('seccion_redes'); ?>

<?= $this->element('seccion_marcas'); ?>

