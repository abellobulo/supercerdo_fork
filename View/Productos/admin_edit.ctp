<div class="page-title">
	<h2><span class="fa fa-list"></span> Productos</h2>
</div>

<div class="page-content-wrap editar_producto">
    <div class="row">
        <div class="col-md-12">
        	<?= $this->Form->create('Producto', array('class' => 'form-horizontal', 'type' => 'file', 'inputDefaults' => array('label' => false, 'div' => false, 'class' => 'form-control'))); ?>

        		<?= $this->Form->input('id'); ?>
        		<?= $this->Form->input('nutricional_eliminados', array('type' => 'hidden', 'value' => '', 'id' => 'nutricional_eliminados')); ?>
        		<?= $this->Form->input('nutricional_extra_eliminados', array('type' => 'hidden', 'value' => '')); ?>

        		<div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><span class="fa fa-mail-forward"></span><strong> Actualizar</strong> Producto <code><?= $this->request->data['Producto']['nombre']; ?></code> </h3>
                                </div>
                                <div class="panel-body">
                                    <p>Para la imagen se recomienda utilizar extensiones .jpg / .png y no mayor a 500 KB.</p>
                                </div>
                                <div class="panel-body form-group-separated">                                                                        
                                    
                                    <div class="row">
                                        
                                        <div class="col-md-6">

                                            <div class="form-group">
                                                <label class="col-md-3 col-xs-12 control-label">Linea del Producto</label>
                                                <div class="col-md-8 col-xs-12">                                
                                                    <?= $this->Form->input('lineas', array('class' => 'form-control select', 'selected' => $this->request->data['Producto']['linea_producto_id'])); ?>
                                                </div>
                                            </div>
                                            
                                           <div class="form-group">
                                                <label class="col-md-3 col-xs-12 control-label">Nombre</label>
                                                <div class="col-md-8 col-xs-12">                                
                                                	<?= $this->Form->input('nombre', array('class' => 'form-control')); ?>
                                                </div>
                                            </div>

                                             <div class="form-group">
                                                <label class="col-md-3 col-xs-12 control-label">Bajada</label>
                                                <div class="col-md-8 col-xs-12">                                
                                                	<?= $this->Form->input('bajada', array('class' => 'form-control')); ?>
                                                </div>
                                            </div>

                                             <div class="form-group">
                                                <label class="col-md-3 col-xs-12 control-label">Descripción</label>
                                                <div class="col-md-8 col-xs-12">                                
                                                	<?= $this->Form->input('descripcion', array('class' => 'form-control summernote')); ?>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 col-xs-12 control-label">Imagen (Actual)</label>
                                                <div class="col-md-8 col-xs-12">                                
                                                	<?= $this->Html->image($this->request->data['Producto']['imagen']['admin']); ?>
                                                	 <span class="help-block">Esta imagen se utilizaré en el catalogo de productos</span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 col-xs-12 control-label">Imagen (500 x 450)</label>
                                                <div class="col-md-8 col-xs-12">                                
                                                	<?= $this->Form->input('imagen', array('type' => 'file', 'class'=>'file', 'data-preview-file-type' => 'any', 'multiple')); ?>
                                                	 <span class="help-block">Esta imagen se utilizaré en el catalogo de productos</span>
                                                </div>
                                                
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 col-xs-12 control-label">Imagen Banner (Actual)</label>
                                                <div class="col-md-8 col-xs-12">                                
                                                	<?= $this->Html->image($this->request->data['Producto']['imagen_banner']['admin']); ?>
                                                	 <span class="help-block">Esta imagen se utilizaré en el catalogo de productos</span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 col-xs-12 control-label">Imagen Banner (1350 x 400)</label>
                                                <div class="col-md-8 col-xs-12">                                
                                                    <?= $this->Form->input('imagen_banner', array('type' => 'file', 'class'=>'file', 'data-preview-file-type' => 'any', 'multiple')); ?>
                                                     <span class="help-block">Esta imagen se utilizará como banner principal del producto</span>
                                                </div>
                                                
                                            </div>
                                            
                                        </div>
                                        <div class="col-md-6">
                                            
                                            <div class="form-group tabla-nutricional_titulo">  
                                             <label class="col-md-3 col-xs-12 control-label">
                                                    Tabla Nutricional <br>
                                                    <?= $this->Html->image('template/add.png', array('class' => 'add_nutriente icon_producto', 'event' => 'add', 'action' => 'nutricional')); ?>
                                                    <?= $this->Html->image('template/delete.png', array('class' => 'add_nutriente icon_producto', 'event' => 'delete', 'action' => 'nutricional')); ?>
                                               </label>

                                                <div class="col-md-8 col-xs-12 tabla-nutricional">                                
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <label class="control-label">Nombre</label>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <label class="control-label">Medida</label>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <label class="control-label">100g</label>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <label class="control-label">1 Porción</label>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <label class="control-label">GDA (*)</label>
                                                        </div>
                                                    </div>

                                                    <? 
                                                    $iteral_nutricional_edit = 0;
                                                    foreach ($this->request->data['NutricionalProducto'] as $key => $nutricional) { 
                                                    		if ( ! $nutricional['extra']){
                                                    		$iteral_nutricional_edit +=2;
                                                    	?>
                                                    		<div class="row" id='nutrientes_<?= $iteral_nutricional_edit ?>' registro="<?= $nutricional['id'] ?>">
		                                                            <div class="col-md-3">
		                                                           <?= $this->Form->input('NutricionalProducto.'.$key.'.nombre', array('class' => 'form-control', 'required')); ?>
		                                                            </div>
		                                                            <div class="col-md-2">
		                                                                <?= $this->Form->input('NutricionalProducto.'.$key.'.medida', array('class' => 'form-control', 'required')); ?>
		                                                            </div>
		                                                            <div class="col-md-2">
		                                                                <?= $this->Form->input('NutricionalProducto.'.$key.'.100g', array('class' => 'form-control')); ?>
		                                                            </div>
		                                                            <div class="col-md-2">
		                                                               <?= $this->Form->input('NutricionalProducto.'.$key.'.porcion', array('class' => 'form-control')); ?>
		                                                            </div>
		                                                            <div class="col-md-2">
		                                                                <?= $this->Form->input('NutricionalProducto.'.$key.'.cda', array('class' => 'form-control')); ?>
		                                                            </div>
		                                                    </div>

                                                    		<? } ?>
                                                    <? } ?>

                                                    
                                                </div>
                                            </div>
                                            
                                            <div class="form-group tabla-nutricional_titulo">                                        
                                               <label class="col-md-3 col-xs-12 control-label tabla-nutricional-extra">Tabla Nutricional 
                                                <span class="help-block">Datos Extras</span>
                                                    <?= $this->Html->image('template/add.png', array('class' => 'add_nutriente icon_producto', 'event' => 'add', 'action' => 'nutricional_extra')); ?>
                                                    <?= $this->Html->image('template/delete.png', array('class' => 'add_nutriente icon_producto', 'event' => 'delete', 'action' => 'nutricional_extra')); ?>
                                                </label>

                                                <div class="col-md-8 col-xs-12 tabla-nutricional">                                
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <label class="control-label">Nombre</label>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label class="control-label">Medida</label>
                                                        </div>
                                                        
                                                    </div>

                                                    <? 
                                                    $iteral_nutricional_extra_edit = 1;
                                                    foreach ($this->request->data['NutricionalProducto'] as $key_extra => $nutricional_extra) { 
                                                    		if (  $nutricional_extra['extra']){
                                                    			$iteral_nutricional_extra_edit += 2;
                                                    	?>
	                                                    		<div class="row" id="nutrientes_extra_<?= $iteral_nutricional_extra_edit; ?>" registro="<?= $nutricional_extra['id'] ?>">
			                                                        <div class="col-md-6">
			                                                           <?= $this->Form->input('NutricionalProducto.'.$key_extra.'.nombre', array('class' => 'form-control', 'required')); ?>
			                                                            <?= $this->Form->input('NutricionalProducto.'.$key_extra.'.extra', array('type' => 'hidden', 'value' => 1)); ?>
			                                                        </div>
			                                                        <div class="col-md-6">
			                                                           <?= $this->Form->input('NutricionalProducto.'.$key_extra.'.medida', array('class' => 'form-control')); ?>
			                                                        </div>
			                                                       
			                                                    </div>
                                                    		<? } ?>
                                                    <? } ?>

                                                    
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 col-xs-12 control-label">Recomendado para hacer</label>
                                                <div class="col-md-8 col-xs-12">                                
                                                    <?= $this->Form->input('para_producto', array('class' => 'form-control select', 'name' => 'ProductoParaProducto[]', 'multiple', 'options' => array('Horno', 'Parrilla'), 'selected' => $paraProducto)); ?>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 col-xs-12 control-label">Productos para probar</label>
                                                <div class="col-md-8 col-xs-12">                                
                                                    <?= $this->Form->input('probar_producto', array('class' => 'form-control select', 'name' =>  'ProductoProbarProducto[]', 'multiple', 'options' => $productos, 'selected' => $probar_producto)); ?>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 col-xs-12 control-label">Recomentar Recetas</label>
                                                <div class="col-md-8 col-xs-12">                                
                                                    <?= $this->Form->input('recomendar_receta', array('class' => 'form-control select', 'name' => 'ProductoRecomendarReceta[]', 'multiple', 'options' => $recetas, 'selected' => $receta_prodcuto)); ?>
                                                </div>
                                            </div>

                                            <hr>

                                            <h3 style="text-align: center;">Configuración SEO</h3>

                                            <?= $this->Form->input('Seo.id', array('type' => 'hidden', 'value' => $seo_producto['Seo']['id'])); ?>

                                            <div class="form-group">
                                                <label class="col-md-3 col-xs-12 control-label">Título</label>
                                                <div class="col-md-8 col-xs-12">                                
                                                    <?= $this->Form->input('Seo.titulo_seo', array('class' => 'form-control', 'value' => $seo_producto['Seo']['titulo_seo'])); ?>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 col-xs-12 control-label">Descripción</label>
                                                <div class="col-md-8 col-xs-12">                                
                                                    <?= $this->Form->input('Seo.descripcion', array('class' => 'form-control', 'type' => 'textarea', 'value' => $seo_producto['Seo']['descripcion'])); ?>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 col-xs-12 control-label">Palabras Claves</label>
                                                <div class="col-md-8 col-xs-12">                                
                                                    <?= $this->Form->input('Seo.palabras', array('class' => 'form-control', 'value' => $seo_producto['Seo']['palabras'])); ?>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 col-xs-12 control-label">Slug</label>
                                                <div class="col-md-8 col-xs-12">                                
                                                    <?= $this->Form->input('Seo.slug', array('class' => 'form-control', 'value' => $seo_producto['Seo']['slug'])); ?>
                                                </div>
                                            </div>

                                        </div>
                                        
                                    </div>

                                </div>
                                <div class="panel-footer">
                                    <div class="pull-right">
										<input type="submit" class="btn btn-primary esperar-carga" autocomplete="off" data-loading-text="Espera un momento..." value="Guardar cambios">
										<?= $this->Html->link('Cancelar', array('action' => 'index'), array('class' => 'btn btn-danger')); ?>
									</div>
                                </div>
                            </div>

        	<?= $this->Form->end(); ?>
        </div>
    </div>
</div>

<!-- CANTIDAD DE NUTRIENTES -->
<?= $this->Html->scriptBlock(sprintf("var iteral_nutricional_edit = '%s';", $iteral_nutricional_edit)); ?>
<!-- CANTIDAD DE NUTRIENTES EXTRA -->
<?= $this->Html->scriptBlock(sprintf("var iteral_nutricional_extra_edit = '%s';", $iteral_nutricional_extra_edit)); ?>

