<!-- 
	* Admin Index Productos
	* Ene 2018
	* Controller: productos
	* Action: admin_index
	* Admin: true
-->  

<?= $this->element('admin_breadcrumb'); ?>

<div class="page-title">
	<h2><span class="fa fa-list"></span> Lista Productos</h2>
</div>


<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap admin-seccion-producto">
	 <div class="row">
         <div class="col-md-12">

         	 <!-- START CONTENT FRAME -->
                <div class="content-frame">   
                    
                    <!-- START CONTENT FRAME TOP -->
                    <div class="content-frame-top">                        
                        <div class="page-title">                    
                            <h2><span class="fa fa-image"></span> Lista de productos</h2>
                        </div>                                      
                    </div>

                     <!-- START CONTENT FRAME RIGHT -->
                    <div class="content-frame-right">                        
                        <div class="block push-up-10">
                            <div action="upload.php" class="dropzone dropzone-mini">
                            	<?= $this->Html->link(
                            		'<div class="dz-default dz-message"><h4>Subir Nuevo Producto</h4></div>',
                            		array('controller' => 'productos', 'action' => 'add'),
                            		array(
                            			'escape'	=> false, 
                            			'class'		=>	'nuevo_banner'
                            		)
                            	); ?>
                            </div>
                        </div>       

                        <div class="block push-up-10">
                            <h4 style="text-align: center;">Cantidad de productos</h4>
                            <div class="list-group border-bottom push-down-20">
                                <a href="<?= $this->webroot ?>admin/Productos/index" class="list-group-item active">Todos <span class="badge badge-primary"><?= $cantidad_productos['cantidad_producto'] ?></span></a>
                                <a href="<?= $this->webroot ?>admin/Productos/index/1" class="list-group-item">Línea Cocina <span class="badge badge-primary"><?= $cantidad_productos['cantidad_producto_cocina'] ?></span></a>
                                <a href="<?= $this->webroot ?>admin/Productos/index/2" class="list-group-item">Línea Sandwich <span class="badge badge-primary"><?= $cantidad_productos['cantidad_producto_sandwich'] ?></span></a>
                                <a href="<?= $this->webroot ?>admin/Productos/index/3" class="list-group-item">Línea Parrilla <span class="badge badge-primary"><?= $cantidad_productos['cantidad_producto_parrilla'] ?></span></a>
                            </div>     
                        </div>         	       
                          
                        <div class="block push-up-10" style="text-align: center;">

                            <h4>Los productos destacados son aquellos que aparecen en primer lugar en la página de cada linea de producto</h4>

                            <div class="dropzone dropzone-mini" style="text-align: center;">

                                <?= $this->Form->create('Producto', array('class' => 'form-horizontal', 'type' => 'file', 'inputDefaults' => array('label' => false, 'div' => false, 'class' => 'form-control'))); ?>

                                    <!-- PRODUCTOS DESTACADO PARA LA LINEA COCINA -->
                                    <? if ( ! empty($destacado_cocina['Producto']['imagen']['admin']) ) { ?>
                                        <div class="form-group">
                                            <span class="help-block"><code><?= $destacado_cocina['Producto']['nombre']; ?></code></span>
                                            <div class="col-md-12 col-xs-12 producto-destacado"> 
                                                <?= $this->Html->image($destacado_cocina['Producto']['imagen']['admin'] ) ?>
                                            </div>
                                        </div>
                                    <? } ?>

                            		<div class="form-group">
                                        <h4>Seleccionar producto Cocina</h4>
                                        <div class="col-md-12 col-xs-12"> 
                                            <?= $this->Form->input('ProductoDestacado.0.lista_producto', array('type' => 'select', 'options' => $lista_producto_cocina, 'empty' => '(Selecciones un producto)', 'class' => 'form-control')); ?>
                                            <?= $this->Form->input('ProductoDestacado.0.destacado_linea', array('type' => 'hidden', 'value' => '1')); ?>
                                        </div>
                                    </div>

                                    <!-- PRODCUTOS DESTACADO PARA LA LINEA PARRILLA -->
                                    <? if ( ! empty($destacado_parrilla['Producto']['imagen']['admin']) ) { ?>
                                        <div class="form-group">
                                            <span class="help-block"><code><?= $destacado_parrilla['Producto']['nombre']; ?></code></span>
                                            <div class="col-md-12 col-xs-12 producto-destacado"> 
                                                <?= $this->Html->image($destacado_parrilla['Producto']['imagen']['admin'] ) ?>
                                            </div>
                                        </div>
                                    <? } ?>

                                    <div class="form-group">
                                        <h4>Seleccionar producto Parrilla</h4>
                                        <div class="col-md-12 col-xs-12"> 
                                            <?= $this->Form->input('ProductoDestacado.1.lista_producto', array('type' => 'select', 'options' => $lista_producto_parrilla, 'empty' => '(Selecciones un producto)', 'class' => 'form-control')); ?>
                                            <?= $this->Form->input('ProductoDestacado.1.destacado_linea', array('type' => 'hidden', 'value' => '3')); ?>
                                        </div>
                                    </div>

                                    <!-- PRODCUTOS DESTACADO PARA LA LINEA SANDWICH -->
                                    <? if ( ! empty($destacado_sandwich['Producto']['imagen']['admin']) ) { ?>
                                        <div class="form-group">
                                            <span class="help-block"><code><?= $destacado_sandwich['Producto']['nombre']; ?></code></span>
                                            <div class="col-md-12 col-xs-12 producto-destacado"> 
                                                <?= $this->Html->image($destacado_sandwich['Producto']['imagen']['admin'] ) ?>
                                            </div>
                                        </div>
                                    <? } ?>

                                    <div class="form-group">
                                        <h4>Seleccionar producto Sandwich</h4>
                                        <div class="col-md-12 col-xs-12"> 
                                            <?= $this->Form->input('ProductoDestacado.2.lista_producto', array('type' => 'select', 'options' => $lista_producto_sadwich, 'empty' => '(Selecciones un producto)', 'class' => 'form-control')); ?>
                                            <?= $this->Form->input('ProductoDestacado.2.destacado_linea', array('type' => 'hidden', 'value' => '2')); ?>
                                        </div>
                                    </div>

                                    <div class="form-group">
											<input type="submit" class="btn btn-danger esperar-carga" autocomplete="off" data-loading-text="Espera un momento..." value="Guardar cambios">
	                                </div>

                            	<?= $this->Form->end(); ?>
                            </div>
                        </div>                                          
                        
                    </div>
                    <!-- END CONTENT FRAME RIGHT -->
                
                    <!-- START CONTENT FRAME BODY -->
                    <div class="content-frame-body content-frame-body-left">

                    	<div class="gallery" id="links">

                        	<? foreach ($productos as $key => $producto) { 
                                $hr = ( $producto['Producto']['activo'] ? 'activo' : 'no-activo');
                                ?>


                                <div class="col-md-3 col-xs-12">
                                    <div class="row">
                                        <a class="gallery-item" href="<?= sprintf('%simg/%s',$this->webroot,$producto['Producto']['imagen']['path']) ?>" title="<?= $producto['Producto']['nombre'] ?>" data-gallery>
                                            <div class="image">                              
                                                <?= $this->Html->image($producto['Producto']['imagen']['admin']); ?>
                                            </div>

                                        </a>

                                    </div>
                                    <div class="row">
                                        <div class="">
                                            <div class="meta">
                                                <div class="col-md-12 col-xs-12">
                                                    <strong><?= $producto['Producto']['nombre'] ?></strong><br>
                                                    <span>Linea: <?= $producto['LineaProducto']['nombre'] ?></span>
                                                    <hr class="<?= $hr; ?>">
                                                </div>

                                                <div class="col-md-12 col-xs-12" style="text-align: center;">

                                                    <!-- EDITAR -->
                                                    <?= $this->Html->link('<i class="fa fa-pencil"></i>', array('controller' => 'Productos', 'action' => 'edit', $producto['Producto']['id']), array('class' => 'btn btn-default btn-xs', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title' => 'Editar', 'escape' => false)); ?>

                                                     <? if ( $producto['Producto']['activo'] ) : ?>

                                                        <!-- DESACTIVAR -->
                                                         <?= $this->Html->link('<i class="fa fa-ban"></i>', array('controller' => 'Banners','action' => 'desactivar', $producto['Producto']['id'], $producto['Producto']['id']), array('class' => 'btn btn-danger btn-xs', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title' => 'Desactivar', 'escape' => false)); ?>

                                                     <? else : ?>

                                                        <!-- ACTIVAR -->
                                                         <?= $this->Html->link('<i class="fa fa-check-square-o"></i>', array('controller' => 'Banners', 'action' => 'activar', $producto['Producto']['id'], $producto['Producto']['id']), array('class' => 'btn btn-success btn-xs', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title' => 'Activar', 'escape' => false)); ?>

                                                     <? endif; ?>

                                                     <?//= $this->Form->postLink('<i class="fa fa-remove"></i>', array('action' => 'delete', $producto['Producto']['id']), array('class' => 'btn btn-xs btn-warning confirmar-eliminacion', 'data-toggle'=>'tooltip','data-toggle'=>'tooltip', 'data-placement' => 'top', 'title' => 'Eliminar', 'escape' => false)); ?>

                                                </div>
                                            </div> 
                                        </div>
                                    </div>
                                </div>

                        	<? } ?>

                        </div>

                        <div class="pull-right">
							<ul class="pagination">
								<?= $this->Paginator->prev('« Anterior', array('tag' => 'li'), null, array('tag' => 'li', 'disabledTag' => 'a', 'class' => 'first disabled hidden')); ?>
								<?= $this->Paginator->numbers(array('tag' => 'li', 'currentTag' => 'a', 'modulus' => 2, 'currentClass' => 'active', 'separator' => '')); ?>
								<?= $this->Paginator->next('Siguiente »', array('tag' => 'li'), null, array('tag' => 'li', 'disabledTag' => 'a', 'class' => 'last disabled hidden')); ?>
							</ul>
						</div>

                        
                    </div>
                </div>

                <!-- BLUEIMP GALLERY -->
		         <?= $this->element('admin_blueimp_gallery'); ?>
		        <!-- END BLUEIMP GALLERY -->

          </div>
     </div>
</div>     

