<!-- 
	* Admin Seccion Productos
	* Ene 2018
	* Controller: productos
	* Action: admin_seccion_productos
	* Admin: true
-->

<?= $this->element('admin_breadcrumb'); ?>

<section class="page-title">
	<h2><span class="fa fa-list"></span> Configuración Sección Productos</h2>
</section>

<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap admin-seccion-producto">
	 <div class="row">
         <div class="col-md-12">

         	 <!-- START CONTENT FRAME -->
                <div class="content-frame">   
                    
                    <!-- START CONTENT FRAME TOP -->
                    <div class="content-frame-top">                        
                        <div class="page-title">                    
                            <h2><span class="fa fa-image"></span> Linea de productos</h2>
                        </div>                                      
                    </div>

                     <!-- START CONTENT FRAME RIGHT -->
                    <div class="content-frame-right">                        
                                               
                        <h4 style="text-align: center;">Cantidad de productos</h4>
                        <div class="list-group border-bottom push-down-20">
                            <a href="<?= $this->webroot ?>admin/Productos/index" class="list-group-item active">Todos <span class="badge badge-primary"><?= $cantidad_productos['cantidad_producto'] ?></span></a>
                            <a href="<?= $this->webroot ?>admin/Productos/index/1" class="list-group-item">Línea Cocina <span class="badge badge-primary"><?= $cantidad_productos['cantidad_producto_cocina'] ?></span></a>
                            <a href="<?= $this->webroot ?>admin/Productos/index/2" class="list-group-item">Línea Sandwich <span class="badge badge-primary"><?= $cantidad_productos['cantidad_producto_sandwich'] ?></span></a>
                            <a href="<?= $this->webroot ?>admin/Productos/index/3" class="list-group-item">Línea Parrilla <span class="badge badge-primary"><?= $cantidad_productos['cantidad_producto_parrilla'] ?></span></a>
                        </div>                                                
                        
                    </div>
                    <!-- END CONTENT FRAME RIGHT -->
                
                    <!-- START CONTENT FRAME BODY -->
                    <div class="content-frame-body content-frame-body-left">
                        
                    
                        
                        <div class="gallery" id="links">

                        	<? foreach ($linea_producto as $key => $linea) { ?>

                                <div class="col-md-4 col-xs-12">
                                    <div class="row">
                                        <a class="gallery-item" href="<?= sprintf('%simg/%s',$this->webroot,$linea['LineaProducto']['imagen']['path']) ?>" title="<?= $linea['LineaProducto']['nombre'] ?>" data-gallery>
                                            <div class="image">                              
                                                <?= $this->Html->image($linea['LineaProducto']['imagen']['admin']); ?>
                                            </div>
                                        </a>

                                    </div>
                                    <div class="row">
                                        <div class="">
                                            <div class="meta">
                                                <div class="col-md-9 col-xs-12">
                                                    <strong><?= $linea['LineaProducto']['nombre'] ?></strong><br>
                                                </div>
                                                <div class="col-md-3 col-xs-12">

                                                    <!-- EDITAR -->
                                                    <?= $this->Html->link('<i class="fa fa-pencil"></i> Editar', array('controller' => 'LineaProductos', 'action' => 'edit', $linea['LineaProducto']['id'],$redirect_page['controller'], $redirect_page['action'], $redirect_page['param']), array('class' => 'btn btn-default btn-xs', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title' => 'Editar', 'escape' => false)); ?>
                                                     
                                                </div>
                                            </div> 
                                        </div>
                                    </div>
                                </div>

                        	<? } ?>

                        </div>
                             
                        
                    </div>       
                    <!-- END CONTENT FRAME BODY -->


                    <div class="content-frame-top">                        
                        <div class="page-title">                    
                            <h2><span class="fa fa-image"></span> Página Productos</h2>
                        </div>                                      
                    </div>


                    <div class="row">
                        <div class="col-md-12">
                            
                            <div class="panel panel-default">

                                <?= $this->Form->create('Producto', array('class' => 'form-horizontal', 'type' => 'file', 'inputDefaults' => array('label' => false, 'div' => false, 'class' => 'form-control'))); ?>
                               
                                <div class="panel-body">                                                                        
                                    <div class="row">
                                        
                                            <div class="col-md-6 form-group-separated">

                                                <? if ( ! empty($banner_producto['Banner']['id']) ) { ?>

                                                    <?= $this->Form->input('id', array('value' => $banner_producto['Banner']['id'])); ?>
                                                <? } ?>

                                                <h3><span class="fa fa-mail-forward"></span> Banner Principal</h3>
                                                <p>Para el Banner se recomienda utilizar extensión <code>.jpg</code> y no mayor a <code>200 KB</code></p>
                                                
                                                <? if ( !empty($banner_producto)) { ?>
                                                    <div class="form-group">
                                                        <label class="col-md-3 col-xs-12 control-label">Banner Actual</label>
                                                        <div class="col-md-6 col-xs-12">                                            
                                                            <div class="input-group">
                                                                <?= $this->Html->image($banner_producto['Banner']['imagen']['admin'] ) ?>
                                                            </div>   
                                                            <h5><?= $banner_producto['Banner']['bajada']; ?></h5>                                         
                                                        </div>
                                                    </div>
                                                <? } ?>


                                              
                                                <div class="form-group">
                                                    <label class="col-md-3 col-xs-12 control-label">Bajada</label>
                                                    <div class="col-md-8 col-xs-12">                                
                                                        <?= $this->Form->input('bajada', array('class' => 'form-control')); ?>
                                                    </div>
                                                </div>
                                                 <div class="form-group">
                                                    <div class="col-md-12 col-xs-12" style="display: flex;justify-content: center;">   
                                                        <div class="input-group">
                                                            <label class=" control-label">Actualizar Banner (774 x 220)</label>
                                                            <?= $this->Form->input('imagen', array('type' => 'file', 'class'=>'file', 'data-preview-file-type' => 'any', 'multiple')); ?>
                                                        </div>                                            
                                                    </div>
                                                </div>
                                                
                                            </div>
                                            <div class="col-md-6 form-group-separated">
                                                
                                                <h3><span class="fa fa-mail-forward"></span> SEO</h3>
                                                 <?= $this->element('admin_data_seo'); ?>
                                                
                                            </div>
                                    </div>

                                </div>
                                <div class="panel-footer">
                                    <div class="pull-right">
                                        <input type="submit" class="btn btn-danger esperar-carga" autocomplete="off" data-loading-text="Espera un momento..." value="Guardar cambios">
                                    </div>
                                </div>

                                 <?= $this->Form->end(); ?>
                            </div>
                            
                        </div>
                    </div>    

               </div>

         </div>
     </div>
</div>