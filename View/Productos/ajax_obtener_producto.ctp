<div class="container animated producto-select-info">

	<div class="row">

		<div class="col-sm-5 col-md-5 col-md-offset-1 producto-destacado">
			<?= $this->Html->image(existe($producto['Producto']['imagen']['path']), array('width' => '65%')); ?>
		</div>

		<div class="col-sm-7 col-md-5 producto-destacado-info" data-animation="animated fadeInDown">

			<h2><?= existe($producto['Producto']['nombre']); ?></h2>

			<p><?= existe($producto['Producto']['bajada']); ?></p>

			<? if ( ! empty($producto['Producto']['id']) ) { ?>
				<?= $this->Html->link(
					'VER INFORMACIÓN',
					array('controller' => 'productos', 'action' => 'view', existe($producto['Producto']['id']) ),
					array(
						'escape' 	=> false,
						'class'		=> 'link animated btn-mas-info'
					)
				); ?>
			<? } ?>
		</div>

	</div>

</div>