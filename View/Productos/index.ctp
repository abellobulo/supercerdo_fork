<!--
	NUESTROS PRODCUTOS - index.ctp
	Controller: productos
	Action: index
	Admin: false
-->


<div id="bootstrap-touch-slider" class="carousel bs-slider slide control-round indicators-line banner-lineas-productos" data-ride="carousel" data-pause="hover" data-interval="8000">

	<div class="carousel-inner" role="listbox">
		<div class="item active" style="background: url(<?= sprintf('img/%s', existe($banner['Banner']['imagen']['path'])); ?>);">
			<div class="bs-slider-overlay"></div>
			<div class="container">
				<!--<div class="slide-text slide_style_left col-sm-12">
					<h1 data-animation="animated fadeInDown"><span class="top">Algo rico</span><br /><span class="middle">PARA</span><br /><span class="bottom">el día a día</span></h1>
				</div>-->
				<div class="slide-text col-sm-12">
					<h1 data-animation="animated fadeInDown"><?= existe($banner['Banner']['bajada']); ?></h1>
				</div>
			</div>
		</div>

	</div>

</div>

<!-- START BREADCRUMB -->
<!-- <?= $this->element('breadcrumb'); ?> -->
<!-- END BREADCRUMB -->

<section class="interior-lineas-productos">

	<div class="capa-fondo">

		<div class="container-fluid">

			<!-- <div class="row">
				<div class="col-sm-12 text-center wow fadeInUp">
					<h2>NUESTROS PRODUCTOS</h2>
				</div>
			</div> -->

			<div class="row">

				<? foreach ($linea_producto as $key => $linea) {
					$img_linea = array(
						'1' =>	'lineas-icon-cocina.png',
						'2'	=>	'lineas-icon-sandwich.png',
						'3'	=>	'lineas-icon-parrilla.png'
	 				);

	 				$link_linea = array(
						'1' =>	'linea_cocina',
						'2'	=>	'linea_sandwich',
						'3'	=>	'linea_parrilla'
	 				);
				?>

					<div class="col-sm-12 col-md-4 p-0 linea wow fadeInUp">
						<a class="link animated" href="<?= $this->webroot; ?>productos/linea_producto/<?= $link_linea[$linea['LineaProducto']['id']] ?>">
							<?= $this->Html->image($linea['LineaProducto']['imagen']['path'], array('class' => 'base animated', 'alt' => $linea['LineaProducto']['nombre'])); ?>
							<div class="capa animated">
								<div class="contenido">
									<?= $this->Html->image(sprintf('lineas-de-productos/%s',$img_linea[$linea['LineaProducto']['id']])); ?>
									<h3><?=  $linea['LineaProducto']['nombre']; ?></h3>
									<!-- <span class="animated">CONOCE NUESTRA LÍNEA</span> -->
								</div>
							</div>
						</a>
					</div>

				<? } ?>

			</div>

		</div>
	</div>

</section>


<?= $this->element('seccion_marcas'); ?>
