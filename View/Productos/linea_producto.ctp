<?
	$titulo_linea = array(
		'linea_parrilla' => 'LÍNEA PARRILLA',
		'linea_sandwich' => 'LÍNEA SANDWICH',
		'linea_cocina' => 'LÍNEA COCINA'
	);

	$fondo_linea = array(
		'linea_parrilla' => 'fondo-linea.jpg',
		'linea_sandwich' => 'lineaSandwich.jpg',
		'linea_cocina' => 'LineaCocina.jpg'
	);


 ?>

 <div id="bootstrap-touch-slider" class="carousel bs-slider slide control-round indicators-line banner-lineas-productos" data-ride="carousel" data-pause="hover" data-interval="8000">

 	<div class="carousel-inner" role="listbox">
 		<div class="item active bg-section-product">
 			<div class="bs-slider-overlay"></div>
 			<div class="container-fluid">
 				<div class="slide-text col-sm-12">
          <h1 data-animation="animated fadeInDown"><?= Ucfirst($linea_producto['LineaProducto']['nombre']); ?></h1>
 				</div>
 			</div>
 		</div>

 	</div>

 </div>


<div class="interior-linea-fondo-mobile visible-xs"></div>
<section class="interior-lineas-productos">

	<div class="capa-fondo">

		<div class="container-fluid">

			<div class="row">

				<? foreach ($lineas_productos as $key => $linea) {
					$img_linea = array(
						'1' =>	'lineas-icon-cocina.png',
						'2'	=>	'lineas-icon-sandwich.png',
						'3'	=>	'lineas-icon-parrilla.png'
	 				);

	 				$link_linea = array(
						'1' =>	'linea_cocina',
						'2'	=>	'linea_sandwich',
						'3'	=>	'linea_parrilla'
	 				);
				?>

					<div class="col-sm-4 col-xs-4 col-md-4 p-0 linea wow fadeInUp">
						<a class="link animated" href="<?= $this->webroot; ?>productos/linea_producto/<?= $link_linea[$linea['LineaProducto']['id']] ?>">
							<?= $this->Html->image($linea['LineaProducto']['imagen']['path'], array('class' => 'base animated', 'alt' => $linea['LineaProducto']['nombre'])); ?>
							<div class="capa animated">
								<div class="contenido">
									<?= $this->Html->image(sprintf('lineas-de-productos/%s',$img_linea[$linea['LineaProducto']['id']])); ?>
									<h3><?=  $linea['LineaProducto']['nombre']; ?></h3>
								</div>
							</div>
						</a>
					</div>

				<? } ?>

			</div>

		</div>
	</div>

</section>

<section class="interior-linea" style="padding-top: 140px;
  background: url( <?= sprintf('%simg/lineas-de-productos/%s', $this->webroot,$fondo_linea[$linea]); ?> );
  background-position: top center !important;
  background-size: cover !important;
  background-attachment: fixed !important;">

	<div class="lista-productos">

		<div class="container">

			<div class="row">

				<? foreach ($productos as $key => $producto) { ?>

					<div class="col-xs-6 col-sm-6 col-md-3 producto wow  ">
						<a class="link animated img-rrss" href="<?= sprintf('%sproductos/view/%s',$this->webroot,$producto['Producto']['id']) ?>" target="_self">
						<div class="link animated producto-select" idProducto="<?= existe($producto['Producto']['id']) ?>">
              <h3><?= existe($producto['Producto']['nombre']) ?></h3>
							<?= $this->Html->image(existe($producto['Producto']['imagen']['catalogo']), array('class' => 'animated')); ?>
              <span class="animated">Ver Más</span>
						</div>
						</a>
					</div>

				<?php } ?>

			</div>

		</div>

	</div>


</section>




<?= $this->element('seccion_marcas'); ?>
