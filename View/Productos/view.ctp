<!--
	PRODUCTOS - view.ctp
	Controller: productos
	Action: view
	Admin: false
-->

<?
	$titulo_linea = array(
		'3' => 'L&Iacute;NEA PARRILLA',
		'2' => 'L&Iacute;NEA SANDWICH',
		'1' => 'L&Iacute;NEA COCINA'
	);

	$fondo_linea = array(
		'3' => 'icon-banner-parrilla.png',
		'2' => 'icon-banner-parrilla.png',
		'1' => 'icon-banner-parrilla.png'
	);

	$img_linea = array(
		'1' =>	'lineas-icon-cocina.png',
		'2'	=>	'lineas-icon-sandwich.png',
		'3'	=>	'lineas-icon-parrilla.png'
	);


 ?>
 <div id="bootstrap-touch-slider" class="carousel bs-slider slide control-round indicators-line banner-interior-receta banner-view" data-ride="carousel" data-pause="hover" data-interval="8000">

  <div class="carousel-inner" role="listbox">

 	 <div class="item active" style="background: url(../../img/Banner/6/nuestros_productos_r.jpg)">

 		 <div class="bs-slider-overlay"></div>



 		 <div class="banner-contenido-bottom">

 			 <div class="container-fluid">

 				 <div class="row">

 					 <div class="col-sm-12 col-md-12 col-lg-12">



 					 </div>

 				 </div>

 			 </div>

 		 </div>

 	 </div>

  </div>

 </div>


 <section class="interior-lineas-productos">

 	<div class="capa-fondo">

 		<div class="container-fluid">

 			<div class="row">

 				<? foreach ($lineas_productos as $key => $linea) {
 					$img_linea = array(
 						'1' =>	'lineas-icon-cocina.png',
 						'2'	=>	'lineas-icon-sandwich.png',
 						'3'	=>	'lineas-icon-parrilla.png'
 	 				);

 	 				$link_linea = array(
 						'1' =>	'linea_cocina',
 						'2'	=>	'linea_sandwich',
 						'3'	=>	'linea_parrilla'
 	 				);
 				?>

 					<div class="col-sm-4 col-xs-4 col-md-4 p-0 linea wow fadeInUp">
 						<a class="link animated" href="<?= $this->webroot; ?>productos/linea_producto/<?= $link_linea[$linea['LineaProducto']['id']] ?>">
 							<?= $this->Html->image($linea['LineaProducto']['imagen']['path'], array('class' => 'base animated', 'alt' => $linea['LineaProducto']['nombre'])); ?>
 							<div class="capa animated">
 								<div class="contenido">
 									<?= $this->Html->image(sprintf('lineas-de-productos/%s',$img_linea[$linea['LineaProducto']['id']])); ?>
 									<h3><?=  $linea['LineaProducto']['nombre']; ?></h3>
 								</div>
 							</div>
 						</a>
 					</div>

 				<? } ?>

 			</div>

 		</div>
 	</div>

 </section>

<section class="interior-producto-top">

	<div class="capa-fondo">
		<div class="fondo-right wow fadeInUp hidden-xs hidden-sm"></div>

			<div class="container-fluid">

				<div class="row">

					<div class="col-sm-12 col-md-6 info-producto-left wow fadeInUp">

						<div class="text-center">
							<?= $this->Html->image($producto['Producto']['imagen']['path'], array('alt' => 'Nombre Producto', 'class' => 'img-producto')); ?>
						</div>

					</div>

					<div class="col-sm-12 col-md-6 info-producto-right wow fadeInUp">

						<h2><?= ucwords($producto['Producto']['nombre']) ?></h2>

						<p><?= $producto['Producto']['descripcion'] ?></p>


					</div>

				</div>

			</div>
	</div>

</section>

<section class="interior-producto-bottom">

	<div class="container-fluid">

		<div class="row">

      <div class="col-sm-6 col-md-6 contenedor-left wow fadeInUp">

        <div class="content-recomendaciones">

          <div class="recomendaciones">

            <div class="recomendacion">
              <h5>Se Recomienda</h5>
              <div class="imagen">
                <?= $this->Html->image('producto/icon-cocina.png'); ?>
              </div>
              <div class="imagen">
                <?= $this->Html->image('producto/icon-parrilla.png'); ?>
              </div>
            </div>

            <div class="recomendacion">
              <h5>Corte</h5>
              <div class="imagen">
                <?= $this->Html->image('producto/icon-cortes.png'); ?>
              </div>
            </div>

          </div>

        </div>


			</div>


			<div class="col-sm-6 col-md-6 contenedor-right wow fadeInUp">

        <div class="contenedor-tabla">

          <h4>Tabla Nutricional</h4>

  				<? foreach ($producto['NutricionalProducto'] as $key => $nutricionalExtra) {
  					if ($nutricionalExtra['extra']){ ?>
  							<p><?= sprintf('%s: %s',$nutricionalExtra['nombre'], $nutricionalExtra['medida'] ) ?></p>
  					<? } ?>
  				<? } ?>

  				<table>

  					<thead>
  						<tr>
  							<th></th>
  							<th></th>
  							<th>100g</th>
  							<th>1 Porción</th>
  							<th>GDA(*)</th>
  						</tr>
  					</thead>

  					<tbody>

  						<? foreach ($producto['NutricionalProducto'] as $key_nutricional => $nutricional) {
  							if ( ! $nutricional['extra']){ ?>
  								<tr>
  									<td><?= existe($nutricional['nombre']); ?></td>
  									<td><?= existe($nutricional['medida']); ?></td>
  									<td><?= existe($nutricional['100g']); ?></td>
  									<td><?= existe($nutricional['porcion']); ?></td>
  									<td><?= existe($nutricional['cda']); ?></td>
  								</tr>
  							<? } ?>
  						<?php } ?>

  					</tbody>

  				</table>

        </div>

			</div>

		</div>

	</div>

</section>

<?= $this->element('receta_recomendadas'); ?>

<section class="seccion-productos" >

	<div class="capa-fondo">
		<div class="container-fluid">

			<div class="row">
				<div class="col-sm-12 text-center wow fadeInUp">
					<h2>PRUEBA TAMBIÉN</h2>
				</div>
			</div>

			<div class="row">

				<?php for ($i = 1; $i <= 4; $i++) { ?>

					<div class="col-xs-6 col-sm-6 col-md-3 p-0 producto wow fadeInUp">

						<a class="link animated" href="#">

							<div class="contenedor">

								<?= $this->Html->image('producto/producto-'.$i.'.jpg', array('class' => 'base animated')); ?>

								<div class="capa animated">

									<div class="contenido animated">

										<h3 class="sin-margin-bottom">NOMBRE PRODUCTO</h3>

										<span>VER PRODUCTO</span>

									</div>

								</div>

							</div>

						</a>

					</div>

				<?php } ?>

			</div>

		</div>
	</div>

</section>

<?= $this->element('seccion_marcas'); ?>
