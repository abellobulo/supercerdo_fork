

<div class="page-title">
	<h2><span class="fa fa-list"></span> Recetas</h2>
</div>

<div class="page-content-wrap editar_receta">
    <div class="row">
        <div class="col-md-12">
        	<?= $this->Form->create('Receta', array('class' => 'form-horizontal', 'type' => 'file', 'inputDefaults' => array('label' => false, 'div' => false, 'class' => 'form-control'))); ?>

        	<?= $this->Form->input('id'); ?>

        		<div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><span class="fa fa-mail-forward"></span><strong> ActualIZAR</strong> Receta </h3>
                                </div>
                                <div class="panel-body">
                                    <p>Para la imagen se recomienda utilizar extensiones .jpg / .png y no mayor a 500 KB.</p>
                                </div>
                                <div class="panel-body form-group-separated">                                                                        
                                    
                                    <div class="row">
                                        
                                        <div class="col-md-6">
                                            
                                           <div class="form-group">
                                                <label class="col-md-3 col-xs-12 control-label">Nombre</label>
                                                <div class="col-md-8 col-xs-12">                                
                                                	<?= $this->Form->input('nombre', array('class' => 'form-control')); ?>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 col-xs-12 control-label">Bajada</label>
                                                <div class="col-md-8 col-xs-12">                                
                                                    <?= $this->Form->input('bajada', array('class' => 'form-control')); ?>
                                                </div>
                                            </div>

                                             <div class="form-group">
                                                <label class="col-md-3 col-xs-12 control-label">Preparación</label>
                                                <div class="col-md-8 col-xs-12">                                
                                                	<?= $this->Form->input('preparacion', array('class' => 'form-control summernote')); ?>
                                                </div>
                                            </div>

                                             <div class="form-group">
                                                <label class="col-md-3 col-xs-12 control-label">Tiempo de Preparación</label>
                                                <div class="col-md-8 col-xs-12">                                
                                                	<?= $this->Form->input('tiempo_preparacion', array('class' => 'form-control')); ?>
                                                	<span class="help-block">El tiempo de preparación es medido en minutos</span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 col-xs-12 control-label">Imagen (Actual)</label>
                                                <div class="col-md-8 col-xs-12">                                
                                                	<?= $this->Html->image($this->request->data['Receta']['imagen']['admin']); ?>
                                                	 <span class="help-block">Esta imagen se utilizaré en el catalogo de recetas</span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 col-xs-12 control-label">Imagen (500 x 450)</label>
                                                <div class="col-md-8 col-xs-12">                                
                                                	<?= $this->Form->input('imagen', array('type' => 'file', 'class'=>'file', 'data-preview-file-type' => 'any', 'multiple')); ?>
                                                </div>
                                                
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 col-xs-12 control-label">Imagen Banner (Actual)</label>
                                                <div class="col-md-8 col-xs-12">                                
                                                	<?= $this->Html->image($this->request->data['Receta']['imagen_banner']['admin']); ?>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 col-xs-12 control-label">Imagen Banner <br>(1350 x 400)</label>
                                                <div class="col-md-8 col-xs-12">                                
                                                    <?= $this->Form->input('imagen_banner', array('type' => 'file', 'class'=>'file', 'data-preview-file-type' => 'any', 'multiple')); ?>
                                                     <span class="help-block">Esta imagen se utilizará como banner principal de la receta</span>
                                                </div>
                                                
                                            </div>
                                            
                                        </div>
                                        <div class="col-md-6">


                                            <div class="form-group">
                                                <label class="col-md-3 col-xs-12 control-label">Porciones</label>
                                                <div class="col-md-8 col-xs-12">                                
                                                	<?= $this->Form->input('porciones', array('class' => 'form-control')); ?>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 col-xs-12 control-label">Calorias por porcion</label>
                                                <div class="col-md-8 col-xs-12">                                
                                                	<?= $this->Form->input('calorias_porcion', array('class' => 'form-control')); ?>
                                                </div>
                                            </div>
                                            
                                            
                                            <div class="form-group">                                        
                                               <label class="col-md-3 col-xs-12 control-label ingredientes">Ingredientes <br>
                                                    <?= $this->Html->image('template/add.png', array('class' => 'btn_ingredientes', 'event' => 'add')); ?>
                                                    <?= $this->Html->image('template/delete.png', array('class' => 'btn_ingredientes', 'event' => 'delete')); ?>
                                                </label>

                                                <div class="col-md-8 col-xs-12 tabla-nutricional">    

                                                <?
                                                $cantIngrdientes = 0; 
                                                foreach ($this->request->data['IngredienteReceta'] as $key => $ingrediente) {?>
                                                	<div class="row" id="ingredientes_<?= $cantIngrdientes ?>">
                                                        <div class="col-md-6">
                                                           <?= $this->Form->input('IngredienteReceta.'.$key.'.ingrediente', array('class' => 'form-control')); ?>
                                                        </div>
                                                    </div>
                                                
                                               <? $cantIngrdientes ++;  } ?>
                                                 </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 col-xs-12 control-label">Nivel de Dificultad</label>
                                                <div class="col-md-8 col-xs-12">                                
                                                    <?= $this->Form->input('nivel_dificultad', array('class' => 'form-control select',  'options' => array('Bajo', 'Medio', 'Alto'))); ?>
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <label class="col-md-3 col-xs-12 control-label">Recomentar Recetas</label>
                                                <div class="col-md-8 col-xs-12">                                
                                                    <?= $this->Form->input('recomendar_receta', array('class' => 'form-control select', 'name' => 'RecomendarReceta[]', 'multiple', 'options' => $recetas, 'selected' => $recomendar_recetas)); ?>
                                                </div>
                                            </div>

                                            <h3 style="text-align: center;">Configuración SEO</h3>

                                             <?= $this->Form->input('Seo.id', array('type' => 'hidden', 'value' => $seo_producto['Seo']['id'])); ?>

                                            <div class="form-group">
                                                <label class="col-md-3 col-xs-12 control-label">Título</label>
                                                <div class="col-md-8 col-xs-12">                                
                                                    <?= $this->Form->input('Seo.titulo_seo', array('class' => 'form-control', 'value' => $seo_producto['Seo']['titulo_seo'])); ?>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 col-xs-12 control-label">Descripción</label>
                                                <div class="col-md-8 col-xs-12">                                
                                                    <?= $this->Form->input('Seo.descripcion', array('class' => 'form-control', 'type' => 'textarea', 'value' => $seo_producto['Seo']['descripcion'])); ?>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 col-xs-12 control-label">Palabras Claves</label>
                                                <div class="col-md-8 col-xs-12">                                
                                                    <?= $this->Form->input('Seo.palabras', array('class' => 'form-control', 'value' => $seo_producto['Seo']['palabras'])); ?>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 col-xs-12 control-label">Slug</label>
                                                <div class="col-md-8 col-xs-12">                                
                                                    <?= $this->Form->input('Seo.slug', array('class' => 'form-control', 'value' => $seo_producto['Seo']['slug'])); ?>
                                                </div>
                                            </div>
                                            
                                        </div>
                                        
                                    </div>

                                </div>
                                <div class="panel-footer">
                                    <div class="pull-right">
										<input type="submit" class="btn btn-primary esperar-carga" autocomplete="off" data-loading-text="Espera un momento..." value="Guardar cambios">
										<?= $this->Html->link('Cancelar', array('action' => 'index'), array('class' => 'btn btn-danger')); ?>
									</div>
                                </div>
                            </div>

        	<?= $this->Form->end(); ?>
        </div>
    </div>
</div>

<!-- CANTIDAD DE INGREDIENTES -->
<?= $this->Html->scriptBlock(sprintf("var iteral_cantIngrdientes = '%s';", $cantIngrdientes)); ?>
