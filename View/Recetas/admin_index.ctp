<!-- 
	* Admin Index Recetas
	* Ene 2018
	* Controller: recetas
	* Action: admin_index
	* Admin: true
-->  

<?= $this->element('admin_breadcrumb'); ?>

<div class="page-title">
	<h2><span class="fa fa-list"></span> Recetas</h2>
</div>


<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap admin-seccion-producto">
	 <div class="row">
         <div class="col-md-12">

         	 <!-- START CONTENT FRAME -->
                <div class="content-frame">   
                    
                    <!-- START CONTENT FRAME TOP -->
                    <div class="content-frame-top">                        
                        <div class="page-title">                    
                            <h2><span class="fa fa-image"></span> Lista de Recetas</h2>
                        </div>                                      
                    </div>

                     <!-- START CONTENT FRAME RIGHT -->
                    <div class="content-frame-right">                        
                        <div class="block push-up-10">
                            <div action="upload.php" class="dropzone dropzone-mini">
                            	<?= $this->Html->link(
                            		'<div class="dz-default dz-message"><h4>Subir Nueva Receta</h4></div>',
                            		array('controller' => 'recetas', 'action' => 'add'),
                            		array(
                            			'escape'	=> false, 
                            			'class'		=>	'nuevo_banner'
                            		)
                            	); ?>
                            </div>
                        </div>                	       
                          
                        <div class="block push-up-10">
                            <div class="dropzone dropzone-mini" style="text-align: center;">

                                <? if ( ! empty($destacado['Receta']['imagen']['admin']) ) { ?>
                                    <div class="form-group">
                                        <h4>Receta Destacada</h4>
                                        <span class="help-block"><code><?= $destacado['Receta']['nombre']; ?></code></span>
                                        <div class="col-md-12 col-xs-12 producto-destacado"> 
                                            <?= $this->Html->image($destacado['Receta']['imagen']['admin'] ) ?>
                                        </div>
                                    </div>
                                <? } ?>

                            	<?= $this->Form->create('Receta', array('class' => 'form-horizontal', 'type' => 'file', 'inputDefaults' => array('label' => false, 'div' => false, 'class' => 'form-control'))); ?>

                            		<div class="form-group">
                                        <h4>Seleccionar Receta para destacar</h4>
                                        <div class="col-md-12 col-xs-12"> 
                                        	<?= $this->Form->input('detacada', array('type' => 'select', 'options' => $lista_recetas, 'empty' => '(Selecciones un receta)', 'class' => 'form-control')); ?>
                                        </div>
                                    </div>

                                    <div class="form-group">
											<input type="submit" class="btn btn-danger esperar-carga" autocomplete="off" data-loading-text="Espera un momento..." value="Guardar cambios">
	                                </div>

                            	<?= $this->Form->end(); ?>
                            </div>
                        </div>     

                        <?= $this->element('admin_data_seo'); ?>                                      
                        
                    </div>
                    <!-- END CONTENT FRAME RIGHT -->
                
                    <!-- START CONTENT FRAME BODY -->
                    <div class="content-frame-body content-frame-body-left">

                    	<div class="gallery" id="links">

                        	<? foreach ($recetas as $key => $receta) { 
                                 $hr = ( $receta['Receta']['activo'] ? 'activo' : 'no-activo');
                                ?>

                                <div class="col-md-3 col-xs-12">
                                    <div class="row">
                                        <a class="gallery-item" href="<?= sprintf('%simg/%s',$this->webroot,$receta['Receta']['imagen']['path']) ?>" title="<?= $receta['Receta']['nombre'] ?>" data-gallery>
                                            <div class="image">
                                                <? if ( ! empty($receta['Receta']['imagen']) ) { ?> 
                                                    <?= $this->Html->image($receta['Receta']['imagen']['admin']); ?>
                                                <? } else { ?>
                                                    <?= $this->Html->image('template/SUPER_CERDO_250.jpg'); ?>
                                                <? } ?>
                                            </div>
                                        </a>

                                    </div>
                                    <div class="row">
                                        <div class="">
                                            <div class="meta">
                                                <div class="col-md-12 col-xs-12">
                                                    <div class="titulo_catalogo">
                                                        <strong><?= $receta['Receta']['nombre'] ?></strong>
                                                    </div>
                                                    <span><?= formatoFecha($receta['Receta']['created'],'dmy'); ?></span>
                                                    <hr class="<?= $hr; ?>">
                                                </div>
                                                <div class="col-md-12 col-xs-12" style="text-align: center;">

                                                    <!-- EDITAR -->
                                                    <?= $this->Html->link('<i class="fa fa-pencil"></i>', array('controller' => 'Recetas', 'action' => 'edit', $receta['Receta']['id']), array('class' => 'btn btn-default btn-xs', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title' => 'Editar', 'escape' => false)); ?>

                                                     <? if ( $receta['Receta']['activo'] ) : ?>

                                                        <!-- DESACTIVAR -->
                                                         <?= $this->Html->link('<i class="fa fa-ban"></i>', array('controller' => 'Recetas','action' => 'desactivar', $receta['Receta']['id'], $receta['Receta']['id']), array('class' => 'btn btn-danger btn-xs', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title' => 'Desactivar', 'escape' => false)); ?>

                                                     <? else : ?>

                                                        <!-- ACTIVAR -->
                                                         <?= $this->Html->link('<i class="fa fa-check-square-o"></i>', array('controller' => 'Recetas', 'action' => 'activar', $receta['Receta']['id'], $receta['Receta']['id']), array('class' => 'btn btn-success btn-xs', 'data-toggle'=>'tooltip', 'data-placement'=>'top', 'title' => 'Activar', 'escape' => false)); ?>

                                                     <? endif; ?>

                                                     <?//= $this->Form->postLink('<i class="fa fa-remove"></i>', array('action' => 'delete', $receta['Receta']['id']), array('class' => 'btn btn-xs btn-warning confirmar-eliminacion', 'data-toggle'=>'tooltip','data-toggle'=>'tooltip', 'data-placement' => 'top', 'title' => 'Eliminar', 'escape' => false)); ?>

                                                </div>
                                            </div> 
                                        </div>
                                    </div>
                                </div>

                        	<? } ?>

                        </div>

                        <div class="pull-right">
							<ul class="pagination">
								<?= $this->Paginator->prev('« Anterior', array('tag' => 'li'), null, array('tag' => 'li', 'disabledTag' => 'a', 'class' => 'first disabled hidden')); ?>
								<?= $this->Paginator->numbers(array('tag' => 'li', 'currentTag' => 'a', 'modulus' => 2, 'currentClass' => 'active', 'separator' => '')); ?>
								<?= $this->Paginator->next('Siguiente »', array('tag' => 'li'), null, array('tag' => 'li', 'disabledTag' => 'a', 'class' => 'last disabled hidden')); ?>
							</ul>
						</div>

                        
                    </div>
                </div>

                <!-- BLUEIMP GALLERY -->
		         <?= $this->element('admin_blueimp_gallery'); ?>
		        <!-- END BLUEIMP GALLERY -->

          </div>
     </div>
</div>     

