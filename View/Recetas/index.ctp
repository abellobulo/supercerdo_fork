<!--
	RECETAS - index.ctp
	Controller: recetas
	Action: index
	Admin: false
-->
<div id="bootstrap-touch-slider" class="carousel bs-slider slide control-round indicators-line banner-interior-receta" data-ride="carousel" data-pause="hover" data-interval="8000">

	<div class="carousel-inner" role="listbox">

		<div class="item active" style="background: url(img/Receta/1/supercerdorecetas2.jpg)">

			<div class="bs-slider-overlay"></div>



			<div class="banner-contenido-bottom">

				<div class="container-fluid">

					<div class="row">

						<div class="col-sm-12 col-md-12 col-lg-12">

							<h1 data-animation="animated fadeInDown">Recetas</h1>

						</div>

					</div>

				</div>

			</div>

		</div>

	</div>

</div>
<section class="interior-lineas-productos">

	<div class="capa-fondo">

		<div class="container-fluid">

			<div class="row">


					<div class="col-sm-4 col-xs-4 col-md-4 p-0 linea wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
						<a class="link animated" href="/recetas">
							<img src="/img/LineaProducto/1/banner_lineas_cocina_n.jpg" class="base animated" alt="LÍNEA COCINA">							<div class="capa animated">
								<div class="contenido">
									<img src="/img/lineas-de-productos/lineas-icon-cocina.png" alt="">									<h3>LÍNEA COCINA</h3>
								</div>
							</div>
						</a>
					</div>


					<div class="col-sm-4 col-xs-4 col-md-4 p-0 linea wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
						<a class="link animated" href="/recetas">
							<img src="/img/LineaProducto/2/banner_lineas_sandwich_n.jpg" class="base animated" alt="LÍNEA SANDWICH">							<div class="capa animated">
								<div class="contenido">
									<img src="/img/lineas-de-productos/lineas-icon-sandwich.png" alt="">									<h3>LÍNEA SANDWICH</h3>
								</div>
							</div>
						</a>
					</div>


					<div class="col-sm-4 col-xs-4 col-md-4 p-0 linea wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
						<a class="link animated" href="/recetas">
							<img src="/img/LineaProducto/3/banner_lineas_parrilla_n.jpg" class="base animated" alt="LÍNEA PARRILLA">							<div class="capa animated">
								<div class="contenido">
									<img src="/img/lineas-de-productos/lineas-icon-parrilla.png" alt="">									<h3>LÍNEA PARRILLA</h3>
								</div>
							</div>
						</a>
					</div>


			</div>

		</div>
	</div>

</section>
<section class="interior-recetas seccion-recetas">

	<div class="capa-fondo">

		<div class="container-fluid">

			<div class="row">

        <? foreach ($recetas as $key => $receta) { ?>

          <div class="col-xs-6 col-sm-6 col-md-3 receta wow fadeInUp">

            <a class="link animated" href="<?= sprintf('%srecetas/view/%s',$this->webroot,$receta['Receta']['id']) ?>">

              <div class="contenedor">

                <?= $this->Html->image($receta['Receta']['imagen']['catalogo'], array('class' => 'base animated', 'alt' => existe($receta['Receta']['imagen']['nombre']))); ?>

                <div class="capa animated">

                  <div class="contenido animated">

                    <h3 class="sin-margin-bottom"><?= Ucfirst(existe($receta['Receta']['nombre'])) ?></h3>

                    <div class="detalles">

                      <div class="detalle">
                        <img src="img/recetas/icon-receta-tiempo.png" class="icon-dificultad">
                        <p class="valor"><?= existe($receta['Receta']['tiempo_preparacion']) ?> Mint</p>
                      </div>

                      <div class="detalle">
                        <img src="img/recetas/icon-receta-porciones.png" class="icon-dificultad">
                        <p class="valor">Porciones <br><?= existe($receta['Receta']['porciones']) ?></p>
                      </div>

                      <div class="detalle">
                        <img src="img/recetas/icon-receta-dif-media.png" class="icon-dificultad">
                        <p class="valor">
                          Dificultad
                        </p>
                      </div>

                    </div>

                    <span>VER RECETA</span>

                  </div>

                </div>

              </div>

            </a>

          </div>

        <?php } ?>

			</div>


			<!-- <div class="paginacion-noticias">
				<div class="container">
					<div class="row">
						<div class="col-sm-12 wow fadeInUp text-center">

							<ul class="pagination">
								<?= $this->Paginator->numbers(array('tag' => 'li', 'currentTag' => 'a', 'modulus' => 2, 'currentClass' => 'active', 'separator' => '')); ?>
							</ul>

						</div>
					</div>
				</div>
			</div> -->

		</div>
	</div>

</section>



<!-- <?= $this->element('tips'); ?> -->

<?= $this->element('seccion_marcas'); ?>
