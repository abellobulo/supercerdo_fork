<!--
	RECETAS - view.ctp
	Controller: recetas
	Action: view
	Admin: false
-->

<?
	$titulo_linea = array(
		'linea_parrilla' => 'LÍNEA PARRILLA',
		'linea_sandwich' => 'LÍNEA SANDWICH',
		'linea_cocina' => 'LÍNEA COCINA'
	);

	$fondo_linea = array(
		'linea_parrilla' => 'fondo-linea.jpg',
		'linea_sandwich' => 'lineaSandwich.jpg',
		'linea_cocina' => 'LineaCocina.jpg'
	);


 ?>
 <div id="bootstrap-touch-slider" class="carousel bs-slider slide control-round indicators-line banner-interior-receta banner-view" data-ride="carousel" data-pause="hover" data-interval="8000">

 	<div class="carousel-inner" role="listbox">

 		<div class="item active" style="background: url(../../img/Banner/6/nuestros_productos_r.jpg)">

 			<div class="bs-slider-overlay"></div>



 			<div class="banner-contenido-bottom">

 				<div class="container-fluid">

 					<div class="row">

 						<div class="col-sm-12 col-md-12 col-lg-12">



 						</div>

 					</div>

 				</div>

 			</div>

 		</div>

 	</div>

 </div>
 <section class="interior-lineas-productos">

 	<div class="capa-fondo">

 		<div class="container-fluid">

 			<div class="row">


 					<div class="col-sm-4 col-xs-4 col-md-4 p-0 linea wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
 						<a class="link animated" href="/recetas">
 							<img src="/img/LineaProducto/1/banner_lineas_cocina_n.jpg" class="base animated" alt="LÍNEA COCINA">							<div class="capa animated">
 								<div class="contenido">
 									<img src="/img/lineas-de-productos/lineas-icon-cocina.png" alt="">									<h3>LÍNEA COCINA</h3>
 								</div>
 							</div>
 						</a>
 					</div>


 					<div class="col-sm-4 col-xs-4 col-md-4 p-0 linea wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
 						<a class="link animated" href="/recetas">
 							<img src="/img/LineaProducto/2/banner_lineas_sandwich_n.jpg" class="base animated" alt="LÍNEA SANDWICH">							<div class="capa animated">
 								<div class="contenido">
 									<img src="/img/lineas-de-productos/lineas-icon-sandwich.png" alt="">									<h3>LÍNEA SANDWICH</h3>
 								</div>
 							</div>
 						</a>
 					</div>


 					<div class="col-sm-4 col-xs-4 col-md-4 p-0 linea wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
 						<a class="link animated" href="/recetas">
 							<img src="/img/LineaProducto/3/banner_lineas_parrilla_n.jpg" class="base animated" alt="LÍNEA PARRILLA">							<div class="capa animated">
 								<div class="contenido">
 									<img src="/img/lineas-de-productos/lineas-icon-parrilla.png" alt="">									<h3>LÍNEA PARRILLA</h3>
 								</div>
 							</div>
 						</a>
 					</div>


 			</div>

 		</div>
 	</div>

 </section>
 <section class="interior-lineas-productos">

 	<div class="capa-fondo">

 		<div class="container-fluid">

 			<div class="row">

 				<? foreach ($lineas_productos as $key => $linea) {
 					$img_linea = array(
 						'1' =>	'lineas-icon-cocina.png',
 						'2'	=>	'lineas-icon-sandwich.png',
 						'3'	=>	'lineas-icon-parrilla.png'
 	 				);

 	 				$link_linea = array(
 						'1' =>	'linea_cocina',
 						'2'	=>	'linea_sandwich',
 						'3'	=>	'linea_parrilla'
 	 				);
 				?>

 					<div class="col-sm-12 col-md-4 p-0 linea wow fadeInUp">
 						<a class="link animated" href="<?= $this->webroot; ?>productos/linea_producto/<?= $link_linea[$linea['LineaProducto']['id']] ?>">
 							<?= $this->Html->image($linea['LineaProducto']['imagen']['path'], array('class' => 'base animated', 'alt' => $linea['LineaProducto']['nombre'])); ?>
 							<div class="capa animated">
 								<div class="contenido">
 									<?= $this->Html->image(sprintf('lineas-de-productos/%s',$img_linea[$linea['LineaProducto']['id']])); ?>
 									<h3><?=  $linea['LineaProducto']['nombre']; ?></h3>
 								</div>
 							</div>
 						</a>
 					</div>

 				<? } ?>

 			</div>

 		</div>
 	</div>

 </section>

<div class="content-recetas_interior">

  <div class="container-fluid">
    <div class="row">
      <div class="col-md-6 p-0">
        <div id="bootstrap-touch-slider" class="banner-receta" style="background: url(<?= sprintf('%simg/%s',$this->webroot,$receta['Receta']['imagen_banner']['path']) ?>); background-size: cover; background-position: center;">
        </div>
      </div>
      <div class="col-md-6 p-0">
        <div class="content-recetas_interior-nombre">
          <h1 data-animation="animated fadeInDown"><?= Ucfirst(existe($receta['Receta']['nombre'])) ?></h1>

          <div class="row">

    				<div class="col-sm-3 col-xs-6 wow fadeInUp col text-center">
              <div class="text-center">
                <?= $this->Html->image('recetas/icon-receta-tiempo.svg'); ?>
              </div>
    					<h4><?= existe($receta['Receta']['tiempo_preparacion']); ?></h4>
    				</div>

    				<div class="col-sm-3 col-xs-6 wow fadeInUp col text-center">
              <div class="text-center">
                <?= $this->Html->image('recetas/icon-receta-porciones.svg'); ?>
              </div>
    					<h4><?= existe($receta['Receta']['porciones']); ?> <br> PORCIONES </h4>
    				</div>

    				<div class="col-sm-3 col-xs-6 wow fadeInUp col text-center">
              <div class="text-center">
                <?= $this->Html->image('recetas/icon-receta-dif-media.svg'); ?>
              </div>
    					<h4>NIVEL DE DIFICULTAD</h4>
    				</div>

            <div class="col-sm-3 col-xs-6 wow fadeInUp col text-center">
              <div class="text-center">
                <?= $this->Html->image('recetas/icon-receta-kal.svg'); ?>
              </div>
    					<h4><?= existe($receta['Receta']['calorias_porcion']); ?> <br> CALORÍAS </h4>
    				</div>

    			</div>

        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-6 col-sm-6 col-xs-12 p-0">
        <div class="content-recetas_interior-lista">
          <h5>INGREDIENTES</h5>
  				<ul>
  					<li>12 costillitas Ribs Super cerdo</li>
  					<li>150 grs. de maní tostado</li>
  					<li>200 cc de yogur</li>
  					<li>1 cebollín picado</li>
  					<li>1 cdta. de curry</li>
  					<li>1 taza de salsa de soya</li>
  				</ul>
        </div>
      </div>
      <div class="col-md-6 col-sm-6 col-xs-12 p-0">
        <div class="content-recetas_interior-receta">

  				<p> <?= existe($receta['Receta']['preparacion']); ?> </p>

          <div class="row">
      			<div class="col-sm-12 col-md-12 col-lg-12">
      				COMPARTIR
      				<a class="link animated" href="https://www.facebook.com/" target="_blank"><i class="fa fa-facebook"></i></a>
      				<a class="link animated" href="https://www.youtube.com/" target="_blank"><i class="fa fa-youtube"></i></a>
      				<a class="link animated" href="https://www.instagram.com/" target="_blank"><i class="fa fa-instagram"></i></a>
      				<a class="link animated" href="" target="_blank"><i class="fa fa-whatsapp"></i></a>
      			</div>
      		</div>

        </div>
      </div>
    </div>
  </div>


  <?= $this->element('receta_recomendadas'); ?>

  <?= $this->element('seccion_marcas'); ?>
</div>
