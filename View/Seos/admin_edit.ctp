<div class="page-title">
	<h2><span class="fa fa-list"></span> SEO</h2>
</div>

<div class="page-content-wrap">
    <div class="row">
        <div class="col-md-12">
        	<?= $this->Form->create('Seo', array('class' => 'form-horizontal', 'type' => 'file', 'inputDefaults' => array('label' => false, 'div' => false, 'class' => 'form-control'))); ?>

        		<?= $this->Form->input('id'); ?>


        		<div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><span class="fa fa-mail-forward"></span><strong> Actualización</strong> Asignación de SEO </h3>
                    </div>
                    <div class="panel-body">
                        <p>Esta asignación de SEO es realizada para el modulo de <?= $this->request->data['Seo']['modulo']; ?></p>
                    </div>
                    <div class="panel-body form-group-separated">   

                    	 <div class="row">
                            
                            <div class="col-md-12">

                               <div class="form-group">
                                    <label class="col-md-3 col-xs-12 control-label">Título</label>
                                    <div class="col-md-6 col-xs-12">                                
                                    	<?= $this->Form->input('titulo_seo', array('class' => 'form-control')); ?>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="col-md-3 col-xs-12 control-label">Descripción</label>
                                    <div class="col-md-6 col-xs-12">                                
                                        <?= $this->Form->input('descripcion', array('class' => 'form-control')); ?>
                                    </div>
                                </div>

                                 <div class="form-group">
                                    <label class="col-md-3 col-xs-12 control-label">Palabras Claves</label>
                                    <div class="col-md-6 col-xs-12">                                
                                    	<?= $this->Form->input('palabras', array('class' => 'form-control')); ?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-3 col-xs-12 control-label">Slug</label>
                                    <div class="col-md-6 col-xs-12">                                
                                    	<?= $this->Form->input('slug', array('class' => 'form-control')); ?>
                                    </div>
                                </div>
                            </div>
                            
                        </div>

                    </div>
                    <div class="panel-footer">
                        <div class="pull-right">
							<input type="submit" class="btn btn-primary esperar-carga" autocomplete="off" data-loading-text="Espera un momento..." value="Guardar cambios">
							<?= $this->Html->link('Cancelar', array('controller' => $redirect_controller, 'action' => $redirect_action), array('class' => 'btn btn-danger')); ?>
						</div>
                    </div>
                </div>

        	<?= $this->Form->end(); ?>
        </div>
    </div>
</div>


