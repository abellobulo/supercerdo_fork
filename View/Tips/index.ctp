<!-- 
	TIPS - index.ctp
	Controller: tips
	Action: index
	Admin: false
-->

<div id="bootstrap-touch-slider" class="carousel bs-slider slide control-round indicators-line banner-tips" data-ride="carousel" data-pause="hover" data-interval="8000">

	<div class="carousel-inner" role="listbox">

		<div class="item active" style="background: url(http://via.placeholder.com/1920x565);">

			<div class="bs-slider-overlay"></div>


			<div class="banner-contenido-bottom">

				<div class="container">

					<div class="row">

						<div class="col-sm-12 col-md-7 col-lg-6">

							<h1 data-animation="animated fadeInDown">Tips</h1>

							<p>Curabitur lobortis id lorem id bibendum. Ut id consectetur magna. Quisque volutpat augue enim, pulvinar lobortis nibh lacinia at. Vestibulum nec erat ut mi sollicitudin porttitor id sit amet risus. Nam tempus vel odio vitae aliquam. In imperdiet eros id lacus vestibulum vestibulum. Suspendisse fermentum sem sagittis ante venenatis egestas quis vel justo. Maecenas semper suscipit nunc, sed aliquam sapien convallis eu. Nulla ut turpis in diam dapibus consequat.</p>

						</div>

					</div>

				</div>

			</div>

		</div>

	</div>

</div>

<section class="breadcrums wow fadeInUp">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<a class="link animated" href="">Home</a> / <a class="link animated" href="recetas/">Recetas</a> / Tips
			</div>
		</div>
	</div>
</section>

<section class="interior-receta-compartir wow fadeInUp">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 col-md-7 col-lg-6">
				COMPARTIR
				<a class="link animated" href="https://www.facebook.com/" target="_blank"><i class="fa fa-facebook"></i></a>
				<a class="link animated" href="https://www.youtube.com/" target="_blank"><i class="fa fa-youtube"></i></a>
				<a class="link animated" href="https://www.instagram.com/" target="_blank"><i class="fa fa-instagram"></i></a>
				<a class="link animated" href="https://www.twitter.com/" target="_blank"><i class="fa fa-twitter"></i></a>
				<a class="link animated" href="" target="_blank"><i class="fa fa-whatsapp"></i></a>
			</div>
		</div>
	</div>
</section>

<section class="interior-tips" style="background: url(/steven/supercerdo/img/template/olla.jpg);
    background-position: top center !important;
    background-size: cover !important;
    background-attachment: fixed !important;">

	<div class="capa-fondo">

		<div class="container">

			<div class="row">

				<?php for ($i = 0; $i < 4; $i++) { ?>

					<div class="col-sm-6 wow fadeInUp tip">
						<div class="contenido">
							<h3>NOMBRE DEL TIP</h3>
							<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.</p>
							<p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Lorem ipsum dolor sit amet, cons ectetuer adipiscing.</p>
							<a class="link animated"><i class="fa fa-print"></i> Imprimir</a>
						</div>
					</div>
					
				<?php } ?>

			</div>

		</div>
		
	</div>

</section>

<?= $this->element('seccion_marcas'); ?>

