/* jshint bitwise:true, browser:true, eqeqeq:true, forin:true, globalstrict:true, indent:4, jquery:true,
   loopfunc:true, maxerr:3, noarg:true, node:true, noempty:true, onevar: true, quotmark:single,
   strict:true, undef:true, white:false */
/* global FB, webroot, fullwebroot */

/*!
 * Books & Bits | Backend
 */

//<![CDATA[
'use strict';

/**
 * jQuery
 */



jQuery(document).ready(function($)
{
	// VARIABLES GLOBALES
	var iteral_nutricional 			= 0;
	var iteral_nutricional_extra 	= 1;
	var iteral_ingrediente			= 0;

	if ( $('.editar_producto').length ){
		 iteral_nutricional = parseInt(iteral_nutricional_edit);
		 iteral_nutricional_extra = parseInt(iteral_nutricional_extra_edit);

		 var eliminar_nutriciona 		= [];
		 var eliminar_nutriciona_extra	= [];
	} 


	if ( $('.editar_receta').length ){
		 iteral_ingrediente = parseInt(iteral_cantIngrdientes);
	} 

	

	// REPETIDOR INGREDIENTES RECETA
	if ( $('.ingredientes').length ){

		$('.btn_ingredientes').on('click', function(){

			var event  = $(this).attr('event');

			if ( event == 'add' ){
				var iteral_ingrediente_antes = iteral_ingrediente;
				iteral_ingrediente ++;
				var html_add = '<div class="row" id="ingredientes_'+ iteral_ingrediente +'">'
	                                + '<div class="col-md-6">'
	                                   + '<input name="data[IngredienteReceta]['+ iteral_ingrediente +'][ingrediente]" class="form-control" required="required" type="text" id="IngredienteReceta'+ iteral_ingrediente +'ingrediente">'
	                                   + '</div> '
	                            + '</div>';

	           $('#ingredientes_'+iteral_ingrediente_antes).after(html_add);
			}
			else{
				if ( iteral_ingrediente > 0 ){
					$('#ingredientes_'+ iteral_ingrediente).remove();
					iteral_ingrediente --;
				}
			}
		});

	}


	// REPETIDOR TABLA NUTRICIONAL
	if ( $('.tabla-nutricional_titulo').length ){

		$('.icon_producto').on('click', function(){

			var action  = $(this).attr('action');
			var event  = $(this).attr('event');

			// REPETIDOR NUTRICIONAL
			if ( action == 'nutricional' ){

				if ( event == 'add' ){
					var iteral_nutricional_antes = iteral_nutricional;
					iteral_nutricional +=2;
					var html_add = '<div class="row" id="nutrientes_'+ iteral_nutricional +'">' 
								    + '<div class="col-md-3">'
								   		+ '<input name="data[NutricionalProducto]['+ iteral_nutricional +'][nombre]" class="form-control" type="text"id="NutricionalProducto'+ iteral_nutricional +'Nombre" required>'
								   	+ '</div>'
								    + '<div class="col-md-2">'
								        + '<input name="data[NutricionalProducto]['+ iteral_nutricional +'][medida]" class="form-control" type="text" id="NutricionalProducto'+ iteral_nutricional +'Medida" required>'
								    + '</div>'
								    + '<div class="col-md-2">'
								        + '<input name="data[NutricionalProducto]['+ iteral_nutricional +'][100g]" class="form-control" type="text" id="NutricionalProducto'+ iteral_nutricional +'100g">'
								    + '</div>'
								    + '<div class="col-md-2">'
								       + '<input name="data[NutricionalProducto]['+ iteral_nutricional +'][porcion]" class="form-control" type="text" id="NutricionalProducto'+ iteral_nutricional +'Porcion">'
								    + '</div>'
								    + '<div class="col-md-2">'
								        + '<input name="data[NutricionalProducto]['+ iteral_nutricional +'][cda]" class="form-control" type="text" id="NutricionalProducto'+ iteral_nutricional +'Cda">'
								    + '</div>'
								+ '</div>';
					$('#nutrientes_'+iteral_nutricional_antes).after(html_add);

				}else if( event == 'delete' &&  iteral_nutricional > 0) {
					if ( $('.editar_producto').length ){
						 var id_registro =  $('#nutrientes_'+ iteral_nutricional).attr('registro');
						 eliminar_nutriciona.push(id_registro);
						 $('#nutricional_eliminados').val(eliminar_nutriciona);
					}

					$('#nutrientes_'+ iteral_nutricional).remove();
					iteral_nutricional -=2;
				}

			}
			// REPETIDOR NUTRICIONAL EXTRA
			else if ( action == 'nutricional_extra' ){

				if ( event == 'add' ){

					var iteral_nutricional_extra_antes = iteral_nutricional_extra;
					iteral_nutricional_extra +=2;
					var html_add_extra = '<div class="row" id="nutrientes_extra_'+ iteral_nutricional_extra +'">'
		                                + '<div class="col-md-6">'
		                                   + '<input name="data[NutricionalProducto]['+ iteral_nutricional_extra +'][nombre]" class="form-control" type="text" id="NutricionalProducto'+ iteral_nutricional_extra +'Nombre" required>'
		                                    + ' <input type="hidden" name="data[NutricionalProducto]['+ iteral_nutricional_extra +'][extra]" class="form-control" value="1" id="NutricionalProducto'+ iteral_nutricional_extra +'Extra">'
		                                + '</div>'
		                               + '<div class="col-md-6">'
		                                  + '<input name="data[NutricionalProducto]['+ iteral_nutricional_extra +'][medida]" class="form-control" type="text" id="NutricionalProducto1Medida">'
		                                + '</div>'
		                           + '</div>';


					$('#nutrientes_extra_'+iteral_nutricional_extra_antes).after(html_add_extra);

				}else if( event == 'delete' &&  iteral_nutricional_extra > 1) {
					$('#nutrientes_extra_'+ iteral_nutricional_extra).remove();
					iteral_nutricional_extra -=2;
				}
				
			}

		});

	}


});
//]]>
