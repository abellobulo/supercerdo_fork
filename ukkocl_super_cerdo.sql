-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 19-06-2018 a las 11:06:45
-- Versión del servidor: 5.6.39-cll-lve
-- Versión de PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `ukkocl_super_cerdo`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sc_administradores`
--

CREATE TABLE `sc_administradores` (
  `id` bigint(20) NOT NULL,
  `nombre` varchar(200) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `clave` varchar(50) DEFAULT NULL,
  `activo` tinyint(1) DEFAULT '0',
  `ultima_seccion` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `rol_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sc_administradores`
--

INSERT INTO `sc_administradores` (`id`, `nombre`, `email`, `clave`, `activo`, `ultima_seccion`, `created`, `modified`, `rol_id`) VALUES
(3, 'Desarrollo UKKO', 'ssalcedo@ukko.cl', '6e5973e1159643e727b79f5cae9d3bdd83b9152e', 0, NULL, '2018-01-25 22:08:06', '2018-01-25 22:08:06', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sc_banners`
--

CREATE TABLE `sc_banners` (
  `id` bigint(20) NOT NULL,
  `activo` tinyint(1) DEFAULT '1',
  `eliminado` tinyint(1) NOT NULL DEFAULT '0',
  `modulo` varchar(200) DEFAULT NULL,
  `tipo` varchar(200) DEFAULT NULL,
  `imagen` varchar(200) DEFAULT NULL,
  `bajada` longtext,
  `link` varchar(200) DEFAULT NULL,
  `administrador_id` bigint(20) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sc_banners`
--

INSERT INTO `sc_banners` (`id`, `activo`, `eliminado`, `modulo`, `tipo`, `imagen`, `bajada`, `link`, `administrador_id`, `created`, `modified`) VALUES
(1, 1, 0, 'Home', 'fondo', 'fondos_secciones_plato_arroz.jpg', '<p><br></p>', '', 3, '2018-02-26 00:00:00', '2018-03-02 12:10:55'),
(2, 1, 0, 'Nuestros Productos', 'fondo', 'fondo_supercerdo_4.jpeg', NULL, NULL, 3, NULL, '2018-03-02 14:13:41'),
(3, 1, 0, 'Línea Cocina', 'fondo', NULL, NULL, NULL, 3, NULL, NULL),
(4, 1, 0, 'línea Sandwich', 'fondo', NULL, NULL, NULL, 3, NULL, NULL),
(5, 1, 0, 'Línea Parrilla', 'fondo', NULL, NULL, NULL, 3, NULL, NULL),
(6, 1, 0, 'Producto', NULL, 'nuestros_productos_r.jpg', 'Nuestros Productos', NULL, 3, '2018-02-27 01:38:54', '2018-02-28 18:14:32'),
(7, 1, 0, 'home', NULL, 'abastero_1920x690_final_2.gif', '<p>Saboreando la Vida</p>', '', 3, '2018-02-28 12:43:52', '2018-02-28 12:43:52');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sc_campania`
--

CREATE TABLE `sc_campania` (
  `id` bigint(20) NOT NULL,
  `nombre` varchar(200) DEFAULT NULL,
  `bajada` longtext,
  `imagen` varchar(200) DEFAULT NULL,
  `link` varchar(200) DEFAULT NULL,
  `activo` tinyint(1) DEFAULT '1',
  `administrador_id` bigint(20) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sc_campania`
--

INSERT INTO `sc_campania` (`id`, `nombre`, `bajada`, `imagen`, `link`, `activo`, `administrador_id`, `created`, `modified`) VALUES
(1, 'El Molido', '¡Participa por premios diarios y salva tu verano en la parrilla!', 'campana_1.jpg', '-aM7F6Au8AE', 1, 3, '2018-02-19 14:33:54', '2018-02-19 14:35:57'),
(2, 'Receta de parrilla Plateada de cerdo campesina.', '', 'campana_2.jpg', 'olgkIZE_FK4', 1, 3, '2018-02-19 14:52:46', '2018-02-19 14:52:46'),
(3, 'Salvavidas de Verano - Barrio', '¡Participa por premios diarios y salva tu verano en la parrilla!', 'campana_3.jpg', '2r1cb51xt50&list=LLCVEMyD4GNBIAvUqOQ7iHcg', 1, 3, '2018-02-19 14:54:04', '2018-02-19 14:54:04');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sc_configuraciones`
--

CREATE TABLE `sc_configuraciones` (
  `id` bigint(20) NOT NULL,
  `adminitrador_id` bigint(20) NOT NULL,
  `modulo` varchar(200) DEFAULT NULL,
  `sub_modulo_id` varchar(45) DEFAULT NULL,
  `nombre` varchar(200) DEFAULT NULL,
  `bajada` longtext,
  `descripcion` longtext,
  `imagen` varchar(200) DEFAULT NULL,
  `tipo` varchar(45) DEFAULT NULL,
  `activo` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sc_configuraciones`
--

INSERT INTO `sc_configuraciones` (`id`, `adminitrador_id`, `modulo`, `sub_modulo_id`, `nombre`, `bajada`, `descripcion`, `imagen`, `tipo`, `activo`) VALUES
(1, 3, 'contacto', NULL, 'Contacto', '', '<h3 style=\"-webkit-tap-highlight-color: rgba(255, 255, 255, 0); font-family: BlissPro-Bold; font-weight: normal; line-height: 16px; color: rgb(255, 255, 255); margin-bottom: 20px; font-size: 16px; position: relative; background-color: rgb(225, 30, 50);\">CALL CENTER</h3><h4 style=\"-webkit-tap-highlight-color: rgba(255, 255, 255, 0); font-family: BlissPro-Bold; line-height: 24px; color: rgb(255, 255, 255); margin-top: 0px; margin-bottom: 20px; font-size: 24px; position: relative; background-color: rgb(225, 30, 50);\">800 20 6000</h4><p style=\"-webkit-tap-highlight-color: rgba(255, 255, 255, 0); margin-bottom: 20px; font-family: BlissPro-Light; color: rgb(255, 255, 255); font-size: 16px; line-height: 22px; padding: 0px 20px; background-color: rgb(225, 30, 50);\">Línea de atención al consumidor&nbsp;<br style=\"-webkit-tap-highlight-color: rgba(255, 255, 255, 0);\">Si necesitas información sobre nuestros productos o quieres consultar sobre la elaboración, envasado, información nutricional y calidad de ellos, comunícate con nosotros al siguiente número:</p><p style=\"-webkit-tap-highlight-color: rgba(255, 255, 255, 0); margin-bottom: 20px; font-family: BlissPro-Light; color: rgb(255, 255, 255); font-size: 16px; line-height: 22px; padding: 0px 20px; background-color: rgb(225, 30, 50);\">Horario de atención&nbsp;<br style=\"-webkit-tap-highlight-color: rgba(255, 255, 255, 0);\">Lunes a Viernes 8:30 a 18:00 hrs.&nbsp;<br style=\"-webkit-tap-highlight-color: rgba(255, 255, 255, 0);\">Sábado 9:00 a 14:00 hrs</p><h4 style=\"-webkit-tap-highlight-color: rgba(255, 255, 255, 0); font-family: BlissPro-Bold; line-height: 24px; color: rgb(255, 255, 255); margin-top: 0px; margin-bottom: 20px; font-size: 24px; position: relative; background-color: rgb(225, 30, 50);\">600 600 7777</h4><p style=\"-webkit-tap-highlight-color: rgba(255, 255, 255, 0); margin-bottom: 20px; font-family: BlissPro-Light; color: rgb(255, 255, 255); font-size: 16px; line-height: 22px; padding: 0px 20px; background-color: rgb(225, 30, 50);\">Línea de atención al cliente&nbsp;<br style=\"-webkit-tap-highlight-color: rgba(255, 255, 255, 0);\">Si eres dueño de un local comercial, negocio, almacén, o quieres comprar nuestros productos, comunícate con nosotros al siguiente número:</p><p style=\"-webkit-tap-highlight-color: rgba(255, 255, 255, 0); margin-bottom: 20px; font-family: BlissPro-Light; color: rgb(255, 255, 255); font-size: 16px; line-height: 22px; padding: 0px 20px; background-color: rgb(225, 30, 50);\">Horario de atención&nbsp;<br style=\"-webkit-tap-highlight-color: rgba(255, 255, 255, 0);\">Lunes a Viernes 8:30 a 18:00 hrs.&nbsp;<br style=\"-webkit-tap-highlight-color: rgba(255, 255, 255, 0);\">Sábado 9:00 a 14:00 hrs</p>', 'contacto.png', 'pageContacto', 1),
(2, 3, 'contacto', NULL, 'Auspicio', '', '<h3 style=\"-webkit-tap-highlight-color: rgba(255, 255, 255, 0); font-family: BlissPro-Bold; font-weight: normal; line-height: 16px; color: rgb(255, 255, 255); margin-bottom: 20px; font-size: 16px; position: relative; background-color: rgb(225, 30, 50);\">CALL CENTER</h3><h4 style=\"-webkit-tap-highlight-color: rgba(255, 255, 255, 0); font-family: BlissPro-Bold; line-height: 24px; color: rgb(255, 255, 255); margin-top: 0px; margin-bottom: 20px; font-size: 24px; position: relative; background-color: rgb(225, 30, 50);\">800 20 6000</h4><p style=\"-webkit-tap-highlight-color: rgba(255, 255, 255, 0); margin-bottom: 20px; font-family: BlissPro-Light; color: rgb(255, 255, 255); font-size: 16px; line-height: 22px; padding: 0px 20px; background-color: rgb(225, 30, 50);\">Línea de atención al consumidor&nbsp;<br style=\"-webkit-tap-highlight-color: rgba(255, 255, 255, 0);\">Si necesitas información sobre nuestros productos o quieres consultar sobre la elaboración, envasado, información nutricional y calidad de ellos, comunícate con nosotros al siguiente número:</p><p style=\"-webkit-tap-highlight-color: rgba(255, 255, 255, 0); margin-bottom: 20px; font-family: BlissPro-Light; color: rgb(255, 255, 255); font-size: 16px; line-height: 22px; padding: 0px 20px; background-color: rgb(225, 30, 50);\">Horario de atención&nbsp;<br style=\"-webkit-tap-highlight-color: rgba(255, 255, 255, 0);\">Lunes a Viernes 8:30 a 18:00 hrs.&nbsp;<br style=\"-webkit-tap-highlight-color: rgba(255, 255, 255, 0);\">Sábado 9:00 a 14:00 hrs</p><h4 style=\"-webkit-tap-highlight-color: rgba(255, 255, 255, 0); font-family: BlissPro-Bold; line-height: 24px; color: rgb(255, 255, 255); margin-top: 0px; margin-bottom: 20px; font-size: 24px; position: relative; background-color: rgb(225, 30, 50);\">600 600 7777</h4><p style=\"-webkit-tap-highlight-color: rgba(255, 255, 255, 0); margin-bottom: 20px; font-family: BlissPro-Light; color: rgb(255, 255, 255); font-size: 16px; line-height: 22px; padding: 0px 20px; background-color: rgb(225, 30, 50);\">Línea de atención al cliente&nbsp;<br style=\"-webkit-tap-highlight-color: rgba(255, 255, 255, 0);\">Si eres dueño de un local comercial, negocio, almacén, o quieres comprar nuestros productos, comunícate con nosotros al siguiente número:</p><p style=\"-webkit-tap-highlight-color: rgba(255, 255, 255, 0); margin-bottom: 20px; font-family: BlissPro-Light; color: rgb(255, 255, 255); font-size: 16px; line-height: 22px; padding: 0px 20px; background-color: rgb(225, 30, 50);\">Horario de atención&nbsp;<br style=\"-webkit-tap-highlight-color: rgba(255, 255, 255, 0);\">Lunes a Viernes 8:30 a 18:00 hrs.&nbsp;<br style=\"-webkit-tap-highlight-color: rgba(255, 255, 255, 0);\">Sábado 9:00 a 14:00 hrs</p>', 'auspicio.png', 'pageContacto', 1),
(3, 3, 'contacto', NULL, 'Reclamo', '', '<h3 style=\"-webkit-tap-highlight-color: rgba(255, 255, 255, 0); font-family: BlissPro-Bold; font-weight: normal; line-height: 16px; color: rgb(255, 255, 255); margin-bottom: 20px; font-size: 16px; position: relative; background-color: rgb(225, 30, 50);\">CALL CENTER</h3><h4 style=\"-webkit-tap-highlight-color: rgba(255, 255, 255, 0); font-family: BlissPro-Bold; line-height: 24px; color: rgb(255, 255, 255); margin-top: 0px; margin-bottom: 20px; font-size: 24px; position: relative; background-color: rgb(225, 30, 50);\">800 20 6000</h4><p style=\"-webkit-tap-highlight-color: rgba(255, 255, 255, 0); margin-bottom: 20px; font-family: BlissPro-Light; color: rgb(255, 255, 255); font-size: 16px; line-height: 22px; padding: 0px 20px; background-color: rgb(225, 30, 50);\">Línea de atención al consumidor&nbsp;<br style=\"-webkit-tap-highlight-color: rgba(255, 255, 255, 0);\">Si necesitas información sobre nuestros productos o quieres consultar sobre la elaboración, envasado, información nutricional y calidad de ellos, comunícate con nosotros al siguiente número:</p><p style=\"-webkit-tap-highlight-color: rgba(255, 255, 255, 0); margin-bottom: 20px; font-family: BlissPro-Light; color: rgb(255, 255, 255); font-size: 16px; line-height: 22px; padding: 0px 20px; background-color: rgb(225, 30, 50);\">Horario de atención&nbsp;<br style=\"-webkit-tap-highlight-color: rgba(255, 255, 255, 0);\">Lunes a Viernes 8:30 a 18:00 hrs.&nbsp;<br style=\"-webkit-tap-highlight-color: rgba(255, 255, 255, 0);\">Sábado 9:00 a 14:00 hrs</p><h4 style=\"-webkit-tap-highlight-color: rgba(255, 255, 255, 0); font-family: BlissPro-Bold; line-height: 24px; color: rgb(255, 255, 255); margin-top: 0px; margin-bottom: 20px; font-size: 24px; position: relative; background-color: rgb(225, 30, 50);\">600 600 7777</h4><p style=\"-webkit-tap-highlight-color: rgba(255, 255, 255, 0); margin-bottom: 20px; font-family: BlissPro-Light; color: rgb(255, 255, 255); font-size: 16px; line-height: 22px; padding: 0px 20px; background-color: rgb(225, 30, 50);\">Línea de atención al cliente&nbsp;<br style=\"-webkit-tap-highlight-color: rgba(255, 255, 255, 0);\">Si eres dueño de un local comercial, negocio, almacén, o quieres comprar nuestros productos, comunícate con nosotros al siguiente número:</p><p style=\"-webkit-tap-highlight-color: rgba(255, 255, 255, 0); margin-bottom: 20px; font-family: BlissPro-Light; color: rgb(255, 255, 255); font-size: 16px; line-height: 22px; padding: 0px 20px; background-color: rgb(225, 30, 50);\">Horario de atención&nbsp;<br style=\"-webkit-tap-highlight-color: rgba(255, 255, 255, 0);\">Lunes a Viernes 8:30 a 18:00 hrs.&nbsp;<br style=\"-webkit-tap-highlight-color: rgba(255, 255, 255, 0);\">Sábado 9:00 a 14:00 hrs</p>', 'reclamo.png', 'pageContacto', 1),
(4, 3, 'contacto', NULL, 'Felicitaciones', '', '<h3 style=\"-webkit-tap-highlight-color: rgba(255, 255, 255, 0); font-family: BlissPro-Bold; font-weight: normal; line-height: 16px; color: rgb(255, 255, 255); margin-bottom: 20px; font-size: 16px; position: relative; background-color: rgb(225, 30, 50);\">CALL CENTER</h3><h4 style=\"-webkit-tap-highlight-color: rgba(255, 255, 255, 0); font-family: BlissPro-Bold; line-height: 24px; color: rgb(255, 255, 255); margin-top: 0px; margin-bottom: 20px; font-size: 24px; position: relative; background-color: rgb(225, 30, 50);\">800 20 6000</h4><p style=\"-webkit-tap-highlight-color: rgba(255, 255, 255, 0); margin-bottom: 20px; font-family: BlissPro-Light; color: rgb(255, 255, 255); font-size: 16px; line-height: 22px; padding: 0px 20px; background-color: rgb(225, 30, 50);\">Línea de atención al consumidor&nbsp;<br style=\"-webkit-tap-highlight-color: rgba(255, 255, 255, 0);\">Si necesitas información sobre nuestros productos o quieres consultar sobre la elaboración, envasado, información nutricional y calidad de ellos, comunícate con nosotros al siguiente número:</p><p style=\"-webkit-tap-highlight-color: rgba(255, 255, 255, 0); margin-bottom: 20px; font-family: BlissPro-Light; color: rgb(255, 255, 255); font-size: 16px; line-height: 22px; padding: 0px 20px; background-color: rgb(225, 30, 50);\">Horario de atención&nbsp;<br style=\"-webkit-tap-highlight-color: rgba(255, 255, 255, 0);\">Lunes a Viernes 8:30 a 18:00 hrs.&nbsp;<br style=\"-webkit-tap-highlight-color: rgba(255, 255, 255, 0);\">Sábado 9:00 a 14:00 hrs</p><h4 style=\"-webkit-tap-highlight-color: rgba(255, 255, 255, 0); font-family: BlissPro-Bold; line-height: 24px; color: rgb(255, 255, 255); margin-top: 0px; margin-bottom: 20px; font-size: 24px; position: relative; background-color: rgb(225, 30, 50);\">600 600 7777</h4><p style=\"-webkit-tap-highlight-color: rgba(255, 255, 255, 0); margin-bottom: 20px; font-family: BlissPro-Light; color: rgb(255, 255, 255); font-size: 16px; line-height: 22px; padding: 0px 20px; background-color: rgb(225, 30, 50);\">Línea de atención al cliente&nbsp;<br style=\"-webkit-tap-highlight-color: rgba(255, 255, 255, 0);\">Si eres dueño de un local comercial, negocio, almacén, o quieres comprar nuestros productos, comunícate con nosotros al siguiente número:</p><p style=\"-webkit-tap-highlight-color: rgba(255, 255, 255, 0); margin-bottom: 20px; font-family: BlissPro-Light; color: rgb(255, 255, 255); font-size: 16px; line-height: 22px; padding: 0px 20px; background-color: rgb(225, 30, 50);\">Horario de atención&nbsp;<br style=\"-webkit-tap-highlight-color: rgba(255, 255, 255, 0);\">Lunes a Viernes 8:30 a 18:00 hrs.&nbsp;<br style=\"-webkit-tap-highlight-color: rgba(255, 255, 255, 0);\">Sábado 9:00 a 14:00 hrs</p>', 'felicitaciones.png', 'pageContacto', 1),
(5, 3, 'contacto', NULL, 'trabaja', '', '<h3 style=\"-webkit-tap-highlight-color: rgba(255, 255, 255, 0); font-family: BlissPro-Bold; font-weight: normal; line-height: 16px; color: rgb(255, 255, 255); margin-bottom: 20px; font-size: 16px; position: relative; background-color: rgb(225, 30, 50);\">CALL CENTER</h3><h4 style=\"-webkit-tap-highlight-color: rgba(255, 255, 255, 0); font-family: BlissPro-Bold; line-height: 24px; color: rgb(255, 255, 255); margin-top: 0px; margin-bottom: 20px; font-size: 24px; position: relative; background-color: rgb(225, 30, 50);\">800 20 6000</h4><p style=\"-webkit-tap-highlight-color: rgba(255, 255, 255, 0); margin-bottom: 20px; font-family: BlissPro-Light; color: rgb(255, 255, 255); font-size: 16px; line-height: 22px; padding: 0px 20px; background-color: rgb(225, 30, 50);\">Línea de atención al consumidor&nbsp;<br style=\"-webkit-tap-highlight-color: rgba(255, 255, 255, 0);\">Si necesitas información sobre nuestros productos o quieres consultar sobre la elaboración, envasado, información nutricional y calidad de ellos, comunícate con nosotros al siguiente número:</p><p style=\"-webkit-tap-highlight-color: rgba(255, 255, 255, 0); margin-bottom: 20px; font-family: BlissPro-Light; color: rgb(255, 255, 255); font-size: 16px; line-height: 22px; padding: 0px 20px; background-color: rgb(225, 30, 50);\">Horario de atención&nbsp;<br style=\"-webkit-tap-highlight-color: rgba(255, 255, 255, 0);\">Lunes a Viernes 8:30 a 18:00 hrs.&nbsp;<br style=\"-webkit-tap-highlight-color: rgba(255, 255, 255, 0);\">Sábado 9:00 a 14:00 hrs</p><h4 style=\"-webkit-tap-highlight-color: rgba(255, 255, 255, 0); font-family: BlissPro-Bold; line-height: 24px; color: rgb(255, 255, 255); margin-top: 0px; margin-bottom: 20px; font-size: 24px; position: relative; background-color: rgb(225, 30, 50);\">600 600 7777</h4><p style=\"-webkit-tap-highlight-color: rgba(255, 255, 255, 0); margin-bottom: 20px; font-family: BlissPro-Light; color: rgb(255, 255, 255); font-size: 16px; line-height: 22px; padding: 0px 20px; background-color: rgb(225, 30, 50);\">Línea de atención al cliente&nbsp;<br style=\"-webkit-tap-highlight-color: rgba(255, 255, 255, 0);\">Si eres dueño de un local comercial, negocio, almacén, o quieres comprar nuestros productos, comunícate con nosotros al siguiente número:</p><p style=\"-webkit-tap-highlight-color: rgba(255, 255, 255, 0); margin-bottom: 20px; font-family: BlissPro-Light; color: rgb(255, 255, 255); font-size: 16px; line-height: 22px; padding: 0px 20px; background-color: rgb(225, 30, 50);\">Horario de atención&nbsp;<br style=\"-webkit-tap-highlight-color: rgba(255, 255, 255, 0);\">Lunes a Viernes 8:30 a 18:00 hrs.&nbsp;<br style=\"-webkit-tap-highlight-color: rgba(255, 255, 255, 0);\">Sábado 9:00 a 14:00 hrs</p>', 'trabaja.png', 'pageContacto', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sc_contactos`
--

CREATE TABLE `sc_contactos` (
  `id` bigint(20) NOT NULL,
  `nombre` varchar(200) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `ciudad` varchar(200) DEFAULT NULL,
  `f_nacimiento` date DEFAULT NULL,
  `telefono` varchar(45) DEFAULT NULL,
  `asunto` varchar(200) DEFAULT NULL,
  `consulta` longtext,
  `tipo` varchar(45) DEFAULT NULL,
  `activo` tinyint(1) DEFAULT '1',
  `send` int(1) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sc_cortes`
--

CREATE TABLE `sc_cortes` (
  `id` bigint(20) NOT NULL,
  `numero` int(10) DEFAULT NULL,
  `imagen` varchar(45) DEFAULT NULL,
  `nombre` varchar(200) DEFAULT NULL,
  `bajada` longtext,
  `descripcion` longtext,
  `activo` tinyint(1) NOT NULL DEFAULT '1',
  `recomendar_receta` varchar(200) DEFAULT NULL,
  `recetas` varchar(200) DEFAULT NULL,
  `recomendar` varchar(200) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sc_cortes`
--

INSERT INTO `sc_cortes` (`id`, `numero`, `imagen`, `nombre`, `bajada`, `descripcion`, `activo`, `recomendar_receta`, `recetas`, `recomendar`, `created`, `modified`) VALUES
(2, 1, 'corte_1.png', 'Cabeza', 'Buen corte para la cacerola.', 'Buen corte para la cacerola.', 1, NULL, NULL, NULL, '2018-02-24 15:55:45', '2018-02-24 15:55:45'),
(3, 2, 'corte_2.png', 'Papada', 'Se ubica bajo el cuello del cerdo.', 'Se ubica bajo el cuello del cerdo.', 1, NULL, NULL, NULL, '2018-02-18 18:10:04', '2018-02-18 18:10:04'),
(4, 3, 'corte_3.png', 'Pernil Mano', 'Se ubica en el muslo delantero del cerdo, entre la paleta y la mano. A la olla queda exquisita.', 'Se ubica en el muslo delantero del cerdo, entre la paleta y la mano. A la olla queda exquisita.', 1, NULL, NULL, NULL, '2018-02-18 18:10:45', '2018-02-18 18:10:45'),
(5, 4, 'corte_4.png', 'Chuleta Vetada', 'Es cercana a las costillas. La Chuleta Vetada tiene más contenido graso, pero es mucho más sabrosa.', 'Es cercana a las costillas. La Chuleta Vetada tiene más contenido graso, pero es mucho más sabrosa.', 1, NULL, NULL, NULL, '2018-02-18 18:11:14', '2018-02-18 18:11:14'),
(6, 5, 'corte_5.png', 'Pulpa Paleta', 'La Pulpa Paleta es un corte que se encuentra en el muslo delantero del cerdo. Lo recomendamos a la parrilla.', 'La Pulpa Paleta es un corte que se encuentra en el muslo delantero del cerdo. Lo recomendamos a la parrilla.', 1, NULL, NULL, NULL, '2018-02-18 18:12:03', '2018-02-18 18:12:03');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sc_cortes_recetas`
--

CREATE TABLE `sc_cortes_recetas` (
  `id` bigint(20) NOT NULL,
  `corte_id` bigint(20) NOT NULL,
  `sc_receta_id` bigint(20) NOT NULL,
  `activo` tinyint(1) DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sc_ingrediente_recetas`
--

CREATE TABLE `sc_ingrediente_recetas` (
  `id` bigint(20) NOT NULL,
  `ingrediente` varchar(200) DEFAULT NULL,
  `activo` tinyint(1) DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `receta_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sc_ingrediente_recetas`
--

INSERT INTO `sc_ingrediente_recetas` (`id`, `ingrediente`, `activo`, `created`, `modified`, `receta_id`) VALUES
(5, '900g de posta rosada SuperCerdo', 1, '2018-02-27 11:52:10', '2018-02-27 11:52:10', 2),
(6, '2 cucharadas de aceite de oliva', 1, '2018-02-27 11:52:10', '2018-02-27 11:52:10', 2),
(7, '2 cucharadas de aceite de oliva', 1, '2018-02-27 11:52:10', '2018-02-27 11:52:10', 2),
(8, 'Asiento de cerdo al jugo con verduras', 1, '2018-02-27 11:54:05', '2018-02-27 11:54:05', 3),
(9, 'Asiento de cerdo al jugo con verduras', 1, '2018-02-27 11:54:05', '2018-02-27 11:54:05', 3),
(11, '2 kilos de costillitas ribs  mediterráneas ,Super Cerdo', 1, '2018-02-28 13:35:12', '2018-02-28 13:35:12', 4),
(12, '2 kilos de costillitas ribs  mediterráneas ,Super Cerdo', 1, '2018-02-28 13:36:09', '2018-02-28 13:36:09', 5),
(16, '2 kilos de costillitas ribs  mediterráneas ,Super Cerdo', 1, '2018-02-28 17:21:24', '2018-02-28 17:21:24', 7),
(17, '2 kilos de costillitas ribs  mediterráneas ,Super Cerdo', 1, '2018-02-28 17:23:02', '2018-02-28 17:23:02', 8),
(18, '2 kilos de costillitas ribs  mediterráneas ,Super Cerdo', 1, '2018-02-28 17:24:30', '2018-02-28 17:24:30', 9),
(19, '2 kilos de costillitas ribs  mediterráneas ,Super Cerdo', 1, '2018-02-28 17:26:06', '2018-02-28 17:26:06', 6),
(20, '2 kilos de costillitas ribs  mediterráneas ,Super Cerdo', 1, '2018-02-28 17:28:15', '2018-02-28 17:28:15', 10),
(21, '2 kilos de costillitas ribs  mediterráneas ,Super Cerdo', 1, '2018-03-02 00:47:42', '2018-03-02 00:47:42', 1),
(22, 'pimienta recién molida', 1, '2018-03-02 00:47:42', '2018-03-02 00:47:42', 1),
(23, 'salsa expresso', 1, '2018-03-02 00:47:42', '2018-03-02 00:47:42', 1),
(24, '3 cdas de aceite de oliva', 1, '2018-03-02 00:47:42', '2018-03-02 00:47:42', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sc_linea_productos`
--

CREATE TABLE `sc_linea_productos` (
  `id` bigint(20) NOT NULL,
  `nombre` varchar(200) DEFAULT NULL,
  `parent_id` bigint(20) DEFAULT NULL,
  `activo` tinyint(1) DEFAULT '1',
  `producto_count` int(11) DEFAULT NULL,
  `imagen` varchar(200) DEFAULT NULL,
  `destacado` tinyint(1) NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `administrador_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sc_linea_productos`
--

INSERT INTO `sc_linea_productos` (`id`, `nombre`, `parent_id`, `activo`, `producto_count`, `imagen`, `destacado`, `created`, `modified`, `administrador_id`) VALUES
(1, 'LÍNEA COCINA', NULL, 1, 20, 'banner_lineas_cocina_n.jpg', 0, NULL, '2018-03-02 12:04:45', 3),
(2, 'LÍNEA SANDWICH', NULL, 1, 6, 'banner_lineas_sandwich_n.jpg', 0, NULL, '2018-03-02 12:04:26', 3),
(3, 'LÍNEA PARRILLA', NULL, 1, 7, 'banner_lineas_parrilla_n.jpg', 0, NULL, '2018-03-02 11:33:53', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sc_marcas`
--

CREATE TABLE `sc_marcas` (
  `id` bigint(20) NOT NULL,
  `nombre` varchar(200) DEFAULT NULL,
  `imagen` varchar(200) DEFAULT NULL,
  `activo` tinyint(1) DEFAULT '1',
  `orden` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `administrador_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sc_nosotros`
--

CREATE TABLE `sc_nosotros` (
  `id` bigint(20) NOT NULL,
  `titulo` varchar(200) DEFAULT NULL,
  `bajada` longtext,
  `descripcion` longtext,
  `imagen` varchar(200) DEFAULT NULL,
  `activo` tinyint(1) DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `administrador_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sc_noticias`
--

CREATE TABLE `sc_noticias` (
  `id` bigint(20) NOT NULL,
  `titulo` varchar(200) DEFAULT NULL,
  `bajada` longtext,
  `descripcion` longtext,
  `activo` tinyint(1) DEFAULT '1',
  `imagen` varchar(200) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `administrador_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sc_noticias`
--

INSERT INTO `sc_noticias` (`id`, `titulo`, `bajada`, `descripcion`, `activo`, `imagen`, `created`, `modified`, `administrador_id`) VALUES
(1, 'Chile sigue ganando no exento de dificultades', 'La selección masculina conservó su invicto en la cita mundialista tras conseguir un nuevo triunfo frente a Bulgaria por 9 goles a 1. Aunque el partido fue duro desde el comienzo con llegadas para ambos elencos, poco a poco Chile logró un mayor protagonismo que se tradujo en el marcador final pese a la extraordinaria participación del portero búlgaro.', '<p style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 14px; line-height: 22px; font-family: Arial, Helvetica, sans-serif; color: rgb(0, 0, 0); text-align: justify;\">La selección masculina conservó su invicto en la cita mundialista tras conseguir un nuevo triunfo frente a Bulgaria por 9 goles a 1. Aunque el partido fue duro desde el comienzo con llegadas para ambos elencos, poco a poco Chile logró un mayor protagonismo que se tradujo en el marcador final pese a la extraordinaria participación del portero búlgaro.</p><p style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 14px; line-height: 22px; font-family: Arial, Helvetica, sans-serif; color: rgb(0, 0, 0); text-align: justify;\">Con este triunfo, la ‘Roja’ acumuló su sexta victoria consecutiva en el torneo y se posicionó en los puestos de avanzada en la segunda fase de grupos de la competición. La figura nacional, Jorge Pacheco, afirmó que “estamos ilusionados desde que empezó el campeonato. Fue un resultado abultado, pero apretado en el juego, así es el fútbol calle”.</p><p style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 14px; line-height: 22px; font-family: Arial, Helvetica, sans-serif; color: rgb(0, 0, 0); text-align: justify;\">En tanto, la selección femenina logró una apretada victoria sobre el combinado de Holanda por 5-2. Triunfo que le permitió a las nacionales seguir sólidas en el camino hacia el título. Durante el encuentro se vio a un Chile más errático que en jornadas anteriores, sin embargo, los desempeños individuales sirvieron para que las dirigidas por Javier Castro consiguieran su quinto triunfo consecutivo.</p><p style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 14px; line-height: 22px; font-family: Arial, Helvetica, sans-serif; color: rgb(0, 0, 0); text-align: justify;\">Luego de la victoria del elenco nacional, el entrenador de la ‘Rojita’ habló acerca del encuentro y afirmó que “se nos complicó un poco el partido porque fallamos en el finiquito, nos creamos muchas oportunidades pero no pudimos concretar como quisimos. Las chicas quieren seguir mejorando y salieron frustradas y con ganas de más al final del encuentro”.</p><p style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 14px; line-height: 22px; font-family: Arial, Helvetica, sans-serif; color: rgb(0, 0, 0); text-align: justify;\">Una nueva jornada se vivirá este jueves desde las 10 hasta las 19 horas.&nbsp;<b>Vive todo el Mundial a través de las pantallas de CDF, canal oficial del Homeless World Cup Chile 2014.</b></p><p style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 14px; line-height: 22px; font-family: Arial, Helvetica, sans-serif; color: rgb(0, 0, 0); text-align: justify;\">​El fixture lo puedes revisar en&nbsp;<a href=\"http://www.homelessworldcup.org/\" target=\"_blank\" style=\"color: rgb(149, 6, 0); cursor: pointer;\">www.homelessworldcup.org</a></p><p style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 14px; line-height: 22px; font-family: Arial, Helvetica, sans-serif; color: rgb(0, 0, 0); text-align: justify;\"><b>Link de descarga de fotos y video (gentileza de CDF)</b>:&nbsp;<a href=\"http://goo.gl/3Poaaa\" target=\"_blank\" style=\"color: rgb(149, 6, 0); cursor: pointer;\">http://goo.gl/3Poaaa</a></p>', 1, 'fotos_futbol_calle_2014_123455_575x498.jpg', '2018-02-28 13:31:47', '2018-02-28 13:31:47', 3),
(2, 'Chile avanza a paso firme en el Mundial de Fútbol Calle', 'Las selecciones chilenas masculina y femenina siguen con tranco ganador en el Mundial de Fútbol Calle, tras superar el día de hoy a Kyrgyzstan y Canadá en el caso de los hombres y a India y Noruega por el lado de las mujeres.', '<p style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 14px; line-height: 22px; font-family: Arial, Helvetica, sans-serif; color: rgb(0, 0, 0); text-align: justify;\">En el primer turno, la selección femenina superó sin mayores complicaciones por 11-0 a India. El encuentro vio a una selección chilena ordenada demostrando un gran juego asociado, mientras que las rivales no pudieron aprovechar las situaciones de gol creadas. Para las chilenas anotaron Julissa Barrera, Belén Castañeda y Dina Barria en dos y Andrea Cerda fue la goleadora del partido con 4 dianas.</p><p style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 14px; line-height: 22px; font-family: Arial, Helvetica, sans-serif; color: rgb(0, 0, 0); text-align: justify;\">En el turno de la tarde, las chilenas cerraron la jornada de forma positiva al ganar por un amplio marcador de 14 tantos a 0 al seleccionado de Noruega.</p><p style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 14px; line-height: 22px; font-family: Arial, Helvetica, sans-serif; color: rgb(0, 0, 0); text-align: justify;\">Tras la doble jornada, la figura nacional, Belén Castañeda, admitió que “es emocionante sentir el cariño de la gente cada vez que jugamos. Eso nos da la garra para ganar cada partido. Queremos salir invictas para lograr que la copa se quede en casa”, finalizó la número 13 del equipo nacional.</p><p style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 14px; line-height: 22px; font-family: Arial, Helvetica, sans-serif; color: rgb(0, 0, 0); text-align: justify;\">Cerrando la jornada para los combinados nacionales, la selección chilena de hombres logró una valiosa victoria sobre el equipo de Kyrgyzstan por 9 tantos contra 5. El elenco local tuvo un duro encuentro contra un equipo bastante sólido en facetas defensivas y rápido en el contragolpe, incluso el equipo visitante contó con 2 lanzamientos penales desperdiciados antes de los 3 minutos de juego, pero el meta nacional Poblete logró desviar ambos tiros. Los goles de la selección nacional fueron obra de Aguayo, Mariqueo, Espinoza y Trujillo. Con este resultado mantienen el invicto en la fase de grupos.</p><p style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 14px; line-height: 22px; font-family: Arial, Helvetica, sans-serif; color: rgb(0, 0, 0); text-align: justify;\">Las selecciones chilenas volverán al terreno de juego el día de mañana en una jornada que se extenderá desde las 10 hasta las 14.30 hrs buscando asegurar el cupo para clasificar a la segunda ronda del mundial. Vive todo el mundial a través de las pantallas de CDF, canal oficial del Mundial de Futbol Calle.</p>', 1, 'fotos_futbol_calle_2014_575x498.jpg', '2018-02-28 13:38:26', '2018-02-28 13:38:26', 3),
(3, 'Primera jornada del Mundial Fútbol Calle', 'El lunes 20 de octubre se vivió una ferviente inauguración del Mundial Fútbol Calle.', '<p style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 14px; line-height: 22px; font-family: Arial, Helvetica, sans-serif; color: rgb(0, 0, 0); text-align: justify;\">En categoría hombres Chile goleó a Bosnia 7-1 y a Eslovenia 12-1</p><p style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 14px; line-height: 22px; font-family: Arial, Helvetica, sans-serif; color: rgb(0, 0, 0); text-align: justify;\">Las mujeres tampoco se quedaron atrás y tuvieron un gran partido inaugural con Argentina, 14-1 fue el aplastante resultado a favor de Chile.</p><h1 style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 14px; line-height: normal; font-family: FuturaBoldRegular, Helvetica, sans-serif; color: rgb(149, 6, 0); margin-top: 5px; text-align: justify;\">Horario&nbsp; partidos martes 21</h1><p style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 14px; line-height: 22px; font-family: Arial, Helvetica, sans-serif; color: rgb(0, 0, 0); text-align: justify;\"><strong>PITCH 1</strong></p><p style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 14px; line-height: 22px; font-family: Arial, Helvetica, sans-serif; color: rgb(0, 0, 0); text-align: justify;\">12:00&nbsp;<strong>Chile v India</strong><br>12:20&nbsp;<strong>Argentina v Brazil</strong><br>12:40&nbsp;<strong>Hungary&nbsp; v&nbsp; Norway</strong>&nbsp;<em>friendly</em><br>13:00&nbsp;<strong>Mexico v Sweden</strong><br>13:20&nbsp;<strong>Bosnia Herz&nbsp; v Argentina</strong><br>13:40&nbsp;<strong>Scotland v Hungary</strong><br>14:00&nbsp;<strong>Netherlands v Bulgaria</strong><br>14:20&nbsp;<strong>Denmark v Romania</strong><br>14:40&nbsp;<strong>Norway v Chile</strong><br>15:00&nbsp;<strong>Ireland v Wales</strong><br>15:20&nbsp;<strong>Italy v France</strong><br>15:40&nbsp;<strong>Costa Rica v Czech Republic</strong><br>16:00&nbsp;<strong>Wales v Hungary</strong><br>16:20&nbsp;<strong>Chile v Kyrgyzstan</strong><br>16:40&nbsp;<strong>Indonesia v South Korea</strong><br>17:00&nbsp;<strong>Russia v Austria</strong>&nbsp;<em>friendly</em><br>17:20<strong>&nbsp;Brazil v Mexico</strong>&nbsp;<em>friendly</em><br>17:40&nbsp;<strong>Portugal v Iithuania</strong>&nbsp;<em>friendly</em><br>18:00&nbsp;<strong>Canada v Chile</strong><br>18:20<strong>&nbsp;USA v Brazil</strong>&nbsp;<em>friendly</em><br>18:40&nbsp;<strong>Bulgaria v USA</strong><br>19:00&nbsp;<strong>Romania v India</strong><br>19:20&nbsp;<strong>Namibia v Cambodia</strong></p><p style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 14px; line-height: 22px; font-family: Arial, Helvetica, sans-serif; color: rgb(0, 0, 0); text-align: justify;\"><strong>PITCH 2<br></strong><br>12:00&nbsp;<strong>Wales v Netherlands</strong>&nbsp;<em>friendly</em><br>12:20&nbsp;<strong>South Korea v Norway</strong><br>12:40&nbsp;<strong>Kyrgyzstan v Slovenia</strong><br>13:00&nbsp;<strong>India v USA</strong><em>&nbsp;friendly</em><br>13:20&nbsp;<strong>Cambodia v Greece</strong>&nbsp;<em>friendly</em><br>13:40&nbsp;<strong>Hong Kong v Germany</strong>&nbsp;<em>friendly</em><br>14:00&nbsp;<strong>England v Argentina</strong>&nbsp;<em>friendly</em><br>14:20&nbsp;<strong>Finland v Lithuania</strong><br>14:40&nbsp;<strong>Poland v Namibia</strong><br>15:00<strong>&nbsp;India v Netherlands</strong><br>15:20<strong>&nbsp;USA v Mexico</strong><br>15:40&nbsp;<strong>Sweden v England</strong><br>16:00&nbsp;<strong>Bosnia Herz v Canada</strong><br>16:20&nbsp;<strong>Scotland v North. Ireland</strong><br>16:40&nbsp;<strong>Netherlands v England</strong><br>17:00&nbsp;<strong>Denmark v Peru</strong><br>17:20&nbsp;<strong>Poland v Switzerland</strong><br>17:40&nbsp;<strong>Ireland v Sweden</strong><br>18:00&nbsp;<strong>Italy v Ghana</strong><br>18:20&nbsp;<strong>Wales v Greece</strong><br>18:40&nbsp;<strong>France v Hong Kong</strong><br>19:00&nbsp;<strong>Czech Republic v Germany&nbsp;<br></strong></p><p style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 14px; line-height: 22px; font-family: Arial, Helvetica, sans-serif; color: rgb(0, 0, 0); text-align: justify;\"><strong>PITCH 3<br></strong><br>12:00&nbsp;<strong>Costa Rica v Finland</strong><br>12:20&nbsp;<strong>North. Ireland v Indonesia</strong><br>12:40&nbsp;<strong>England v Russia</strong><br>13:00&nbsp;<strong>Peru v Austria</strong><br>13:20&nbsp;<strong>Switzerland v Brazil</strong><br>13:40&nbsp;<strong>Sweden v Mexico</strong><br>14:00&nbsp;<strong>Ghana v Portugal</strong><br>15:20&nbsp;<strong>Argentina v Slovenia</strong><br>15:40&nbsp;<strong>Hungary v Norway</strong></p>', 1, 'futbol2014m_575x498.jpg', '2018-02-28 13:39:42', '2018-02-28 13:39:42', 3),
(4, 'Quieren hacer historia: Chile se instaló en la final del Mundial de Fútbol Calle', '', '<p style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 14px; line-height: 22px; font-family: Arial, Helvetica, sans-serif; color: rgb(0, 0, 0); text-align: justify;\">La selecciones nacionales tanto masculina como femenina tuvieron una dura jornada sabatina en el certamen que se disputa en la Plaza de la Ciudadanía. Con un recinto repleto de hinchas que cantaron durante todos los encuentros, la ‘Roja’ masculina tuvo que superar a Holanda en cuartos de final y bajar a su eterna bestia negra, Brasil en semis para sacar pasajes a la final del torneo. En tanto la ‘Rojita’ femenina disputará la copa tras vencer a Gales y Brasil.</p><p style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 14px; line-height: 22px; font-family: Arial, Helvetica, sans-serif; color: rgb(0, 0, 0); text-align: justify;\">Costó pero se logró. En un primer partido muy apretado, los dirigidos por Juan Erazo supieron sacar provecho de las debilidades del rival y vencieron por 4-3 al combinado de Holanda .</p><p style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 14px; line-height: 22px; font-family: Arial, Helvetica, sans-serif; color: rgb(0, 0, 0); text-align: justify;\">Pero la tarea que venía en el segundo partido de la jornada iba a tener la misma tensión que el anterior. Esto porque la ‘Roja’ enfrentaba en semifinales a Brasil, uno de los favoritos de la cita mundialista. La selección nacional supo dosificar el desgaste físico y desequilibrar en pasajes claves del partido para vencer en el último minuto al ‘Scratch’ por 8 tantos a 7.</p><p style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 14px; line-height: 22px; font-family: Arial, Helvetica, sans-serif; color: rgb(0, 0, 0); text-align: justify;\">“Si bien siempre entramos con la mentalidad de ganar, Brasil nos mantuvo bien concentrados desde el comienzo. Chile merece triunfos y vamos a luchar de manera sana para quedarnos con la copa”, señaló el capitán de la selección, Robert Sandoval.</p><p style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 14px; line-height: 22px; font-family: Arial, Helvetica, sans-serif; color: rgb(0, 0, 0); text-align: justify;\">En el turno de las mujeres, el recinto ubicado frente al Palacio de La Moneda fue escenario del paso de Chile a la final de la categoría tras vencer a Gales en cuartos de final y lograr un histórico triunfo ante Brasil en semifinales. La ‘Rojita’ supo llevar la presión del encuentro y consagró su paso a la final con un peleado 4-1 ante la ‘Verde-amarela’ con una sólida actuación de la portera chilena Romina Lillo.</p><p style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 14px; line-height: 22px; font-family: Arial, Helvetica, sans-serif; color: rgb(0, 0, 0); text-align: justify;\">“Seguimos con la misma confianza que desde el primer partido. Estamos en nuestra casa y en la final eso se tiene que demostrar”, declaró la goleadora nacional, Belen Castañeda.</p><p style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 14px; line-height: 22px; font-family: Arial, Helvetica, sans-serif; color: rgb(0, 0, 0); text-align: justify;\">Las finales del primer Mundial de Fútbol Calle organizado por nuestro país se vivirá este domingo desde las 17:30 hrs y tendrá como protagonistas a Chile que peleará el título ante Bosnia Herzegovina en categoría masculina y frente a Mexico, las actuales campeonas, por el lado de las mujeres. De ganar los dos duelos, Chile se transformaría en el primer país en la historia del torneo que sale campeón con ambas selecciones.</p><p style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 14px; line-height: 22px; font-family: Arial, Helvetica, sans-serif; color: rgb(0, 0, 0); text-align: justify;\">Todo esto lo podrás vivir a través de CDF, canal oficial de la Homeless World Cup Chile 2014.</p>', 1, 'noticia25_575x498.png', '2018-02-28 17:32:37', '2018-02-28 17:32:37', 3),
(5, 'Chile se corona bicampeón Mundial', '', '<p style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 14px; line-height: 22px; font-family: Arial, Helvetica, sans-serif; color: rgb(0, 0, 0); text-align: justify;\">Bajo un recinto atiborrado de espectadores se vivió este domingo desde las 17:00 hrs la gran final del espectáculo deportivo social más importante</p><p style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 14px; line-height: 22px; font-family: Arial, Helvetica, sans-serif; color: rgb(0, 0, 0); text-align: justify;\">Tras una reñida competencia que tuvo una duración de una semana, por primera vez en la historia de este campeonato, un país se convierte en campeón con ambas selecciones.</p><p style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 14px; line-height: 22px; font-family: Arial, Helvetica, sans-serif; color: rgb(0, 0, 0); text-align: justify;\">Fueron siete días de lucha en pleno corazón de Santiago, en la Plaza de la Ciudadanía, y con más de 200 mil espectadores que acompañaron durante todos los partidos a “La Roja” con vítores de C-H-I y llenando el recinto con los colores de la bandera patria.</p><p style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 14px; line-height: 22px; font-family: Arial, Helvetica, sans-serif; color: rgb(0, 0, 0); text-align: justify;\">Durante el torneo, la selección masculina cosechó sólo victorias, ganándoles en cuartos de final a Holanda y en semifinal, en un apretado encuentro, a su mayor rival: Brasil por 8-7. Así llegaron a la esperada final frente a Bosnia Herzegovina, partido que los consagró campeones del mundo tras derrotarlos 5-2.</p><p style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 14px; line-height: 22px; font-family: Arial, Helvetica, sans-serif; color: rgb(0, 0, 0); text-align: justify;\">Las mujeres también fueron inquebrantables durante todo el evento deportivo, superando a Gales en cuartos de final y logrando un histórico triunfo ante Brasil en semifinales por 4-1. El partido final no fue fácil, ya que se enfrentaron al campeón del 2013, México. En un ajustado encuentro por 4-3 se impusieron a las aztecas, resultado que las deja como acreedoras del título mundial.</p>', 1, 'webfcwin_575x498.jpg', '2018-02-28 17:41:05', '2018-02-28 17:41:05', 3),
(6, 'La ‘Roja’ sigue sumando puntos en el corazón de Santiago', '', '<p style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 14px; line-height: 22px; font-family: Arial, Helvetica, sans-serif; color: rgb(0, 0, 0); text-align: justify;\">Los elencos nacionales lograron terminar de manera positiva el día con dos victorias para cada categoría que les permite seguir en la punta de la tabla en sus respectivos grupos.</p><p style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 14px; line-height: 22px; font-family: Arial, Helvetica, sans-serif; color: rgb(0, 0, 0); text-align: justify;\">Durante el primer turno del día, el combinado chileno masculino logró un triunfo frente a su par de Austria. La ‘Roja’ tuvo que trabajar arduamente durante los primeros minutos del partido para abrir el marcador y poder establecer una diferencia que les permitió quedarse con el partido por 8 tantos contra 2.</p><p style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 14px; line-height: 22px; font-family: Arial, Helvetica, sans-serif; color: rgb(0, 0, 0); text-align: justify;\">En el partido de la tarde, la selección masculina pudo retener el invicto ganando su partido contra Hungría con un 6-1 categórico.</p><p style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 14px; line-height: 22px; font-family: Arial, Helvetica, sans-serif; color: rgb(0, 0, 0); text-align: justify;\">Por el lado de la categoría femenina, el elenco nacional abrió la jornada con un contundente 8-1 ante Holanda. En un encuentro con muchas llegadas y ocasiones de gol desperdiciadas por ambos equipos, las chilenas lograron conservar la calma y lograr definir en los minutos finales el resultado.</p><p style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 14px; line-height: 22px; font-family: Arial, Helvetica, sans-serif; color: rgb(0, 0, 0); text-align: justify;\">En el turno de la tarde, las dirigidas por Javier Castro, vieron peligrar su invicto en el campeonato por el equipo proveniente de Hungría, incluso teniendo que remontar un 2-0 en contra durante los primeros minutos del encuentro. A medida que avanzaba el partido, las chilenas fueron encontrando su fútbol logrando descolocar a la defensa húngara y abrir el arco rival. Comandados por la capitana Denise Silva y la goleadora Belén Castañeda, Chile logró un resultado de 6 goles contra 2 permitiéndoles conservar la punta del Grupo A.</p><p style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 14px; line-height: 22px; font-family: Arial, Helvetica, sans-serif; color: rgb(0, 0, 0); text-align: justify;\">El fútbol volverá a la Plaza de la Ciudadanía este viernes cuando la ‘Roja’ masculina salte a la cancha a las 15:40hrs frente al elenco de Grecia. Por su parte, el combinado femenino verá acción a las 18:40hrs cuando enfrente a su par de Inglaterra. Vive todo el Mundial a través de las pantallas de CDF, canal oficial del Mundial Futbol Calle Chile 2014.</p>', 1, 'fotos_futbol_calle_chile_575x498.jpg', '2018-02-28 17:41:58', '2018-02-28 17:41:58', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sc_nutricional_productos`
--

CREATE TABLE `sc_nutricional_productos` (
  `id` bigint(20) NOT NULL,
  `extra` tinyint(1) DEFAULT '0',
  `nombre` varchar(45) DEFAULT NULL,
  `medida` varchar(45) DEFAULT NULL,
  `100g` varchar(45) DEFAULT NULL,
  `porcion` varchar(45) DEFAULT NULL,
  `cda` varchar(45) DEFAULT NULL,
  `activo` tinyint(1) DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `producto_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sc_nutricional_productos`
--

INSERT INTO `sc_nutricional_productos` (`id`, `extra`, `nombre`, `medida`, `100g`, `porcion`, `cda`, `activo`, `created`, `modified`, `producto_id`) VALUES
(12, 0, 'EnergÃ­a', '(Kcal)', '316', '253', '13%', 1, '2018-02-27 01:48:52', '2018-02-27 01:48:52', 1),
(13, 0, 'ProteÃ­nas', '(g)', '14,9', '11,9', '24%', 1, '2018-02-27 01:48:52', '2018-02-27 01:48:52', 1),
(14, 0, 'Ãc. Grasos Saturados', '(g)', '9,9', '7,9', '36%', 1, '2018-02-27 01:48:52', '2018-02-27 01:48:52', 1),
(15, 0, 'Ãc. Grasos Trans.', '(g)', '0,0', '0,0', '', 1, '2018-02-27 01:48:52', '2018-02-27 01:48:52', 1),
(16, 0, 'Ãc. Grasos Monoinsat.', '(g)', '9,9', '7,9', '', 1, '2018-02-27 01:48:52', '2018-02-27 01:48:52', 1),
(17, 0, 'Ãc. Grasos Poliinsat.', '(g)', '5,3', '4,2', '', 1, '2018-02-27 01:48:52', '2018-02-27 01:48:52', 1),
(18, 0, 'Colesterol', '(mg)', '124', '99', '33%', 1, '2018-02-27 01:48:52', '2018-02-27 01:48:52', 1),
(19, 0, 'H de C disponibles', '(g)', '6,7', '5,4', '2%', 1, '2018-02-27 01:48:52', '2018-02-27 01:48:52', 1),
(20, 0, 'AzÃºcares Totales', '(g)', '', '', '', 1, '2018-02-27 01:48:52', '2018-02-27 01:48:52', 1),
(21, 1, 'PorciÃ³n', '80 g (1 trozo)', NULL, NULL, NULL, 1, '2018-02-27 01:48:52', '2018-02-27 01:48:52', 1),
(22, 1, 'Porciones por envase', 'aprox. aprox. 9', NULL, NULL, NULL, 1, '2018-02-27 01:48:52', '2018-02-27 01:48:52', 1),
(25, 0, '1', '1', '', '', '', 1, '2018-02-27 06:37:11', '2018-02-27 06:37:11', 3),
(26, 1, '1', '1', NULL, NULL, NULL, 1, '2018-02-27 06:37:11', '2018-02-27 06:37:11', 3),
(27, 0, '12', '12', '', '', '', 1, '2018-02-27 06:37:45', '2018-02-27 06:37:45', 4),
(28, 1, '12', '12', NULL, NULL, NULL, 1, '2018-02-27 06:37:45', '2018-02-27 06:37:45', 4),
(29, 0, '1', '1', '', '', '', 1, '2018-02-27 06:38:27', '2018-02-27 06:38:27', 5),
(30, 1, '1', '1', NULL, NULL, NULL, 1, '2018-02-27 06:38:27', '2018-02-27 06:38:27', 5),
(31, 0, '1', '1', '', '', '', 1, '2018-02-27 06:39:04', '2018-02-27 06:39:04', 6),
(32, 1, '1', '2', NULL, NULL, NULL, 1, '2018-02-27 06:39:04', '2018-02-27 06:39:04', 6),
(33, 0, '12', '12', '', '', '', 1, '2018-02-27 06:46:52', '2018-02-27 06:46:52', 7),
(34, 1, '12', '12', NULL, NULL, NULL, 1, '2018-02-27 06:46:52', '2018-02-27 06:46:52', 7),
(35, 0, 'n1', '1', '2', '3', '4', 1, '2018-02-27 13:02:46', '2018-02-27 13:02:46', 2),
(36, 1, '1', '2', NULL, NULL, NULL, 1, '2018-02-27 13:02:46', '2018-02-27 13:02:46', 2),
(61, 0, 'nutriente', '1', '1', '1', '1', 1, '2018-02-28 12:08:21', '2018-02-28 12:08:21', 12),
(62, 1, 'Porción', '80 g (1 trozo)', NULL, NULL, NULL, 1, '2018-02-28 12:08:21', '2018-02-28 12:08:21', 12),
(63, 0, 'nutriente', '1', '1', '1', '1', 1, '2018-02-28 12:10:46', '2018-02-28 12:10:46', 13),
(64, 1, 'Porción', '80 g (1 trozo)', NULL, NULL, NULL, 1, '2018-02-28 12:10:46', '2018-02-28 12:10:46', 13),
(67, 0, 'nutriente', '1', '1', '1', '1', 1, '2018-02-28 12:13:55', '2018-02-28 12:13:55', 15),
(68, 1, 'Porción', '80 g (1 trozo)', NULL, NULL, NULL, 1, '2018-02-28 12:13:55', '2018-02-28 12:13:55', 15),
(69, 0, 'nutriente', '1', '1', '1', '1', 1, '2018-02-28 12:15:12', '2018-02-28 12:15:12', 16),
(70, 1, 'Porción', '80 g (1 trozo)', NULL, NULL, NULL, 1, '2018-02-28 12:15:12', '2018-02-28 12:15:12', 16),
(71, 0, 'nutriente', '1', '1', '1', '1', 1, '2018-02-28 12:16:34', '2018-02-28 12:16:34', 17),
(72, 1, 'Porción', '80 g (1 trozo)', NULL, NULL, NULL, 1, '2018-02-28 12:16:34', '2018-02-28 12:16:34', 17),
(73, 0, 'nutriente', '1', '1', '1', '1', 1, '2018-02-28 12:18:39', '2018-02-28 12:18:39', 18),
(74, 1, 'Porción', '80 g (1 trozo)', NULL, NULL, NULL, 1, '2018-02-28 12:18:39', '2018-02-28 12:18:39', 18),
(75, 0, 'nutriente', '1', '1', '1', '1', 1, '2018-02-28 12:19:35', '2018-02-28 12:19:35', 19),
(76, 1, 'Porción', '80 g (1 trozo)', NULL, NULL, NULL, 1, '2018-02-28 12:19:35', '2018-02-28 12:19:35', 19),
(77, 0, 'nutriente', '1', '1', '1', '1', 1, '2018-02-28 12:20:38', '2018-02-28 12:20:38', 20),
(78, 1, 'Porción', '80 g (1 trozo)', NULL, NULL, NULL, 1, '2018-02-28 12:20:38', '2018-02-28 12:20:38', 20),
(79, 0, 'nutriente', '1', '1', '1', '1', 1, '2018-02-28 12:23:28', '2018-02-28 12:23:28', 21),
(80, 1, 'Porción', '80 g (1 trozo)', NULL, NULL, NULL, 1, '2018-02-28 12:23:28', '2018-02-28 12:23:28', 21),
(83, 0, 'nutriente', '1', '1', '1', '1', 1, '2018-02-28 12:26:01', '2018-02-28 12:26:01', 23),
(84, 1, 'Porción', '80 g (1 trozo)', NULL, NULL, NULL, 1, '2018-02-28 12:26:01', '2018-02-28 12:26:01', 23),
(85, 0, 'nutriente', '1', '1', '1', '1', 1, '2018-02-28 12:28:26', '2018-02-28 12:28:26', 24),
(86, 1, 'Porción', '80 g (1 trozo)', NULL, NULL, NULL, 1, '2018-02-28 12:28:26', '2018-02-28 12:28:26', 24),
(87, 0, 'nutriente', '1', '1', '1', '1', 1, '2018-02-28 12:30:17', '2018-02-28 12:30:17', 25),
(88, 1, 'Porción', '80 g (1 trozo)', NULL, NULL, NULL, 1, '2018-02-28 12:30:17', '2018-02-28 12:30:17', 25),
(93, 0, 'nutriente', '1', '1', '1', '1', 1, '2018-02-28 12:33:30', '2018-02-28 12:33:30', 28),
(94, 1, 'Porción', '80 g (1 trozo)', NULL, NULL, NULL, 1, '2018-02-28 12:33:30', '2018-02-28 12:33:30', 28),
(97, 0, 'nutriente', '1', '2', '1', '1', 1, '2018-02-28 12:53:01', '2018-02-28 12:53:01', 11),
(98, 1, 'Porción', '80 g (1 trozo)', NULL, NULL, NULL, 1, '2018-02-28 12:53:01', '2018-02-28 12:53:01', 11),
(104, 0, 'nutriente', '1', '1', '1', '1', 1, '2018-02-28 12:54:43', '2018-02-28 12:54:43', 9),
(105, 1, 'porción', '1', NULL, NULL, NULL, 1, '2018-02-28 12:54:43', '2018-02-28 12:54:43', 9),
(106, 0, 'nutriente', '1', '1', '1', '1', 1, '2018-02-28 12:55:29', '2018-02-28 12:55:29', 10),
(107, 1, 'Porción', '80 g (1 trozo)', NULL, NULL, NULL, 1, '2018-02-28 12:55:29', '2018-02-28 12:55:29', 10),
(108, 0, 'nutriente', '1', '1', '1', '1', 1, '2018-02-28 12:57:16', '2018-02-28 12:57:16', 26),
(109, 1, 'Porción', '80 g (1 trozo)', NULL, NULL, NULL, 1, '2018-02-28 12:57:16', '2018-02-28 12:57:16', 26),
(110, 0, 'nutriente', '1', '1', '1', '1', 1, '2018-02-28 12:57:47', '2018-02-28 12:57:47', 27),
(111, 1, 'Porción', '80 g (1 trozo)', NULL, NULL, NULL, 1, '2018-02-28 12:57:47', '2018-02-28 12:57:47', 27),
(112, 0, 'nutriente', '1', '1', '1', '1', 1, '2018-02-28 12:58:28', '2018-02-28 12:58:28', 29),
(113, 1, 'Porción', '80 g (1 trozo)', NULL, NULL, NULL, 1, '2018-02-28 12:58:28', '2018-02-28 12:58:28', 29),
(131, 0, 'Energía', '(Kcal)', '123', '98', '', 1, '2018-03-02 00:43:01', '2018-03-02 00:43:01', 8),
(132, 0, 'Proteínas', '(g)', '21,3', '17', '', 1, '2018-03-02 00:43:01', '2018-03-02 00:43:01', 8),
(133, 0, 'Grasa total', '(g)', '4,2', '3,4', '', 1, '2018-03-02 00:43:01', '2018-03-02 00:43:01', 8),
(134, 1, 'Porción', '80 g (1 trozo)', NULL, NULL, NULL, 1, '2018-03-02 00:43:01', '2018-03-02 00:43:01', 8),
(135, 1, 'Porciones por envase:', 'aprox. 11 aprox.', NULL, NULL, NULL, 1, '2018-03-02 00:43:01', '2018-03-02 00:43:01', 8),
(138, 0, 'nutriente', '1', '1', '1', '1', 1, '2018-03-02 00:53:04', '2018-03-02 00:53:04', 31),
(139, 1, 'Porción', '80 g (1 trozo)', NULL, NULL, NULL, 1, '2018-03-02 00:53:04', '2018-03-02 00:53:04', 31),
(140, 0, 'nutriente', '1', '1', '1', '1', 1, '2018-03-02 00:55:23', '2018-03-02 00:55:23', 32),
(141, 1, 'Porción', '80 g (1 trozo)', NULL, NULL, NULL, 1, '2018-03-02 00:55:23', '2018-03-02 00:55:23', 32),
(142, 0, 'nutriente', '1', '1', '1', '1', 1, '2018-03-02 00:56:12', '2018-03-02 00:56:12', 30),
(143, 1, 'Porción', '80 g (1 trozo)', NULL, NULL, NULL, 1, '2018-03-02 00:56:12', '2018-03-02 00:56:12', 30),
(144, 0, 'nutriente', '1', '1', '1', '1', 1, '2018-03-02 00:58:47', '2018-03-02 00:58:47', 33),
(145, 1, 'Porción', '80 g (1 trozo)', NULL, NULL, NULL, 1, '2018-03-02 00:58:47', '2018-03-02 00:58:47', 33),
(150, 0, 'nutriente', '1', '1', '1', '1', 1, '2018-03-02 01:41:26', '2018-03-02 01:41:26', 36),
(151, 1, 'Porción', '80 g (1 trozo)', NULL, NULL, NULL, 1, '2018-03-02 01:41:26', '2018-03-02 01:41:26', 36),
(154, 0, 'nutriente', '1', '1', '1', '1', 1, '2018-03-02 06:02:50', '2018-03-02 06:02:50', 35),
(155, 1, 'Porción', '80 g (1 trozo)', NULL, NULL, NULL, 1, '2018-03-02 06:02:50', '2018-03-02 06:02:50', 35),
(156, 0, 'nutriente', '1', '1', '1', '1', 1, '2018-03-02 06:03:08', '2018-03-02 06:03:08', 37),
(157, 1, 'Porción', '80 g (1 trozo)', NULL, NULL, NULL, 1, '2018-03-02 06:03:08', '2018-03-02 06:03:08', 37),
(158, 0, 'nutriente', '1', '1', '1', '1', 1, '2018-03-02 06:03:57', '2018-03-02 06:03:57', 34),
(159, 1, 'Porción', '80 g (1 trozo)', NULL, NULL, NULL, 1, '2018-03-02 06:03:57', '2018-03-02 06:03:57', 34),
(160, 0, 'nutriente', '1', '1', '1', '1', 1, '2018-03-02 06:04:39', '2018-03-02 06:04:39', 14),
(161, 1, 'Porción', '80 g (1 trozo)', NULL, NULL, NULL, 1, '2018-03-02 06:04:39', '2018-03-02 06:04:39', 14),
(162, 0, 'nutriente', '1', '1', '1', '1', 1, '2018-03-02 06:06:20', '2018-03-02 06:06:20', 38),
(163, 1, 'Porción', '80 g (1 trozo)', NULL, NULL, NULL, 1, '2018-03-02 06:06:20', '2018-03-02 06:06:20', 38),
(164, 0, 'nutriente', '1', '1', '1', '1', 1, '2018-03-02 06:07:27', '2018-03-02 06:07:27', 22),
(165, 1, 'Porción', '80 g (1 trozo)', NULL, NULL, NULL, 1, '2018-03-02 06:07:27', '2018-03-02 06:07:27', 22),
(166, 0, 'nutriente', '1', '1', '1', '1', 1, '2018-03-02 06:10:03', '2018-03-02 06:10:03', 39),
(167, 1, 'Porción', '80 g (1 trozo)', NULL, NULL, NULL, 1, '2018-03-02 06:10:03', '2018-03-02 06:10:03', 39),
(168, 0, 'nutriente', '1', '1', '1', '1', 1, '2018-03-02 06:13:22', '2018-03-02 06:13:22', 40),
(169, 1, 'Porción', '80 g (1 trozo)', NULL, NULL, NULL, 1, '2018-03-02 06:13:22', '2018-03-02 06:13:22', 40);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sc_politicas`
--

CREATE TABLE `sc_politicas` (
  `id` bigint(20) NOT NULL,
  `politica` longtext,
  `activo` tinyint(1) DEFAULT '1',
  `imagen` varchar(200) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modifed` datetime DEFAULT NULL,
  `administrador_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sc_productos`
--

CREATE TABLE `sc_productos` (
  `id` bigint(20) NOT NULL,
  `slug` varchar(200) DEFAULT NULL,
  `nombre` varchar(200) DEFAULT NULL,
  `bajada` longtext,
  `descripcion` longtext,
  `activo` tinyint(1) DEFAULT '1',
  `destacado` tinyint(1) NOT NULL DEFAULT '0',
  `destacado_linea` varchar(20) DEFAULT NULL,
  `imagen` varchar(200) DEFAULT NULL,
  `imagen_banner` varchar(200) DEFAULT NULL,
  `corte_id` bigint(20) DEFAULT NULL,
  `porcion` varchar(45) DEFAULT NULL,
  `porcion_envase` varchar(45) DEFAULT NULL,
  `para_producto` varchar(200) DEFAULT NULL,
  `probar_otro_producto` varchar(200) DEFAULT NULL,
  `recomendar_receta` varchar(200) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `administrador_id` bigint(20) NOT NULL,
  `linea_producto_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sc_productos`
--

INSERT INTO `sc_productos` (`id`, `slug`, `nombre`, `bajada`, `descripcion`, `activo`, `destacado`, `destacado_linea`, `imagen`, `imagen_banner`, `corte_id`, `porcion`, `porcion_envase`, `para_producto`, `probar_otro_producto`, `recomendar_receta`, `created`, `modified`, `administrador_id`, `linea_producto_id`) VALUES
(8, NULL, 'Asiento Extra Magro', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', 'Este es una corte del cerdo ideal para el horno. Sellado en aceite de oliva hasta que este bien dorado. Ponlo sobre una fuente de horno con trocitos de tomates y laminas de tocino picado y un chorrito de vino blanco. Llévalo al horno hasta que este bien cocido y disfruta de esta carne tierna acompañado de arroz primavera.', 1, 1, NULL, 'linea_cocina_asiento.png', 'lineacocina.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-28 10:04:10', '2018-03-02 00:43:00', 3, 1),
(9, NULL, 'Bistec de Cerdo', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 1, 0, NULL, 'linea_cocina_bistec.png', 'linea_cocina_bistec.png', NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-28 11:53:18', '2018-02-28 12:54:43', 3, 1),
(10, NULL, 'Carne Molida', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 1, 0, NULL, 'linea_cocina_carne_molida.png', 'copia_de_olla_3_4.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-28 12:04:29', '2018-02-28 12:55:29', 3, 1),
(11, NULL, 'Cazuela', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 1, 0, NULL, 'linea_cocina_cazuela.png', 'copia_de_olla_3_4.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-28 12:07:11', '2018-02-28 12:53:01', 3, 1),
(12, NULL, 'Chuleta Centro', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 1, 0, NULL, 'linea_cocina_chuleta_centro.png', 'copia_de_olla_3_4.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-28 12:08:19', '2018-02-28 12:08:19', 3, 1),
(13, NULL, 'Chuletitas', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 1, 0, NULL, 'linea_cocina_chuletitas.png', 'copia_de_olla_5_4.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-28 12:10:45', '2018-02-28 12:10:45', 3, 1),
(14, NULL, 'Filete', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 1, 0, NULL, 'linea_sarten_filete.png', 'copia_de_olla_2_4.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-28 12:12:52', '2018-03-02 06:04:39', 3, 1),
(15, NULL, 'Lomo Centro', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 1, 0, NULL, 'linea_cocina_lomo_centro.png', 'copia_de_olla_3_4.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-28 12:13:53', '2018-02-28 12:13:53', 3, 1),
(16, NULL, 'Molida Boloñesa', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ', '<p><span style=\"color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</span><br></p>', 1, 0, NULL, 'linea_cocina_molida_bolonesa.png', 'copia_de_olla_2_4.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-28 12:15:10', '2018-02-28 12:15:10', 3, 1),
(17, NULL, 'Molida Sofrito', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 1, 0, NULL, 'linea_cocina_molida_sofrito.png', 'copia_de_olla_3_4.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-28 12:16:32', '2018-02-28 12:16:32', 3, 1),
(18, NULL, 'Pernil Mano', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nis', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 1, 0, NULL, 'linea_cocina_pernil_mano.png', 'copia_de_olla_3_4.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-28 12:18:37', '2018-02-28 12:18:37', 3, 1),
(19, NULL, 'Posta Negra', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 1, 0, NULL, 'linea_cocina_posta_negra.png', 'copia_de_olla_3_4.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-28 12:19:33', '2018-02-28 12:19:33', 3, 1),
(20, NULL, 'Posta Rosada', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco labori', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 1, 0, NULL, 'linea_cocina_posta_rosada.png', 'copia_de_olla_3_4.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-28 12:20:36', '2018-02-28 12:20:36', 3, 1),
(21, NULL, 'Pulpa Paleta', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 1, 0, NULL, 'linea_cocina_pulpa_paleta.png', 'copia_de_olla_3_4.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-28 12:23:26', '2018-02-28 12:23:26', 3, 1),
(22, NULL, 'Pulpa pierna', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 1, 0, NULL, 'linea_sarten_pulpa_pierna.png', 'copia_de_olla_3_4.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-28 12:24:21', '2018-03-02 06:07:26', 3, 1),
(23, NULL, 'Lomo Vetado', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 1, 0, NULL, 'linea_parrilla__lomo_vetado.png', 'copia_de_olla_5_4.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-28 12:26:00', '2018-02-28 12:26:00', 3, 3),
(24, NULL, 'Baby Back Ribs', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 1, 1, NULL, 'linea_parrilla_baby_back_ribs.png', 'copia_de_olla_3_4.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-28 12:28:24', '2018-02-28 12:53:54', 3, 3),
(25, NULL, 'Baby Back Ribs', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 1, 0, NULL, 'linea_parrilla_bbr.png', 'copia_de_olla_5_4.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-28 12:30:17', '2018-02-28 12:30:17', 3, 3),
(26, NULL, 'Costillar a la Chilena', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 1, 0, NULL, 'linea_parrilla_costillar_a_la_chilena.png', 'copia_de_olla_5_4.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-28 12:31:11', '2018-02-28 12:57:16', 3, 3),
(27, NULL, 'Costillar Barbecue', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 1, 0, NULL, 'linea_parrilla_costillar_bbq.png', 'copia_de_olla_2_4.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-28 12:32:23', '2018-02-28 12:57:47', 3, 3),
(28, NULL, 'Costillar Deshuesado', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 1, 0, NULL, 'linea_parrilla_costillar_deshuesado.png', 'copia_de_olla_5_4.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-28 12:33:29', '2018-02-28 12:33:29', 3, 3),
(29, NULL, 'Costillar Sureño', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 1, 0, NULL, 'linea_parrilla_costillar_sureno.png', 'copia_de_olla_5_4.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-28 12:36:46', '2018-02-28 12:58:28', 3, 3),
(30, NULL, 'Jamón Acaramelado', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturien vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. ', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. ', 1, 1, NULL, 'linea_sandwich_jamon_acaramelado.png', 'linea_sandwich_jamon_acaramelado.png', NULL, NULL, NULL, NULL, NULL, NULL, '2018-03-02 00:36:24', '2018-03-02 00:56:12', 3, 2),
(31, NULL, 'Jamón Ahumado', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. ', 1, 0, NULL, 'linea_sandwich_jamon_ahumado.png', 'lineasandwich.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-03-02 00:50:54', '2018-03-02 00:53:01', 3, 2),
(32, NULL, 'Jamón Pierna', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. ', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. ', 1, 0, NULL, 'linea_sandwich_jamon_pierna.png', 'lineasandwich.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-03-02 00:55:19', '2018-03-02 00:55:19', 3, 2),
(33, NULL, 'Jamón Sandwich', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolorsto, rhoncus ut, imperdiet a, venenatis vitae, justo. ', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. ', 1, 0, NULL, 'linea_sandwich_jamon_sandwich.png', 'lineasandwich.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-03-02 00:58:44', '2018-03-02 00:58:44', 3, 2),
(34, NULL, 'Mortadela', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis ncus ut, imperdiet a, venenatis vitae, justo. ', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. ', 1, 0, NULL, 'linea_sandwich_mortadela.png', 'lineasandwich.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-03-02 01:33:30', '2018-03-02 06:03:57', 3, 2),
(35, NULL, 'Abastero', 'Lorem ipsum dolor sit amet, congilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. ', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. ', 1, 0, NULL, 'linea_sarten_abastero.png', 'lineasandwich.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-03-02 01:35:14', '2018-03-02 06:02:50', 3, 1),
(36, NULL, 'Asado Carnicero', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam ', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. ', 1, 0, NULL, 'linea_sarten_asado_carnicero.png', 'lineasandwich.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-03-02 01:41:22', '2018-03-02 01:41:22', 3, 2),
(37, NULL, 'Chuleta Centro', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec q', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. ', 1, 0, NULL, 'linea_sarten_chuleta_centro.png', 'lineasandwich.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-03-02 06:01:14', '2018-03-02 06:03:08', 3, 1),
(38, NULL, 'Ganso', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec qto, rhoncus ut, imperdiet a, venenatis vitae, justo. ', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. ', 1, 0, NULL, 'linea_sarten_ganso.png', 'lineacocina.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-03-02 06:06:18', '2018-03-02 06:06:18', 3, 1),
(39, NULL, 'Lomo Vetado porcionado', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus ', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. ', 1, 0, NULL, 'linea_sarten_lomo_vetado_porcionado.png', 'lineacocina.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-03-02 06:10:01', '2018-03-02 06:10:01', 3, 1),
(40, NULL, 'Trocitos', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibu enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. ', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. ', 1, 0, NULL, 'linea_sarten_trocitos.png', 'lineacocina.jpg', NULL, NULL, NULL, NULL, NULL, NULL, '2018-03-02 06:13:20', '2018-03-02 06:13:20', 3, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sc_recetas`
--

CREATE TABLE `sc_recetas` (
  `id` bigint(20) NOT NULL,
  `nombre` varchar(200) DEFAULT NULL,
  `bajada` longtext,
  `imagen` varchar(200) DEFAULT NULL,
  `imagen_banner` varchar(200) DEFAULT NULL,
  `activo` tinyint(1) DEFAULT '1',
  `destacado` tinyint(1) NOT NULL DEFAULT '0',
  `tiempo_preparacion` varchar(45) DEFAULT NULL,
  `porciones` varchar(45) DEFAULT NULL,
  `calorias_porcion` varchar(45) DEFAULT NULL,
  `nivel_dificultad` varchar(45) DEFAULT NULL,
  `preparacion` longtext,
  `recomendar_receta` varchar(200) DEFAULT NULL,
  `administrador_id` bigint(20) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sc_recetas`
--

INSERT INTO `sc_recetas` (`id`, `nombre`, `bajada`, `imagen`, `imagen_banner`, `activo`, `destacado`, `tiempo_preparacion`, `porciones`, `calorias_porcion`, `nivel_dificultad`, `preparacion`, `recomendar_receta`, `administrador_id`, `created`, `modified`) VALUES
(1, 'Costillitas Ribs, Expresso', '', 'receta_1.jpg', 'supercerdorecetas.jpg', 1, 1, '', '4', '', '0', 'Combinar en un bowl, aceite de oliva, ajo molido, vinagre de manzanas, salsa de soya, ketchup, miel y café expresso.Marinar las costillitas con estos jugos y reservar refrigerado  y tapado, por 2 horas .Al cabo de este tiempo, salpicar las costillitas con pimienta y cocinar en la parrilla , , empezando por la parte del hueso, pincelando frecuentemente con la marinada durante 25 minutos. Luego voltear con la carne hacia abajo y cocinar otros 15 minutos hasta que la carne esté dorada y cocida.Servir de inmediato, acompañándolo con puré o ensaladas.', NULL, 3, '2018-02-27 11:44:50', '2018-03-02 00:47:42'),
(2, 'Posta rosada arvejada', '', 'receta_2.jpg', 'olla.jpg', 1, 0, '', '4', '', '1', 'Precalienta el horno a temperatura media–alta. En una fuente para horno, coloca la posta rosada SuperCerdo y rocía con el aceite de oliva. Sazona con sal y pimienta, y cocina por 15 minutos. Baja la temperatura del horno a media y agrega la salsa de tomates mezclada con el caldo de verduras, las arvejas y zanahorias. Añade las hojas de laurel y cocina 30 minutos o hasta que la carne y las verduras estén cocidas. Sirve.', '1', 3, '2018-02-27 11:52:05', '2018-02-27 11:52:05'),
(3, 'Asiento de cerdo al jugo con verduras', '', 'olla.jpg', 'olla_2.jpg', 1, 0, '40', '', '', '0', 'En una olla grande freír en aceite de oliva el trozo de Asiento e incorporar los zuchinis junto con las berenjenas y los pimientos cortados en cubos. Agregar el agua o caldo de verduras, sal y pimienta y cocinar por unos 40 minutos.Servir acompañando de lechugas frescas.', '1,2', 3, '2018-02-27 11:53:55', '2018-02-27 11:53:55'),
(4, 'Costillitas Ribs, Expresso', '', 'receta_3.jpg', 'banner_principal.jpg', 1, 0, '15', '', '', '0', '<p style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 14px; line-height: 22px; font-family: Arial, Helvetica, sans-serif; color: rgb(0, 0, 0); text-align: justify;\">Combinar en un bowl, aceite de oliva, ajo molido, vinagre de manzanas, salsa de soya, ketchup, miel y café expresso.</p><p style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 14px; line-height: 22px; font-family: Arial, Helvetica, sans-serif; color: rgb(0, 0, 0); text-align: justify;\">Marinar las costillitas con estos jugos y reservar refrigerado&nbsp; y tapado, por 2 horas .</p><p style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 14px; line-height: 22px; font-family: Arial, Helvetica, sans-serif; color: rgb(0, 0, 0); text-align: justify;\">Al cabo de este tiempo, salpicar las costillitas con pimienta y cocinar en la parrilla , , empezando por la parte del hueso, pincelando frecuentemente con la marinada durante 25 minutos.</p><p style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 14px; line-height: 22px; font-family: Arial, Helvetica, sans-serif; color: rgb(0, 0, 0); text-align: justify;\">&nbsp;Luego voltear con la carne hacia abajo y cocinar otros 15 minutos hasta que la carne esté dorada y cocida.</p><p style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 14px; line-height: 22px; font-family: Arial, Helvetica, sans-serif; color: rgb(0, 0, 0); text-align: justify;\">Servir de inmediato, acompañándolo con puré o ensaladas.</p><div><br></div>', NULL, 3, '2018-02-28 13:34:32', '2018-02-28 13:35:12'),
(5, 'Costillitas Ribs de Cerdo con tapenade de maní', 'Colocar las costillitas en una fuente y marinar  en la salsa de soya por 1 hora en el refrigerador.', 'receta_4.jpg', 'banner_principal.jpg', 1, 0, '', '', '', '0', '<p style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 14px; line-height: 22px; font-family: Arial, Helvetica, sans-serif; color: rgb(0, 0, 0); text-align: justify;\">Colocar las costillitas en una fuente y marinar&nbsp; en la salsa de soya por 1 hora en el refrigerador.</p><p style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 14px; line-height: 22px; font-family: Arial, Helvetica, sans-serif; color: rgb(0, 0, 0); text-align: justify;\">Moler juntos el maní, y cebollín y agregar el yogur , y el curry formando una pasta .</p><p style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 14px; line-height: 22px; font-family: Arial, Helvetica, sans-serif; color: rgb(0, 0, 0); text-align: justify;\">Calentar una plancha de cocina o parrilla, a fuego fuerte&nbsp; y cocinar las costillitas&nbsp; 10 minutos por cada lado.</p><p style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 14px; line-height: 22px; font-family: Arial, Helvetica, sans-serif; color: rgb(0, 0, 0); text-align: justify;\">Untar con salsa de maní y servir de inmediato.</p>', NULL, 3, '2018-02-28 13:36:09', '2018-02-28 13:36:09'),
(6, 'Posta rosada arvejada', '', 'receta_1.jpg', '53636wide.jpg', 1, 0, '45', '', '', '0', 'Precalienta el horno a temperatura media–alta. En una fuente para horno, coloca la posta rosada SuperCerdo y rocía con el aceite de oliva. Sazona con sal y pimienta, y cocina por 15 minutos. Baja la temperatura del horno a media y agrega la salsa de tomates mezclada con el caldo de verduras, las arvejas y zanahorias. Añade las hojas de laurel y cocina 30 minutos o hasta que la carne y las verduras estén cocidas. Sirve.', NULL, 3, '2018-02-28 17:12:26', '2018-02-28 17:26:06'),
(7, 'Baby Back Ribs y salsa de cilantro', '', 'baby_back_ribs_y_salsa_de_cilantro.jpg', '53636wide.jpg', 1, 0, '80', '4', '', '0', 'salpimentar las costillas y bañar  con salsa inglesa, soya, y jugo de limón.Marinar por 1 hora .Calentar la parrilla a fuego medio fuerte y coinar las costilas partiendo por el lado del hueso, 15 a 20 minutos por cada lado hasta que la carne esté dorada y crocante.Para la salsa: mezclar en un bowl, el cilantro, crema ácida, mayonesa, jugo de limón. Salpimentar y servir de inmediato acompañando las costillitas calientes.', NULL, 3, '2018-02-28 17:21:24', '2018-02-28 17:21:24'),
(8, 'Costillitas Baby Back Ribs a la menta', '', 'costillitas_baby_back_ribs_a_la_menta..jpg', '53636wide.jpg', 1, 0, '30', '6', '', '0', 'Preparar una pasta mezclando menta, cebolla molida, jugo de limón y aceite de maravilla. Salpimentar las costillitas y untar bien con la salsa de menta por ambos lados.Calentar una parrilla o plancha de cocina a fuego medio fuerte y cocinar las costillitas por el lado del hueso primero durante unos 15 minutos.Voltear y cocinar  15 minutos más por el lado de la carne, untando las costillitas con la menta ocasionalmente.Cuando las costillitas estén totalmente doradas y cocidas, retirar del fuego, salpicar con menta fresca extra y servir de inmediato solas o acompañadas de ensaladas.Sirven como aperitivo cortando costillitas individuales o al plato.', NULL, 3, '2018-02-28 17:23:02', '2018-02-28 17:23:02'),
(9, 'Posta Rosada de cerdo al horno', ' Cocinar por  45 minutos hasta que la carne este lista.', 'magro_posta_rosada_575x498.jpg', '53636wide.jpg', 1, 0, '30', '1', '', '0', 'En un sartén dorar por ambos lados la Posta Rosada. Luego colocar la carne en una bandeja para horno junto con los tomates y las cebollas moradas cortados en mitades. Agregar sal y pimienta y el tomillo sobre la carne y demás ingredientes. Cocinar por  45 minutos hasta que la carne este lista.', NULL, 3, '2018-02-28 17:24:30', '2018-02-28 17:24:30'),
(10, 'Plateada de lomo con callampas, tipo vietnam', '', 'plateada_de_lomo_asada_en_vinagre_de_manzanas_y_salsa_tartara1_575x498.jpg', '53636wide.jpg', 1, 0, '120', '6', '', '0', 'En un bowl con agua caliente hidratar las callampas secas por 5 minutos hasta que estén blandas.  Estilar y cortar en tiritas medianas. Reservar .Cortar cada  plateada de lomo en tres  trozos medianos.Calentar el aceite en un sartén grande y dorar los trozos de plateada por ambos lados 3 minutos, empezando por  el lado de mayor grasa.Retirar el exceso de aceite del sartén y volver a colocarlo en el fuego. Ahora añadir la salsa de soya, el vino blanco, azúcar ,  la cáscara de naranjas y las callampas secas.Tapar  el sartén y cocinar a fuego medio por 20 minutos, volteando de vez en cuando la carne de cerdo.Cuidar que no se seque por completo, añadiendo pequeños chorritos de agua cuando sea necesario.Una vez listo,  servir acompañado de arroz o ensaladas.', NULL, 3, '2018-02-28 17:28:15', '2018-02-28 17:28:15');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sc_recomendaciones`
--

CREATE TABLE `sc_recomendaciones` (
  `id` bigint(20) NOT NULL,
  `corte_id` bigint(20) NOT NULL,
  `activo` tinyint(1) DEFAULT '1',
  `tipo_recomendacion` varchar(45) DEFAULT NULL,
  `producto_id` bigint(20) NOT NULL,
  `tipo_recomendacion_id` bigint(20) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sc_recomendacion_recetas`
--

CREATE TABLE `sc_recomendacion_recetas` (
  `id` bigint(20) NOT NULL,
  `receta_id` bigint(20) NOT NULL,
  `receta_recomendada_id` bigint(20) NOT NULL,
  `activo` tinyint(1) DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sc_rol_administradores`
--

CREATE TABLE `sc_rol_administradores` (
  `id` bigint(20) NOT NULL,
  `rol` varchar(200) DEFAULT NULL,
  `activo` tinyint(1) DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sc_seos`
--

CREATE TABLE `sc_seos` (
  `id` bigint(20) NOT NULL,
  `modulo` varchar(200) NOT NULL,
  `modulo_id` varchar(20) NOT NULL,
  `activo` tinyint(1) DEFAULT '1',
  `titulo_seo` varchar(200) DEFAULT NULL,
  `descripcion` longtext,
  `palabras` varchar(200) DEFAULT NULL,
  `slug` varchar(200) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sc_seos`
--

INSERT INTO `sc_seos` (`id`, `modulo`, `modulo_id`, `activo`, `titulo_seo`, `descripcion`, `palabras`, `slug`, `created`, `modified`) VALUES
(1, 'Producto', '1', 1, 'Costillitas Ribs Barbecue', 'Prueba todo nuestro sabor con estas deliciosas Costillitas Ribs Barbecue. Lo mejor para disfrutar ahora a la parrilla. Puedes acompaÃ±ar de papas cocidas, purÃ©, arroz, ensaladas, o lo que se te ocurra.', 'Costillitas Ribs Barbecue', 'Costillitas_Ribs_Barbecue', '2018-02-26 01:16:26', '2018-02-27 01:48:52'),
(2, 'Producto', '2', 1, '', '', '', '', '2018-02-27 02:05:01', '2018-02-27 13:02:46'),
(3, 'Producto', '3', 1, '', '', '', '', '2018-02-27 06:37:11', '2018-02-27 06:37:11'),
(4, 'Producto', '4', 1, '', '', '', '', '2018-02-27 06:37:45', '2018-02-27 06:37:45'),
(5, 'Producto', '5', 1, '', '', '', '', '2018-02-27 06:38:27', '2018-02-27 06:38:27'),
(6, 'Producto', '6', 1, '', '', '', '', '2018-02-27 06:39:04', '2018-02-27 06:39:04'),
(7, 'Producto', '7', 1, '', '', '', '', '2018-02-27 06:46:52', '2018-02-27 06:46:52'),
(8, 'Receta', '1', 1, '', '', '', '', '2018-02-27 11:44:50', '2018-03-02 00:47:42'),
(9, 'Receta', '2', 1, '', '', '', '', '2018-02-27 11:52:10', '2018-02-27 11:52:10'),
(10, 'Receta', '3', 1, '', '', '', '', '2018-02-27 11:54:05', '2018-02-27 11:54:05'),
(11, 'Producto', '8', 1, 'Asiento Extra Magro', 'Este es una corte del cerdo ideal para el horno. Sellado en aceite de oliva hasta que este bien dorado. Ponlo sobre una fuente de horno con trocitos de tomates y laminas de tocino picado y un chorrito de vino blanco. Llévalo al horno hasta que este bien cocido y disfruta de esta carne tierna acompañado de arroz primavera.', 'Asiento Extra Magro', '', '2018-02-28 10:04:13', '2018-03-02 00:43:01'),
(12, 'Producto', '9', 1, '', '', '', '', '2018-02-28 11:53:19', '2018-02-28 12:54:43'),
(13, 'Producto', '10', 1, '', '', '', '', '2018-02-28 12:04:31', '2018-02-28 12:55:29'),
(14, 'Producto', '11', 1, '', '', '', '', '2018-02-28 12:07:13', '2018-02-28 12:53:01'),
(15, 'Producto', '12', 1, '', '', '', '', '2018-02-28 12:08:21', '2018-02-28 12:08:21'),
(16, 'Producto', '13', 1, '', '', '', '', '2018-02-28 12:10:46', '2018-02-28 12:10:46'),
(17, 'Producto', '14', 1, '', '', '', '', '2018-02-28 12:12:54', '2018-03-02 06:04:39'),
(18, 'Producto', '15', 1, '', '', '', '', '2018-02-28 12:13:55', '2018-02-28 12:13:55'),
(19, 'Producto', '16', 1, '', '', '', '', '2018-02-28 12:15:12', '2018-02-28 12:15:12'),
(20, 'Producto', '17', 1, '', '', '', '', '2018-02-28 12:16:34', '2018-02-28 12:16:34'),
(21, 'Producto', '18', 1, '', '', '', '', '2018-02-28 12:18:39', '2018-02-28 12:18:39'),
(22, 'Producto', '19', 1, '', '', '', '', '2018-02-28 12:19:35', '2018-02-28 12:19:35'),
(23, 'Producto', '20', 1, '', '', '', '', '2018-02-28 12:20:38', '2018-02-28 12:20:38'),
(24, 'Producto', '21', 1, '', '', '', '', '2018-02-28 12:23:28', '2018-02-28 12:23:28'),
(25, 'Producto', '22', 1, '', '', '', '', '2018-02-28 12:24:23', '2018-03-02 06:07:27'),
(26, 'Producto', '23', 1, '', '', '', '', '2018-02-28 12:26:01', '2018-02-28 12:26:01'),
(27, 'Producto', '24', 1, '', '', '', '', '2018-02-28 12:28:26', '2018-02-28 12:28:26'),
(28, 'Producto', '25', 1, '', '', '', '', '2018-02-28 12:30:17', '2018-02-28 12:30:17'),
(29, 'Producto', '26', 1, '', '', '', '', '2018-02-28 12:31:11', '2018-02-28 12:57:16'),
(30, 'Producto', '27', 1, '', '', '', '', '2018-02-28 12:32:25', '2018-02-28 12:57:47'),
(31, 'Producto', '28', 1, '', '', '', '', '2018-02-28 12:33:30', '2018-02-28 12:33:30'),
(32, 'Producto', '29', 1, '', '', '', '', '2018-02-28 12:36:47', '2018-02-28 12:58:28'),
(33, 'Noticia', '1', 1, '', '', '', '', '2018-02-28 13:31:47', '2018-02-28 13:31:47'),
(34, 'Receta', '4', 1, '', '', '', '', '2018-02-28 13:34:32', '2018-02-28 13:35:13'),
(35, 'Receta', '5', 1, '', '', '', '', '2018-02-28 13:36:09', '2018-02-28 13:36:09'),
(36, 'Noticia', '2', 1, '', '', '', '', '2018-02-28 13:38:26', '2018-02-28 13:38:26'),
(37, 'Noticia', '3', 1, '', '', '', '', '2018-02-28 13:39:42', '2018-02-28 13:39:42'),
(38, 'Receta', '6', 1, '', '', '', '', '2018-02-28 17:12:26', '2018-02-28 17:26:06'),
(39, 'Receta', '7', 1, '', '', '', '', '2018-02-28 17:21:24', '2018-02-28 17:21:24'),
(40, 'Receta', '8', 1, '', '', '', '', '2018-02-28 17:23:02', '2018-02-28 17:23:02'),
(41, 'Receta', '9', 1, '', '', '', '', '2018-02-28 17:24:30', '2018-02-28 17:24:30'),
(42, 'Receta', '10', 1, '', '', '', '', '2018-02-28 17:28:15', '2018-02-28 17:28:15'),
(43, 'Noticia', '4', 1, '', '', '', '', '2018-02-28 17:32:37', '2018-02-28 17:32:37'),
(44, 'Noticia', '5', 1, '', '', '', '', '2018-02-28 17:41:05', '2018-02-28 17:41:05'),
(45, 'Noticia', '6', 1, '', '', '', '', '2018-02-28 17:41:58', '2018-02-28 17:41:58'),
(46, 'Producto', '30', 1, '', '', '', '', '2018-03-02 00:36:25', '2018-03-02 00:56:12'),
(47, 'Producto', '31', 1, '', '', '', '', '2018-03-02 00:50:55', '2018-03-02 00:53:04'),
(48, 'Producto', '32', 1, '', '', '', '', '2018-03-02 00:55:23', '2018-03-02 00:55:23'),
(49, 'Producto', '33', 1, '', '', '', '', '2018-03-02 00:58:47', '2018-03-02 00:58:47'),
(50, 'Producto', '34', 1, '', '', '', '', '2018-03-02 01:33:33', '2018-03-02 06:03:57'),
(51, 'Producto', '35', 1, '', '', '', '', '2018-03-02 01:35:17', '2018-03-02 06:02:50'),
(52, 'Producto', '36', 1, '', '', '', '', '2018-03-02 01:41:26', '2018-03-02 01:41:26'),
(53, 'Producto', '37', 1, '', '', '', '', '2018-03-02 06:01:17', '2018-03-02 06:03:08'),
(54, 'Producto', '38', 1, '', '', '', '', '2018-03-02 06:06:20', '2018-03-02 06:06:20'),
(55, 'Producto', '39', 1, '', '', '', '', '2018-03-02 06:10:03', '2018-03-02 06:10:03'),
(56, 'Producto', '40', 1, '', '', '', '', '2018-03-02 06:13:22', '2018-03-02 06:13:22');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sc_tipo_recomendaciones`
--

CREATE TABLE `sc_tipo_recomendaciones` (
  `id` bigint(20) NOT NULL,
  `nombre` varchar(200) DEFAULT NULL,
  `imagen` varchar(200) DEFAULT NULL,
  `activo` tinyint(1) DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `administrador_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sc_tips`
--

CREATE TABLE `sc_tips` (
  `id` bigint(20) NOT NULL,
  `nombre` varchar(200) DEFAULT NULL,
  `descripcion` longtext,
  `activo` tinyint(1) DEFAULT '1',
  `destacado` tinyint(1) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `adminitrador_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sc_valoraciones`
--

CREATE TABLE `sc_valoraciones` (
  `id` bigint(20) NOT NULL,
  `activo` tinyint(1) DEFAULT '1',
  `producto_id` bigint(20) NOT NULL,
  `ip_server` varchar(200) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `administrador_id` bigint(20) NOT NULL,
  `receta_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `sc_administradores`
--
ALTER TABLE `sc_administradores`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_sc_administradores_sc_rol_administradores_idx` (`rol_id`);

--
-- Indices de la tabla `sc_banners`
--
ALTER TABLE `sc_banners`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_sc_banners_sc_administradores1_idx` (`administrador_id`);

--
-- Indices de la tabla `sc_campania`
--
ALTER TABLE `sc_campania`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_sc_campania_sc_administradores1_idx` (`administrador_id`);

--
-- Indices de la tabla `sc_configuraciones`
--
ALTER TABLE `sc_configuraciones`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_sc_configuraciones_sc_administradores1_idx` (`adminitrador_id`);

--
-- Indices de la tabla `sc_contactos`
--
ALTER TABLE `sc_contactos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `sc_cortes`
--
ALTER TABLE `sc_cortes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `sc_cortes_recetas`
--
ALTER TABLE `sc_cortes_recetas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_sc_cortes_recetas_sc_cortes1_idx` (`corte_id`),
  ADD KEY `fk_sc_cortes_recetas_sc_receta1_idx` (`sc_receta_id`);

--
-- Indices de la tabla `sc_ingrediente_recetas`
--
ALTER TABLE `sc_ingrediente_recetas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_sc_ingrediente_recetas_sc_receta1_idx` (`receta_id`);

--
-- Indices de la tabla `sc_linea_productos`
--
ALTER TABLE `sc_linea_productos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_sc_categoria_productos_sc_administradores1_idx` (`administrador_id`);

--
-- Indices de la tabla `sc_marcas`
--
ALTER TABLE `sc_marcas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_sc_marcas_sc_administradores1_idx` (`administrador_id`);

--
-- Indices de la tabla `sc_nosotros`
--
ALTER TABLE `sc_nosotros`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_ssc_nosotros_sc_administradores1_idx` (`administrador_id`);

--
-- Indices de la tabla `sc_noticias`
--
ALTER TABLE `sc_noticias`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_sc_noticias_sc_administradores1_idx` (`administrador_id`);

--
-- Indices de la tabla `sc_nutricional_productos`
--
ALTER TABLE `sc_nutricional_productos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_sc_nutricional_productos_sc_productos1_idx` (`producto_id`);

--
-- Indices de la tabla `sc_politicas`
--
ALTER TABLE `sc_politicas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_sc_politicas_sc_administradores1_idx` (`administrador_id`);

--
-- Indices de la tabla `sc_productos`
--
ALTER TABLE `sc_productos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_sc_productos_sc_cortes1_idx` (`corte_id`),
  ADD KEY `fk_sc_productos_sc_administradores1_idx` (`administrador_id`),
  ADD KEY `fk_sc_prodcutos_sc_linea_prodcutos_idx` (`nombre`),
  ADD KEY `fk_sc_productos_sc_linea_productos_idx` (`linea_producto_id`);

--
-- Indices de la tabla `sc_recetas`
--
ALTER TABLE `sc_recetas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_sc_receta_sc_administradores1_idx` (`administrador_id`);

--
-- Indices de la tabla `sc_recomendaciones`
--
ALTER TABLE `sc_recomendaciones`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_sc_recomendado_recetas_sc_cortes1_idx` (`corte_id`),
  ADD KEY `fk_sc_recomendaciones_sc_productos1_idx` (`producto_id`),
  ADD KEY `fk_sc_recomendaciones_sc_tipo_recomendaciones1_idx` (`tipo_recomendacion_id`);

--
-- Indices de la tabla `sc_recomendacion_recetas`
--
ALTER TABLE `sc_recomendacion_recetas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_sc_recomendacion_recetas_sc_recetas1_idx` (`receta_id`),
  ADD KEY `fk_sc_recomendacion_recetas_sc_recetas2_idx` (`receta_recomendada_id`);

--
-- Indices de la tabla `sc_rol_administradores`
--
ALTER TABLE `sc_rol_administradores`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `sc_seos`
--
ALTER TABLE `sc_seos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `sc_tipo_recomendaciones`
--
ALTER TABLE `sc_tipo_recomendaciones`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_sc_tipo_recomendaciones_sc_administradores1_idx` (`administrador_id`);

--
-- Indices de la tabla `sc_tips`
--
ALTER TABLE `sc_tips`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_sc_tips_sc_administradores1_idx` (`adminitrador_id`);

--
-- Indices de la tabla `sc_valoraciones`
--
ALTER TABLE `sc_valoraciones`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_sc_valoracion_productos_sc_producto1_idx` (`producto_id`),
  ADD KEY `fk_sc_valoracion_productos_sc_administradores1_idx` (`administrador_id`),
  ADD KEY `fk_sc_valoracion_productos_sc_receta1_idx` (`receta_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `sc_administradores`
--
ALTER TABLE `sc_administradores`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `sc_banners`
--
ALTER TABLE `sc_banners`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `sc_campania`
--
ALTER TABLE `sc_campania`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `sc_configuraciones`
--
ALTER TABLE `sc_configuraciones`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `sc_contactos`
--
ALTER TABLE `sc_contactos`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `sc_cortes`
--
ALTER TABLE `sc_cortes`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `sc_cortes_recetas`
--
ALTER TABLE `sc_cortes_recetas`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `sc_ingrediente_recetas`
--
ALTER TABLE `sc_ingrediente_recetas`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT de la tabla `sc_linea_productos`
--
ALTER TABLE `sc_linea_productos`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `sc_marcas`
--
ALTER TABLE `sc_marcas`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `sc_nosotros`
--
ALTER TABLE `sc_nosotros`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `sc_noticias`
--
ALTER TABLE `sc_noticias`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `sc_nutricional_productos`
--
ALTER TABLE `sc_nutricional_productos`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=170;
--
-- AUTO_INCREMENT de la tabla `sc_politicas`
--
ALTER TABLE `sc_politicas`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `sc_productos`
--
ALTER TABLE `sc_productos`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT de la tabla `sc_recetas`
--
ALTER TABLE `sc_recetas`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT de la tabla `sc_recomendaciones`
--
ALTER TABLE `sc_recomendaciones`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `sc_recomendacion_recetas`
--
ALTER TABLE `sc_recomendacion_recetas`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `sc_rol_administradores`
--
ALTER TABLE `sc_rol_administradores`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `sc_seos`
--
ALTER TABLE `sc_seos`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;
--
-- AUTO_INCREMENT de la tabla `sc_tipo_recomendaciones`
--
ALTER TABLE `sc_tipo_recomendaciones`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `sc_tips`
--
ALTER TABLE `sc_tips`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `sc_valoraciones`
--
ALTER TABLE `sc_valoraciones`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `sc_administradores`
--
ALTER TABLE `sc_administradores`
  ADD CONSTRAINT `fk_sc_administradores_sc_rol_administradores` FOREIGN KEY (`rol_id`) REFERENCES `sc_rol_administradores` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `sc_banners`
--
ALTER TABLE `sc_banners`
  ADD CONSTRAINT `fk_sc_banners_sc_administradores1` FOREIGN KEY (`administrador_id`) REFERENCES `sc_administradores` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `sc_campania`
--
ALTER TABLE `sc_campania`
  ADD CONSTRAINT `fk_sc_campania_sc_administradores1` FOREIGN KEY (`administrador_id`) REFERENCES `sc_administradores` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `sc_configuraciones`
--
ALTER TABLE `sc_configuraciones`
  ADD CONSTRAINT `fk_sc_configuraciones_sc_administradores1` FOREIGN KEY (`adminitrador_id`) REFERENCES `sc_administradores` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `sc_cortes_recetas`
--
ALTER TABLE `sc_cortes_recetas`
  ADD CONSTRAINT `fk_sc_cortes_recetas_sc_cortes1` FOREIGN KEY (`corte_id`) REFERENCES `sc_cortes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_sc_cortes_recetas_sc_receta1` FOREIGN KEY (`sc_receta_id`) REFERENCES `sc_recetas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `sc_linea_productos`
--
ALTER TABLE `sc_linea_productos`
  ADD CONSTRAINT `fk_sc_categoria_productos_sc_administradores1` FOREIGN KEY (`administrador_id`) REFERENCES `sc_administradores` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `sc_marcas`
--
ALTER TABLE `sc_marcas`
  ADD CONSTRAINT `fk_sc_marcas_sc_administradores1` FOREIGN KEY (`administrador_id`) REFERENCES `sc_administradores` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `sc_nosotros`
--
ALTER TABLE `sc_nosotros`
  ADD CONSTRAINT `fk_ssc_nosotros_sc_administradores1` FOREIGN KEY (`administrador_id`) REFERENCES `sc_administradores` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `sc_noticias`
--
ALTER TABLE `sc_noticias`
  ADD CONSTRAINT `fk_sc_noticias_sc_administradores1` FOREIGN KEY (`administrador_id`) REFERENCES `sc_administradores` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `sc_politicas`
--
ALTER TABLE `sc_politicas`
  ADD CONSTRAINT `fk_sc_politicas_sc_administradores1` FOREIGN KEY (`administrador_id`) REFERENCES `sc_administradores` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
