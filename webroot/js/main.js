'use strict';

jQuery.extend({
	
	errorValidacion : function($container, $item, mensaje) {
		
		notyfy({
			type			: 'error',
			text			: mensaje,
			dismissQueue	: true,
			timeout			: 6000,
			closeWith		: ['click'],
		});

		if ($item && $item.length) {
			$item.focus();
		}

		return false;

	}

});

jQuery(document).ready(function($) {

	$('.select-corte').on('click', function(){
		var id_corte = $(this).attr('id-corte');
		$('.img_corte').hide();
		$('.corte_cerdo_'+id_corte).removeClass('no-visible');
		$('.corte_cerdo_'+id_corte).show();

		$('.corte-div-info').hide();
		$('.corte-info-'+id_corte).removeClass('no-visible');
		$('.corte-info-'+id_corte).show();

	});


	
	var $form_container		= $('.formulario-contacto'),
		$form				= $('form', $form_container);

	if (typeof validation_error === 'boolean' && validation_error) {
		$.errorValidacion(null, null, validation_message);
	}

	if (typeof form_sent === 'boolean' && form_sent) {
		notyfy({
			type			: 'success',
			text			: form_message,
			dismissQueue	: true,
			timeout			: 6000,
			closeWith		: ['click'],
		});
	}

	if ($form.length) {
		
		$('[name$="[nombre]"]', $form).alphanumeric({ allow: ' .-\'"' });
		$('[name$="[apellido]"]', $form).alphanumeric({ allow: ' .-\'"' });
		//$('[name$="[telefono]"]', $form).mask('9 99999999', { placeholder: 'x xxxxxxxx' });
		$('[name$="[email]"]', $form).alphanumeric({ allow: '+-_~.@' });

		$form.validate({

			showErrors		: function() {return false;},

			invalidHandler	: function(evento, validador) {
				return $.errorValidacion($form_container, $(validador.errorList[0].element), validador.errorList[0].message);
			},

			submitHandler	: function(form) { if ( ! $(form).valid() ) { return false; } else { form.submit(); } },

			rules			: {
				'data[Lead][nombre]': {
					required		: true,
					maxlength		: 30
				},
				'data[Lead][apellido]': {
					required		: true,
					maxlength		: 30
				},
				'data[Lead][telefono]': {
					required		: true,
					maxlength		: 30
				},
				'data[Lead][email]': {
					required		: true,
					email			: true,
					maxlength		: 150
				},
				'data[Lead][asunto]': {
					required		: true,
					maxlength		: 150
				},
				'data[Lead][mensaje]': {
					required		: true,
					maxlength		: 300
				}
			},

			messages		: {

				'data[Lead][nombre]': {
					required		: 'Debes ingresar tu nombre.',
					maxlength		: $.validator.format('Tu nombre debe tener un máximo de {0} caracteres.')
				},
				'data[Lead][apellido]': {
					required		: 'Debes ingresar tu apellido.',
					maxlength		: $.validator.format('Tu apellido debe tener un máximo de {0} caracteres.')
				},
				'data[Lead][telefono]': {
					required		: 'Debes ingresar tu número de teléfono.',
					maxlength		: $.validator.format('Tu teléfono debe tener un máximo de {0} caracteres.')
				},
				'data[Lead][email]': {
					required		: 'Debes ingresar tu email.',
					email			: 'Debes ingresar un email válido.',
					maxlength		: $.validator.format('Tu email debe tener un máximo de {0} caracteres.')
				},
				'data[Lead][asunto]': {
					required		: 'Debes ingresar tu nombre.',
					maxlength		: $.validator.format('El asunto debe tener un máximo de {0} caracteres.')
				},
				'data[Lead][mensaje]': {
					required		: 'Debes ingresar el mensaje.',
					maxlength		: $.validator.format('El mensaje debe tener un máximo de {0} caracteres.')
				}
			}

		});

	}

	$('#menu-site').on( "mouseleave", function() {
	    OcultarMenu();
	 });

	$('.producto-select').on('click', function(){
		var idProducto 	=	$(this).attr('idProducto');

		$.ajax({
			type		: 'POST',
			url			: webroot + 'productos/ajax_obtenerProducto',
			data			: {
				idProducto		: idProducto,
			},
			success		: function(msg){
				$('.producto-select-info').addClass('fadeOutUp');
				$('.producto-select-info').html(msg);
				$('.producto-select-info').removeClass('fadeOutUp');
				$('.producto-select-info').addClass('fadeInUp');

			}
		});
	});

});

function AlturaSlider () {

	var AltoNavegador = $(window).height();

	if ($("#bootstrap-touch-slider").hasClass("banner-home")) {

		var AnchoNavegador = $(window).width();

		if (AnchoNavegador <= 768) {
			$("#bootstrap-touch-slider").css("height", AltoNavegador + "px");
		}

	}

	else {
		$("#bootstrap-touch-slider").css("max-height", AltoNavegador + "px");
	}

	if ($(".interior-linea-fondo-mobile").length > 0) {
		$(".interior-linea-fondo-mobile").css("height", AltoNavegador + "px");
	}

}

function FondoHeader () {

	if ($(window).scrollTop() > 100) {
		$("header").addClass("con-fondo");
	}
	else {
		$("header").removeClass("con-fondo");
	}

}


//Reforestar
	$(document).on('click', '.modal_compania', function() {
	    $('.modal-compania').modal('show');
	    var video = $(this).attr('data-video-id');
	    $('.modal-compania iframe').attr('src', 'https://www.youtube.com/embed/' + video+'?autoplay=1');
	});

	$('.modal-compania').on('hide.bs.modal', function() {
	    $('.modal-compania iframe').attr('src', '');
	});

$(document).ready(function() {

	new WOW().init();

	FondoHeader ();
	AlturaSlider ();

	$('#bootstrap-touch-slider').bsTouchSlider();

	$("#slider-instagram").owlCarousel({
		navigation: true,
		navigationText: ["<i class='fa fa-angle-left' aria-hidden='true'></i>", "<i class='fa fa-angle-right' aria-hidden='true'></i>"],
		slideSpeed: 300,
		pagination: false,
		paginationSpeed: 1000,
		autoHeight: false,
		autoPlay: 4000,
		autoPlayTimeout: 100,
		autoPlayHoverPause: false,
		items: 4,
		loop: true,
		touchDrag: true
	});

});

$(window).scroll(function() {
	FondoHeader();
	AlturaSlider ();
});

$(window).resize(function() {
	AlturaSlider ();
});

function SliderScroll () {
	$("html, body").animate({scrollTop: $("#bootstrap-touch-slider").next().offset().top - $("header").height()}, 500, function() {});
}

function DesplegarMenu () {
	$("header .btn-menu, header nav, .contenedor-principal").addClass("menu-desplegado");
	$("header .btn-menu a").attr("onclick", "OcultarMenu();");
	//$("header .btn-menu a").removeAttr("onmouseover");
	//$("header .btn-menu a").removeAttr("onmouseout");
	$('.btn-menu').css('left','220px');
}

function OcultarMenu () {
	$("header .btn-menu, header nav, .contenedor-principal").removeClass("menu-desplegado");
	$("header .btn-menu a").attr("onclick", "DesplegarMenu();");
	$("header .btn-menu a").attr("onmouseout", "FueraDeMenu();");
	$('.btn-menu').css('left','40px');
}

function FueraDeMenu () {
	$("header .btn-menu a").attr("onclick", "DesplegarMenu();");
	$("header .btn-menu a").removeAttr("onmouseout");
}

function MenuScroll (seccion) {
	$("html, body").animate({scrollTop: $("." + seccion).offset().top - $("header").height()}, 500, function() {
		$("header .btn-menu, header nav, .contenedor-principal").removeClass("menu-desplegado");
	});
}

function DesplegarSubmenu (obj) {
	$(obj).parent().find(".submenu").toggleClass("desplegado");
}